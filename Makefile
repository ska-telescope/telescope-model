
include .make/base.mk
include .make/python.mk
include .make/tmdata.mk

DOCS_SPHINXOPTS = -W --keep-going
PYTHON_SWITCHES_FOR_FLAKE8 = --ignore=E203,W503
PYTHON_VARS_BEFORE_PYTEST = PYTHONPATH=./src
PYTHON_VARS_AFTER_PYTEST = --cov-fail-under=100 --no-cov-on-fail

# Workaround for older pylint objecting to subscripts in type annotations
PYTHON_SWITCHES_FOR_PYLINT = --disable=E1136

PYTHON_LINT_TARGET = src/ tests/ script/ docs/src/

python-pre-format:
	@python script/normalise_cleandoc.py src/ska_telmodel \
	  || echo "Warning: Skipping normalising cleandoc! Ensure 'bowler' is installed!"
