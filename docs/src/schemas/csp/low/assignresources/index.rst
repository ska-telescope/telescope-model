ska-low-csp-assignresources
===========================

Examples for the different versions of the assignresources schema

.. toctree::
  :maxdepth: 1
  :caption: LOW CSP assignresources examples

  ska-low-csp-assignresources-3.2
  ska-low-csp-assignresources-3.0
  ska-low-csp-assignresources-2.0