ska-low-csp-configure
=====================

Examples for the different versions of the configure schema

.. toctree::
  :maxdepth: 1
  :caption: LOW CSP configure examples

  ska-low-csp-configure-4.0
  ska-low-csp-configure-3.2
  ska-low-csp-configure-3.1
  ska-low-csp-configure-3.0
  ska-low-csp-configure-2.0
