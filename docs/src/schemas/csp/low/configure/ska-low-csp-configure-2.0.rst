CSP configure 2.0
=================

JSON schema and example for CSP Low configure

.. ska-schema:: https://schema.skao.int/ska-low-csp-configure/2.0
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-low-csp-configure/2.0

       Example JSON (LOW CSP Configuration for CBF 0.1)

   .. ska-schema-example:: https://schema.skao.int/ska-low-csp-configure/2.0 pst_scan_vr

       Example JSON (CSP configuration for PST voltage recorder scan 2.4)
