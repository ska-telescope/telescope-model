CSP configure 3.0
=================

JSON schema and example for CSP Low configure

.. ska-schema:: https://schema.skao.int/ska-low-csp-configure/3.0
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-low-csp-configure/3.0

      Example JSON (LOW CSP Configuration for CBF 0.2)

   .. ska-schema-example:: https://schema.skao.int/ska-low-csp-configure/3.0 pst_scan_ft

      Example JSON (CSP configuration for PST flow through scan 2.4)
    
   .. ska-schema-example:: https://schema.skao.int/ska-low-csp-configure/3.0 pst_scan_pt

      Example JSON (CSP configuration for PST pulsar timing scan 2.4)

   .. ska-schema-example:: https://schema.skao.int/ska-low-csp-configure/3.0 pst_scan_ds

      Example JSON (CSP configuration for PST dynamic spectrum scan 2.4)
