ska-low-csp-releaseresources
============================

Examples for the different versions of the releaseresources schema

.. toctree::
  :maxdepth: 1
  :caption: LOW CSP releaseresources examples

  ska-low-csp-releaseresources-3.2
  ska-low-csp-releaseresources-3.0
  ska-low-csp-releaseresources-2.0
