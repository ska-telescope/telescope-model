ska-csp-assignresources
========================
Examples for the different versions of the assignresources schema

.. toctree::
  :maxdepth: 1
  :caption: MID CSP assignresources examples

  ska-csp-assignresources-3.0
  ska-csp-assignresources-2.3
  ska-csp-assignresources-2.2
