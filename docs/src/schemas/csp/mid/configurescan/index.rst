
ska-csp-configurescan
=====================

Examples for the different versions of the configurescan schema

.. toctree::
  :maxdepth: 1
  :caption: MID CSP configurescan examples

  ska-csp-configure-6.0
  ska-csp-configure-5.0
  ska-csp-configure-4.1
  ska-csp-configure-4.0
  ska-csp-configure-3.0
  ska-csp-configure-2.6
  ska-csp-configure-2.5
  ska-csp-configure-2.4
  ska-csp-configure-2.3
  ska-csp-configure-2.2
  ska-csp-configure-2.1
  ska-csp-configure-2.0
  ska-csp-configure-1.0
  ska-csp-configure-0.1