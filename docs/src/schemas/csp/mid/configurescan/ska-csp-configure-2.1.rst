
CSP configurescan 2.1
=====================

JSON schema and example for CSP Mid configure

.. ska-schema:: https://schema.skao.int/ska-csp-configure/2.1
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-csp-configure/2.1

       Example JSON (TMC input for science_a visibility scan)

   .. ska-schema-example:: https://schema.skao.int/ska-csp-configure/2.1 science_a

       Example JSON (CSP configuration for science_a visibility scan)

   .. ska-schema-example:: https://schema.skao.int/ska-csp-configure/2.1 cal_a

       Example JSON (CSP configuration for cal_a visibility scan)

   .. ska-schema-example:: https://schema.skao.int/ska-pss-configure/0.1 pss

       Example JSON (CSP configuration for PSS scan)