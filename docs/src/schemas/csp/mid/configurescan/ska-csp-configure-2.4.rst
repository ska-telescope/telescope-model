
CSP configurescan 2.4
=====================

JSON schema and example for CSP Mid configure

.. ska-schema:: https://schema.skao.int/ska-csp-configure/2.4
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-csp-configure/2.4

       Example JSON (TMC input for science_a visibility scan)

   .. ska-schema-example:: https://schema.skao.int/ska-csp-configure/2.4 science_a

       Example JSON (CSP configuration for science_a visibility scan)

   .. ska-schema-example:: https://schema.skao.int/ska-csp-configure/2.4 cal_a

       Example JSON (CSP configuration for cal_a visibility scan)

   .. ska-schema-example:: https://schema.skao.int/ska-csp-configure/2.4 pss

       Example JSON (CSP configuration for PSS scan)

   .. ska-schema-example:: https://schema.skao.int/ska-csp-configure/2.4 pst_beam

       Example JSON (CSP configuration for PST beam configuration)

   .. ska-schema-example:: https://schema.skao.int/ska-csp-configure/2.4 pst_scan_pt

       Example JSON (CSP configuration for PST pulsar timing scan)

   .. ska-schema-example:: https://schema.skao.int/ska-csp-configure/2.4 pst_scan_ds

       Example JSON (CSP configuration for PST dynamic spectrum scan)

   .. ska-schema-example:: https://schema.skao.int/ska-csp-configure/2.4 pst_scan_ft

       Example JSON (CSP configuration for PST flow through scan)

   .. ska-schema-example:: https://schema.skao.int/ska-csp-configure/2.4 pst_scan_vr

       Example JSON (CSP configuration for PST voltage recording scan)
