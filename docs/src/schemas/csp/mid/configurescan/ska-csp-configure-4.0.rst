
CSP configurescan  4.0
======================

JSON schema and example for CSP Mid configure

.. ska-schema:: https://schema.skao.int/ska-csp-configurescan/4.0
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-csp-configurescan/4.0

       Example (TMC input for science_a visibility scan)

   .. ska-schema-example:: https://schema.skao.int/ska-csp-configurescan/4.0 science_a

       Example (CSP configuration for science_a visibility scan)

   .. ska-schema-example:: https://schema.skao.int/ska-csp-configurescan/4.0 cal_a

       Example (CSP configuration for cal_a visibility scan)

   .. ska-schema-example:: https://schema.skao.int/ska-csp-configurescan/4.0 pss

       Example (CSP configuration for PSS scan)

   .. ska-schema-example:: https://schema.skao.int/ska-csp-configurescan/4.0 pst_beam

       Example (CSP configuration for PST beam configuration)

   .. ska-schema-example:: https://schema.skao.int/ska-csp-configurescan/4.0 pst_scan_pt

       Example (CSP configuration for PST pulsar timing scan)

   .. ska-schema-example:: https://schema.skao.int/ska-csp-configurescan/4.0 pst_scan_ds

       Example (CSP configuration for PST dynamic spectrum scan)

   .. ska-schema-example:: https://schema.skao.int/ska-csp-configurescan/4.0 pst_scan_ft

       Example (CSP configuration for PST flow through scan)

   .. ska-schema-example:: https://schema.skao.int/ska-csp-configurescan/4.0 pst_scan_vr

       Example (CSP configuration for PST voltage recording scan)
