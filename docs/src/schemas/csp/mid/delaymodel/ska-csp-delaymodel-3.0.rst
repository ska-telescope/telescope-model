CSP delay model 3.0
===================

JSON schema and example for CSP Mid delay model

.. ska-schema:: https://schema.skao.int/ska-mid-csp-delaymodel/3.0
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-mid-csp-delaymodel/3.0

       Example JSON
