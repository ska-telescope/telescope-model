CSP releaseresources 2.2
========================

JSON schema and example for CSP Mid releaseresources

.. ska-schema:: https://schema.skao.int/ska-csp-releaseresources/2.2
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-csp-releaseresources/2.2

       Example JSON