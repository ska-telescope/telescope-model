CSP releaseresources 3.0
========================

JSON schema and example for CSP Mid releaseresources

.. ska-schema:: https://schema.skao.int/ska-csp-releaseresources/3.0
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-csp-releaseresources/3.0

       Example JSON (Release all resources)

   .. ska-schema-example:: https://schema.skao.int/ska-csp-releaseresources/3.0 2

       Example JSON (Release specified resources)