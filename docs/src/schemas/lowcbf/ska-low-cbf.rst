Low CBF schemas
===============

Schemas used for commands to Low.CBF subarrays


.. toctree::
  :maxdepth: 1
  :caption: Low.CBF subarray command schemas

  ska-low-cbf-assignresources
  ska-low-cbf-configurescan
  ska-low-cbf-scan
  ska-low-cbf-releaseresources
