
ska-low-mccs-antenna-config
===========================

.. ska-schema:: https://schema.skao.int/ska-telmodel-antenna/1.0
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-telmodel-antenna/1.0

       Example JSON.
