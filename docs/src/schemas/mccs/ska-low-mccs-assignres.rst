
ska-low-mccs-assignresources
============================

.. ska-schema:: https://schema.skatelescope.org/ska-low-mccs-assignresources/1.0
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skatelescope.org/ska-low-mccs-assignresources/1.0

       Example JSON.
