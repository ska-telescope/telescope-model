
ska-mid-cbf-initsysparam 1.0
=============================

.. ska-schema:: https://schema.skao.int/ska-mid-cbf-initsysparam/1.0
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:


   .. ska-schema-example:: https://schema.skao.int/ska-mid-cbf-initsysparam/1.0

       Example (Mid.CBF Parameters)

   .. ska-schema-example:: https://schema.skao.int/ska-mid-cbf-initsysparam/1.0 uri

       Example (Mid.CBF Parameters Source URI)
