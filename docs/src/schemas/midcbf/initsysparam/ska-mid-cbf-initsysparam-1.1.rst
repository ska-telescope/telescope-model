
ska-mid-cbf-initsysparam 1.1
=============================

.. ska-schema:: https://schema.skao.int/ska-mid-cbf-initsysparam/1.1
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:


   .. ska-schema-example:: https://schema.skao.int/ska-mid-cbf-initsysparam/1.1

       Example (Mid.CBF Parameters)

   .. ska-schema-example:: https://schema.skao.int/ska-mid-cbf-initsysparam/1.1 uri

       Example (Mid.CBF Parameters Source URI)
