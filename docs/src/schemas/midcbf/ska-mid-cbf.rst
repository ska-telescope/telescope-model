
Mid CBF schemas
==============================

Schemas used for commands to the CSP Mid.CBF. 

See 
`Mid.CBF Controller and Subarray command documentation
<https://developer.skao.int/projects/ska-mid-cbf-mcs/en/latest/guide/interfaces/lmc_mcs_interface.html>`_
for documentation of all commands.

There is a proposal to add an AA0.5 version of the telmodel. Until the proposal is approved, 
the definition of the AA0.5 Mid.CBF can be found through the links below.

`AA0.5 Mid.CBF Capabilities
<https://confluence.skatelescope.org/display/SE/AA0.5+CBF+Capabilities+and+Configuration>`_

`AA0.5 Mid.CBF Possible Configurations and Future Capabilities
<https://confluence.skatelescope.org/display/SE/2.+FDR+%28AA2-AA4%29+Mid.CBF+Overview#id-2.FDR(AA2AA4)Mid.CBFOverview-5.SummaryofRoll-outMilestoneConfigurations>`_

.. toctree::
  :maxdepth: 1
  :caption: Mid.CBF schemas

  ../csp/mid/configurescan/index
  ../csp/mid/delaymodel/index
  ../csp/mid/scan/index
  initsysparam/index