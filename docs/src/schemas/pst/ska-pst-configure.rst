ska-pst-configure
=====================
Examples for the different versions of the configure schema

.. toctree::
  :maxdepth: 1
  :caption: PST configure examples

  ska-pst-configure-2.5
  ska-pst-configure-2.4 
