
ska-sdp-releaseres
==================

.. ska-schema:: https://schema.skao.int/ska-sdp-releaseres/1.0
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-sdp-releaseres/1.0

       Example

.. ska-schema:: https://schema.skao.int/ska-sdp-releaseres/0.5
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-sdp-releaseres/0.5

       Example

.. ska-schema:: https://schema.skao.int/ska-sdp-releaseres/0.4
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-sdp-releaseres/0.4

       Example
