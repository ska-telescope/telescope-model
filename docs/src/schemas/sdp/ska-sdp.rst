
Science Data Processor schemas
==============================

Schemas used for commands to / attributes from the SDP LMC. See
`SDP LMC subarray documentation
<https://developer.skao.int/projects/ska-sdp-lmc/en/latest/sdp-subarray.html>`_
for an overview of the interactions.

.. toctree::
  :maxdepth: 1
  :caption: SDP schemas

  ska-sdp-assignres
  ska-sdp-configure
  ska-sdp-scan
  ska-sdp-recvaddrs
  ska-sdp-releaseres
