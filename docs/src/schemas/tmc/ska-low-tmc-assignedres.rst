
ska-low-tmc-assignedresources
=============================

.. ska-schema:: https://schema.skatelescope.org/ska-low-tmc-assignedresources/1.0
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skatelescope.org/ska-low-tmc-assignedresources/1.0

       Example JSON.
