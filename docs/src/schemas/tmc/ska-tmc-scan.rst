
ska-tmc-scan
================

.. ska-schema:: https://schema.skao.int/ska-tmc-scan/2.1
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-tmc-scan/2.1

       Example JSON.