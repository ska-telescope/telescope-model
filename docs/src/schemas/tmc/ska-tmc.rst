
Telescope Manager Control schemas
=================================

.. toctree::
  :maxdepth: 1
  :caption: Low Schemas

  ska-low-tmc-assignres
  ska-low-tmc-configure
  ska-low-tmc-releaseres
  ska-low-tmc-scan
  ska-low-tmc-assignedres
  
.. toctree::
  :maxdepth: 1
  :caption: Mid Schemas
  
  ska-tmc-assignresources
  ska-tmc-configure
  ska-tmc-releaseresources
  ska-tmc-scan