.. doctest-skip-all
.. _package-guide:

.. _ska_telmodel:

*************
API reference
*************

ska_telmodel.data
-------------------

.. automodule:: ska_telmodel.data
    :members:
    :undoc-members:

.. _ska_telmodel.schema:

ska_telmodel.schema
-------------------

.. automodule:: ska_telmodel.schema
    :members:

