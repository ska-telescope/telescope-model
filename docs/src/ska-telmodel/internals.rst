

*********
Internals
*********

ska_telmodel.schema
====================

.. autofunction:: ska_telmodel.schema._validate_schema_using_document_url

ska_telmodel._common
====================

.. automodule:: ska_telmodel._common
    :members:

.. _ska-telmodel-channel-map:

ska_telmodel.channel_map
========================

.. automodule:: ska_telmodel.channel_map
    :members:

ska_telmodel.data
=================

.. automodule:: ska_telmodel.data.sources
    :members:
.. automodule:: ska_telmodel.data.backend
    :members:
.. automodule:: ska_telmodel.data.new_data_backend
    :members:

ska_telmodel.csp
================

.. automodule:: ska_telmodel.csp.config
    :members:
.. automodule:: ska_telmodel.csp.examples
    :members:
.. automodule:: ska_telmodel.csp.interface
    :members:
.. automodule:: ska_telmodel.csp.schema
    :members:
.. automodule:: ska_telmodel.csp.validators
    :members:
.. automodule:: ska_telmodel.csp.version
    :members:

ska_telmodel.pst
================

.. automodule:: ska_telmodel.pst.examples
    :members:
.. automodule:: ska_telmodel.pst.low_examples
    :members:
.. automodule:: ska_telmodel.pst.mid_examples
    :members:
.. automodule:: ska_telmodel.pst.schema
    :members:
.. automodule:: ska_telmodel.pst.version
    :members:


ska_telmodel.sdp
================

.. automodule:: ska_telmodel.sdp.common
    :members:
.. automodule:: ska_telmodel.sdp.example
    :members:
.. automodule:: ska_telmodel.sdp.schema
    :members:
.. automodule:: ska_telmodel.sdp.version
    :members:
