import importlib
import json
import pathlib
from traceback import format_exception

from docutils import nodes
from docutils.parsers.rst import Directive, DirectiveError
from docutils.utils import SystemMessagePropagation

from ska_telmodel.schema import schema_by_uri

sphinx_jsonschema = importlib.import_module("sphinx-jsonschema")


class SkaSchema(Directive):
    required_arguments = 1  # URI
    has_content = True

    option_spec = sphinx_jsonschema.JsonSchema.option_spec

    def run(self):
        # Split URI
        vals = self.arguments[0].rsplit("#", 1)
        if len(vals) > 2:
            raise ValueError(f"Invalid URI: {vals}")
        if len(vals) == 1:
            uri = vals[0]
            pointer = ""
        else:
            uri, pointer = vals

        # Parse contents
        contents_node = nodes.container("", classes=[])
        self.state.nested_parse(
            self.content, self.content_offset, contents_node
        )

        # Write to temporary file
        try:
            # Get schema by interface URI
            schema = schema_by_uri(uri, 0)

            # Dump to file
            json_schema = schema.json_schema(uri)
            config = self.state.document.settings.env.config
            if config.ska_schema_jsonschema_path:
                uri_path = uri[uri.find("//") + 2 :]
                uri_path = uri_path[uri_path.find("/") + 1 :]
                outdir = self.state.document.settings.env.app.builder.outdir
                path = pathlib.Path(
                    outdir, config.ska_schema_jsonschema_path, uri_path
                )
                path.parent.mkdir(parents=True, exist_ok=True)
                with open(path, "w") as f:
                    json.dump(json_schema, f, sort_keys=True, indent="  ")

            # Generate node for wide format
            fmt = sphinx_jsonschema.wide_format.WideFormat(
                self.state,
                self.lineno,
                uri,
                self.options,
                self.state.document.settings.env.app,
            )

            # Generate stringified JSON schema
            json_schema = schema.stringify_keys_recursive().json_schema(uri)

            # Generate documentation via JSON schema
            results = fmt.run(json_schema, pointer)
            for node in results:
                if isinstance(node, nodes.section):
                    for i, node2 in enumerate(node):
                        if isinstance(node2, nodes.title):
                            node.insert(i + 1, contents_node)
                            break
                    break
            return results

        except SystemMessagePropagation as detail:
            return [detail.args[0]]
        except DirectiveError as error:
            raise self.directive_error(error.level, error.msg)
        except Exception as error:
            tb = error.__traceback__
            # loop through all traceback points to only return the
            # last traceback
            while tb.tb_next:
                tb = tb.tb_next

            raise self.error(
                "".join(format_exception(type(error), error, tb, chain=False))
            )
