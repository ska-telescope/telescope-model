import importlib
import json
from traceback import format_exception

import jsbeautifier
from docutils import nodes
from docutils.parsers.rst import Directive, DirectiveError
from docutils.utils import SystemMessagePropagation
from sphinx.directives.code import CodeBlock

from ska_telmodel.schema import example_by_uri, validate

sphinx_jsonschema = importlib.import_module("sphinx-jsonschema")


class SkaSchemaExample(Directive):
    required_arguments = 1  # URI
    optional_arguments = 1  # Parameters
    final_argument_whitespace = True
    option_spec = CodeBlock.option_spec
    has_content = True

    def run(self):
        uri = self.arguments[0]
        if len(self.arguments) > 1:
            args = self.arguments[1].split(",")
        else:
            args = []

        # Write to temporary file
        try:
            # Get example
            example = example_by_uri(uri, *args)

            # Check against schema
            validate(uri, example, 2)

            # Make container
            toggle_node = nodes.container("", classes=["toggle"])
            header_node = nodes.container("", classes=["header"])
            self.state.nested_parse(
                self.content, self.content_offset, header_node
            )
            toggle_node += header_node

            # Generate code
            example_str = jsbeautifier.beautify(json.dumps(example))
            codeblock = CodeBlock(
                "",
                ["JSON"],
                self.options,
                example_str.split("\n"),
                self.lineno,
                self.content_offset,
                "",
                self.state,
                self.state_machine,
            )
            toggle_node += codeblock.run()
            return [toggle_node]

        except SystemMessagePropagation as detail:
            return [detail.args[0]]
        except DirectiveError as error:
            raise self.directive_error(error.level, error.msg)
        except Exception as error:
            tb = error.__traceback__
            # loop through all traceback points to only return
            # the last traceback
            while tb.tb_next:
                tb = tb.tb_next

            raise self.error(
                "".join(format_exception(type(error), error, tb, chain=False))
            )
