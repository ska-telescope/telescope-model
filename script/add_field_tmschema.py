import sys

from bowler import Query
from fissix import fixer_util, pygram, pytree


def add_field_tmschema(node, cap, fn):
    # Remove schema argument from argument list
    indent = node.prefix
    schema = cap["name"]
    schema.next_sibling.remove()
    schema.remove()

    # Remove entire trailer containing (now modified!) argument list
    trailer = cap["trailer"]
    trailer.remove()

    # Create '{schema}.add_field({*args})"
    schema.prefix = ""
    result = pytree.Node(
        pygram.python_symbols.simple_stmt,
        [
            pytree.Node(
                pygram.python_symbols.power,
                [
                    schema,
                    pytree.Node(
                        pygram.python_symbols.trailer,
                        [fixer_util.Dot(), fixer_util.Name("add_field")],
                    ),
                    trailer,
                ],
            ),
            fixer_util.Newline(),
        ],
        prefix=indent,
    )
    return result


Query(sys.argv[1]).select(
    """
simple_stmt<
  power< 'add_field'
    trailer=trailer< '('
        arglist< name=NAME ',' any* >
    ')' >
  > any*
>"""
).modify(add_field_tmschema).execute()
