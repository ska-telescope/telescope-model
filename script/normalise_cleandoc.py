'''
Refactoring to normalise cleandoc indentation, i.e.

   asd(..., description=cleandoc(
          """
             asd"""))

to

   asd(..., description=cleandoc(
          """
          asd
          """))
'''

import sys
from inspect import cleandoc

from bowler import Query
from fissix import fixer_util


def find_prefix(node):
    # Node itself has a prefix? Return
    if "\n" in node.prefix:
        return node.prefix

    # Otherwise indent one over the biggest prefix we can find
    node = node.parent
    while node:
        if "\n" in node.prefix:
            return node.prefix + "    "
        node = node.parent

    return ""


def normalise_cleandoc(node, cap, fn):
    # Only modify if it is a multi-line-string
    string = cap["str"].value
    if not (string.startswith('"""') and string.endswith('"""')) and not (
        string.startswith("'''") and string.endswith("'''")
    ):
        return
    sep = string[:3]
    string = string[3:-3]

    # Get indent
    indent = find_prefix(cap["str"])
    if "\n" in indent:
        indent = indent[indent.rindex("\n") + 1 :]

    # Clean, check whether we can simplify
    lines = cleandoc(string).split("\n")
    if len(lines) == 1:
        if '"' not in lines[0]:
            sep = '"'
        return fixer_util.String(sep + lines[0] + sep, prefix=node.prefix)

    # Reindent it, replace
    new_string = sep + "\n"
    new_string += "\n".join(
        (indent if line else "") + line.rstrip() for line in lines
    )
    new_string += "\n" + indent + sep

    if new_string != cap["str"].value:
        cap["str"].replace(
            fixer_util.String(new_string, prefix=cap["str"].prefix)
        )


Query(sys.argv[1]).select(
    """
power<
  'cleandoc'
  trailer< '('
    str=STRING
  ')' >
>"""
).modify(normalise_cleandoc).execute()
