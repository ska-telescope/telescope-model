"""
Refactoring to move from

   bla.add_field(..., optional=True, ...)

to

   bla.add_opt_field(..., ...)

"""

import sys

from bowler import Query
from fissix import fixer_util


def add_field_tmschema(node, cap, fn):
    # Replace add_field
    cap["add_field"].replace(fixer_util.Name("add_opt_field"))

    # Remove optional parameter
    if cap["opt"].next_sibling is not None:
        cap["opt"].next_sibling.remove()
    cap["opt"].remove()


Query(sys.argv[1]).select(
    """
simple_stmt<
  power<
    NAME
    trailer< '.' add_field='add_field' >
    trailer< '('
        arglist<
          any*
          opt = argument<
            'optional' '=' 'True'
          >
          any*
        >
    ')' >
  > any*
>"""
).modify(add_field_tmschema).execute()
