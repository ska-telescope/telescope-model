"""
SKA telescope model command line utility.

The environment variable SKA_TELMODEL_SOURCES can also be used for sources.
"""

import argparse
import logging
import os
import shutil
import sys
import tempfile
from pathlib import Path
from typing import Callable

from prettytable import PrettyTable

from ska_telmodel.data import TMData, TMObject
from ska_telmodel.data.frontend import TMDataBackend
from ska_telmodel.data.large_files import upload_large_file
from ska_telmodel.data.new_data_backend import GitBackend
from ska_telmodel.data.sources import DEFAULT_SOURCES
from ska_telmodel.schema import validate

logger = logging.getLogger(__name__)


class CustomFormatter(logging.Formatter):
    # Adapted from https://stackoverflow.com/questions/
    #   384076/how-can-i-color-python-logging-output

    white = "\x1b[1m"
    grey = "\x1b[38;20m"
    yellow = "\x1b[33;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"

    format_normal = "%(message)s"
    format_verbose = (
        "%(asctime)s - %(name)s - %(levelname)s - "
        "%(message)s (%(filename)s:%(lineno)d)"
    )

    def __init__(self, verbose):
        format_str = self.format_verbose if verbose else self.format_normal
        self.formatters = {
            logging.DEBUG: logging.Formatter(
                self.grey + format_str + self.reset
            ),
            logging.INFO: logging.Formatter(
                (self.white if verbose else self.grey)
                + format_str
                + self.reset
            ),
            logging.WARNING: logging.Formatter(
                self.yellow + format_str + self.reset
            ),
            logging.ERROR: logging.Formatter(
                self.red + format_str + self.reset
            ),
            logging.CRITICAL: logging.Formatter(
                self.bold_red + format_str + self.reset
            ),
        }

    def format(self, record):
        return self.formatters[record.levelno].format(
            record
        )  # pragma: no cover


class CLI:
    """SKA telescope model command line utility."""

    def __init__(self):
        self._sources = None

    def sources(
        self, sources: list[str], local: bool = False, update: bool = False
    ):
        """Get the list of sources to be used."""
        srcs = sources
        if not srcs:
            if local:
                srcs = "file://."
            else:
                srcs = os.getenv("SKA_TELMODEL_SOURCES")
        if srcs:
            srcs = srcs.split(",")
        else:
            srcs = DEFAULT_SOURCES

        self._sources = TMData(source_uris=srcs, update=update)

    def ls(
        self,
        prefix,
        full_listing: bool = False,
        human_readable: bool = False,
        summary: bool = False,
    ):
        """List telescope model keys with a particular prefix."""
        data = self._sources
        if prefix:
            data = data[prefix]

        if not full_listing:
            for entry in data:
                print(entry)
            return

        file_count, total_size = 0, 0

        table = PrettyTable()
        table.field_names = [
            "Source",
            "Key",
            "Large File?",
            "Cached?",
            "Size",
        ]
        table.align = "l"
        table.align["Size"] = "r"
        any_large = False

        for entry in data:
            tm_obj = data.get(entry)
            key = entry
            if prefix:
                key = f"{prefix}/{entry}"
            size = tm_obj.size

            file_count += 1
            total_size += size

            size = self._get_file_size(size) if human_readable else f"{size} B"

            any_large |= tm_obj.is_large_file
            table.add_row(
                [
                    tm_obj.source.get_uri(False),
                    key,
                    "yes" if tm_obj.is_large_file else "no",
                    "yes" if tm_obj.is_cached else "no",
                    size,
                ]
            )

        if any_large is False:
            table.del_column("Large File?")
            table.del_column("Cached?")

        print(table)
        if summary:
            print()
            print(f"Total Keys: {file_count}")
            if human_readable:
                print(f"Total Size: {self._get_file_size(total_size)}")
            else:
                print(f"Total Size: {total_size}")

    def _get_file_size(self, file_size: int) -> str:
        if file_size < 1024:
            return f"{file_size:.0f} B"

        file_size /= 1024
        if file_size < 1024:
            return f"{file_size:.0f} KB"

        file_size /= 1024
        if file_size < 1024:
            return f"{file_size:.0f} MB"

        file_size /= 1024

        return f"{file_size:.0f} GB"

    def pin(self):
        """
        Generates a "pinned" telescope model data source list, where all
        URIs replaced such that they will uniquely identify the contents
        of the telescope model data repository.
        """
        src_uris = self._sources.get_sources(True)
        for uri in src_uris:
            logging.info("Using %s", uri)
        print(f'SKA_TELMODEL_SOURCES={",".join(src_uris)}')

    def cp(self, key: str, path: str, recursive: bool = False):
        """
        Retrieves specified telescope model data, and copies it to
        the given path.
        """
        data = TMData(source_uris=self._sources)
        src_path = key or ""
        dest_path = Path(path)

        # Individual file?
        if not recursive or TMDataBackend.valid_key(src_path):
            if src_path.endswith(".link"):
                src_path = src_path[:-5]
            if dest_path.is_dir():
                dest_path = Path(dest_path, Path(src_path).name)
            data[src_path].copy(dest_path)
            print(f"{src_path} -> {dest_path}")
            return

        # Global?
        if src_path:
            src_data = data[src_path]
        else:
            src_data = data

        # Copy files recursively
        for key in src_data:
            dest = dest_path / key
            dest.parent.mkdir(parents=True, exist_ok=True)
            src_data[key].copy(dest)
            print(f"{Path(src_path, key)} -> {dest}")

    def cat(self, key):
        """
        Retrieves and prints the telescope model data identified by the
        given key to stdout.
        """
        with self._sources[key].open() as f:
            shutil.copyfileobj(f, sys.stdout.buffer)

    def validate(
        self, key: str, strict: bool = False, recursive: bool = False
    ):
        """
        Validates given keys (or files) against applicable schemas from
        the telescope model library
        """
        data = TMData(source_uris=self._sources)
        src_path = key

        # Individual file?
        if not recursive or TMDataBackend.valid_key(src_path):
            keys_to_check = [src_path]
        else:
            # Global?
            if src_path:
                keys_to_check = [f"{src_path}/{key}" for key in data[src_path]]
            else:
                keys_to_check = list(data)

        # Start checking keys
        success = True
        for key in keys_to_check:
            logger.info(f" * Checking {key}:")

            # Attempt to deserialise
            try:
                dct = data[key].get_dict()
            except ValueError as e:
                logger.error(data[key].get())
                logger.error(f" {e}")
                success = False
                continue

            # Check whether we can validate
            validated = False
            if "interface" in dct:
                try:
                    logger.info(dct["interface"])
                    validate(dct["interface"], dct, 1 + strict)
                    validated = True
                except ValueError as e:
                    logger.error(f"{e}")
                    success = False
                    continue

            # Fail if we do not find any method for validation
            if not validated:
                logger.error("No schema to use for validation!")
                success = False

        # Fail if appropriate
        if not success:
            exit(1)

    def upload_data(
        self,
        new_data_path: str,
        repo: str = None,
        upload_path: str = None,
        force_large_file: bool = False,
    ):
        """Upload a file/directory into a telmodel repo."""
        # if upload_path is None, assume same as input data.
        if upload_path is None:
            upload_path = new_data_path

        file = Path(new_data_path)
        if not file.exists():
            logger.error('Input file "%s" does not exist', new_data_path)
            exit(1)
        backend_file_local_path = None

        # If repo is None attempt to find the repo
        if repo is None:
            try:
                tm_data = self._sources[upload_path]
            except KeyError:
                tm_data = None
            if not isinstance(tm_data, TMObject):
                logger.error(
                    "Could not automatically determine repo/path, "
                    "try specifying --repo or path"
                )
                exit(1)
            backend_type = tm_data.source.backend_name()
            if backend_type == "file":
                backend_file_local_path = tm_data.source.base_path
                repo = "-"
            elif backend_type == "mem":
                logger.error("Memory backend is not supported")
                exit(1)
            elif backend_type in ["car", "gitlab"]:
                repo = tm_data.source.project_name

        else:
            check_repo = Path(repo)
            if check_repo.exists() and check_repo.is_dir():
                backend_type = "file"
                backend_file_local_path = check_repo
                repo = "local_files"
            else:
                backend_type = "gitlab"

        logger.debug("Backend in current use: %s", backend_type)
        if backend_type in ["car", "gitlab"]:
            logger.debug("Full support is available")
            self._confirm_choice(
                f"Add {new_data_path} to git://{repo} ({upload_path})"
            )
            self._upload_git(repo, file, upload_path, force_large_file)
        elif backend_type == "file":
            logger.debug("Only upload and link is available for local repo")
            self._confirm_choice(
                f"Add {new_data_path} to "
                f"file://{backend_file_local_path} ({upload_path})"
            )
            self._upload_and_add(
                repo,
                file,
                backend_file_local_path,
                upload_path,
                None,
                force_large_file,
            )
        else:  # pragma: no cover
            logger.error("Current backend not supported [%s]", backend_type)
            exit(1)

        logger.info("File added")

    def _confirm_choice(self, confirm_string: str):  # pragma: no cover
        confirm_choice = input(f"{confirm_string}? [y|N]: ")
        if len(confirm_choice) > 0:
            if confirm_choice.lower()[0] not in ["y", "t"]:
                logger.info("Cancelled")
                exit(0)
        else:
            logger.info("Cancelled")
            exit(0)

    def _upload_git(
        self,
        repo: str,
        file: Path,
        key_path: str,
        force_car_upload: bool = False,
    ):
        git_repo = GitBackend(repo=repo)
        branch_name = (
            input("Specify branch name to use: ").lower().replace(" ", "")
        )
        try:
            git_repo.start_transaction(branch_name)
        except ValueError as err:
            if str(err) == "Branch Already Exists":
                logger.warning("Branch already exists")
                new_branch_name = input(
                    "Enter new name, or blank to re-use the branch: "
                )
                if new_branch_name == "":
                    git_repo.start_transaction(
                        branch_name, create_new_branch=False
                    )
                else:
                    branch_name = new_branch_name
                    git_repo.start_transaction(new_branch_name)
        self._upload_and_add(
            repo,
            file,
            git_repo.local_location,
            key_path,
            git_repo.add_data,
            force_car_upload,
        )
        git_repo.commit(input("Please specify a commit message: "))
        git_repo.commit_transaction()
        logger.info(
            f"Branch used: {branch_name}, "
            f"please create relevant merge request."
        )
        if not (repo.startswith("http") or repo.startswith("ssh")):
            logger.info(
                f"https://gitlab.com/{repo}/-/merge_requests/new?"
                f"merge_request%5Bsource_branch%5D={branch_name}"
            )

    def _upload_and_add(
        self,
        repo,
        new_file: Path,
        local_repo_path: Path,
        key_path: str,
        copy_method: Callable[[str | Path, str | Path], None],
        force_car_upload: bool,
    ):
        max_size = os.environ.get("SKA_CAR_LARGE_FILE_SIZE", 1024 * 1024 * 100)
        logger.debug("Maximum file size is: %d bytes", max_size)
        if not force_car_upload and new_file.stat().st_size < max_size:
            logger.info("Adding file to repository directly")
            if copy_method is not None:
                copy_method(new_file, key_path)
            else:
                dest = Path(local_repo_path, key_path)
                dest.parent.mkdir(parents=True, exist_ok=True)
                shutil.copy(new_file, dest)
        else:
            logger.info("Large file. Uploading file to the CAR")
            link_file_contents = upload_large_file(repo, new_file, key_path)

            logger.debug("Adding link file to repository")
            with tempfile.TemporaryDirectory("ska-telmodel") as dirname:
                link_file_path = Path(dirname, "temp.link")
                with open(link_file_path, "w") as link_file:
                    link_file.write(link_file_contents)

                if copy_method is not None:
                    copy_method(link_file_path, f"{key_path}.link")
                else:
                    dest = Path(local_repo_path, f"{key_path}.link")
                    dest.parent.mkdir(parents=True, exist_ok=True)
                    shutil.copy(link_file_path, dest)


def main():
    # Parse command line
    cli = CLI()

    # Global Arguments
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "-v",
        "--verbose",
        help="Verbose mode",
        action="store_true",
        default=False,
    )
    parser.add_argument(
        "-U",
        "--update",
        help="Update source list",
        action="store_true",
        default=False,
    )
    parser.add_argument(
        "-S",
        "--sources",
        help=(
            "Set telescope model data sources of truth "
            "(','-separated list of URIs)"
        ),
        type=str,
    )
    parser.add_argument(
        "-l",
        "--local",
        help='Equivalent to "--sources=file://."',
        action="store_true",
        default=False,
    )

    sub_parsers = parser.add_subparsers(dest="command")

    parser_ls = sub_parsers.add_parser("ls", help=cli.ls.__doc__)
    parser_ls.add_argument("prefix", nargs="?", help="The prefix to use")
    parser_ls.add_argument(
        "-l",
        "--long",
        help="Print long listing",
        action="store_true",
        default=False,
    )
    parser_ls.add_argument(
        "-H",
        "--human-readable",
        help="Print sizes in human readable form, requires '-l'",
        action="store_true",
        default=False,
    )
    parser_ls.add_argument(
        "-s",
        "--summary",
        help="Print a summary after the list, requires '-l'",
        action="store_true",
        default=False,
    )

    parser_cat = sub_parsers.add_parser("cat", help=cli.cat.__doc__)
    parser_cat.add_argument("key", help="The keys to output")

    parser_cp = sub_parsers.add_parser("cp", help=cli.cp.__doc__)
    parser_cp.add_argument(
        "-R",
        "--recursive",
        help="Copy / validate keys or files recursively",
        action="store_true",
        default=False,
    )
    parser_cp.add_argument("key", help="From what directory/file to copy from")
    parser_cp.add_argument(
        "path", nargs="?", default=".", help="Where to copy to"
    )

    parser_validate = sub_parsers.add_parser(
        "validate", help=cli.validate.__doc__
    )
    parser_validate.add_argument(
        "-t",
        "--strict",
        help="Strict validation mode",
        action="store_true",
        default=False,
    )
    parser_validate.add_argument(
        "-R",
        "--recursive",
        help="Copy / validate keys or files recursively",
        action="store_true",
        default=False,
    )
    parser_validate.add_argument(
        "key", default="", help="From what directory/file to copy from"
    )

    _ = sub_parsers.add_parser("pin", help=cli.pin.__doc__)
    parser_ud = sub_parsers.add_parser("upload", help=cli.upload_data.__doc__)
    parser_ud.add_argument("data_file", help="Data file to upload", type=str)
    parser_ud.add_argument(
        "path",
        help="Specify the path in the tmdata where we should upload to.",
        default=None,
        nargs="?",
        type=str,
    )
    parser_ud.add_argument(
        "-r",
        "--repo",
        help="Specify gitlab repository to be updated.",
        default=None,
        type=str,
    )
    parser_ud.add_argument(
        "--force-car-upload",
        help="Force this file to upload as a large file",
        default=False,
        action="store_true",
    )

    args = parser.parse_args()

    # Initialise logging (globally)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(CustomFormatter(args.verbose))
    logging.basicConfig(
        level=(logging.DEBUG if args.verbose else logging.INFO),
        handlers=[ch],
    )
    logging.captureWarnings(True)

    cli.sources(args.sources, args.local, args.update)

    match args.command:
        case "ls":
            cli.ls(args.prefix, args.long, args.human_readable, args.summary)
        case "pin":
            cli.pin()
        case "cat":
            cli.cat(args.key)
        case "cp":
            cli.cp(args.key, args.path, args.recursive)
        case "validate":
            cli.validate(args.key, args.strict, args.recursive)
        case "upload":
            cli.upload_data(
                args.data_file, args.repo, args.path, args.force_car_upload
            )


if __name__ == "__main__":
    main()  # pragma: no cover
