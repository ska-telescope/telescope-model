# This module defines the LOW CSP schema
# for the OSO-TMC interface

from inspect import cleandoc

from .._common import TMSchema, split_interface_version
from ..lowcbf.schema import get_fsp_descr
from ..lowcbf.version import lowcbf_configurescan_uri
from ..skydirection import get_skydirection


def get_lowcbf_conf_schema_for_oso_tmc(
    version: str,
    strict: bool,
    tmc_schema_uri: str = "",
) -> TMSchema:
    """Retrieve Low CBF configure resources schema used by OSO-TMC interface.

    This method is invoked by Low TMC to create the configuration schema for
    the OSO-TMC interface.

    :param version: Interface Version URI
    :param strict: Schema strictness
    :param tmc_schema_uri: tmc URI version. Used to differentiate
        the OSO-TMC-CSP schema according
        to the TMC interface version
    :return: TMSchema
    """

    lowcbf = TMSchema.new(
        "LOWCBF subarray configurescan",
        version,
        strict,
        description=cleandoc(
            """
            Correlator and Beamformer specific parameters. This section
            contains the parameters relevant only for CBF
            sub-system. This section is forwarded only to CBF
            subelement.
            """
        ),
        as_reference=True,
    )
    lowcbf.add_field(
        "stations",
        get_all_stn_beams_descr_for_oso_tmc(version, strict),
        description="Subarray Stations and station beam input" "descriptions",
    )
    lowcbf.add_opt_field(
        "timing_beams",
        get_pst_beam_descr_outer_for_oso_tmc(version, strict, tmc_schema_uri),
        description="PST beam outputs descriptions",
    )
    lowcbf.add_opt_field(
        "search_beams",
        str,  # contents TBD
        description="PSS beam outputs descriptions",
    )
    cbf_version = split_interface_version(version) == (0, 1)
    lowcbf.add_opt_field(
        ("visibilities" if cbf_version else "vis"),
        get_vis_descr_outer_for_oso_tmc(version, strict, tmc_schema_uri),
        description="Visibility output descriptions",
    )
    lowcbf.add_opt_field(
        "zooms",
        str,  # contents TBD
        description="Zoom visibility output descriptions",
    )
    return lowcbf


def get_all_stn_beams_descr_for_oso_tmc(
    version: str,
    strict: bool,
) -> TMSchema:
    """Low.CBF station beams description schema for OSO-TMC interface.

    :param version: Interface Version URI
    :param strict: Schema strictness
    """
    stn_beam_descr = TMSchema.new("Subarray station beams", version, strict)
    # nakshatra support: added to allow OSO-TMC interface tests.
    # change to mandatory when Firmware field becomes deprecated
    stn_beam_descr.add_opt_field(
        "stn_beam_id",
        int,
        description="station beam id",
    )
    # modify key-word from beam_id to stn_beam_id in version >= 1.0
    cbf_version = split_interface_version(version) < (1, 0)
    stn_beam_descr.add_field(
        ("beam_id" if cbf_version else "stn_beam_id"),
        int,
        description="station beam id",
    )
    stn_beam_descr.add_field(
        "freq_ids",
        [
            int,
        ],
        description="list of station beam frequency ids",
    )
    if version.startswith(lowcbf_configurescan_uri(0, 1)):
        stn_beam_descr.add_field("boresight_dly_poly", str, description="URL")
    all_stn_beams_descr = TMSchema.new(
        "Subarray stations and station beams",
        version,
        strict,
        description=cleandoc("Station and station beams parameters"),
        as_reference=True,
    )
    all_stn_beams_descr.add_field(
        "stns",
        [
            [int, int],  # ie [station_id, substation_id],
        ],
        description="",
    )
    all_stn_beams_descr.add_field(
        "stn_beams",
        [
            stn_beam_descr,
        ],
        description="",
    )
    return all_stn_beams_descr


def get_vis_stn_beams_for_oso_tmc(
    version: str,
    strict: bool,
    tmc_schema_uri: str = "",
) -> TMSchema:
    """Low.CBF schema for station beams from which to calculate visibilities
    for OSO-TMC interface.

    :param version: Interface Version URI
    :param strict: Schema strictness
    :param tmc_schema_uri: tmc URI version. If defined, the CSP configure
        schema for the OSO-TMC interface is returned. Otherwise, the one
        for the TMC-CSP.
    """
    vis_stn_beams = TMSchema.new(
        "Station beams to correlate", version, strict, as_reference=True
    )
    vis_stn_beams.add_field("stn_beam_id", int, description="Station Beam ID")
    if tmc_schema_uri:
        (tmc_major, tmc_minor) = split_interface_version(tmc_schema_uri)
        if (tmc_major, tmc_minor) == (3, 1):
            vis_stn_beams.add_opt_field(
                "host",
                [
                    [int, str],
                ],
                description="SDP channel & IP Address",
            )
            vis_stn_beams.add_opt_field(
                "port",
                [
                    [int, int, int],
                ],
                description="SDP chan & UDP port, stride",
            )
            vis_stn_beams.add_opt_field(
                "mac",
                [
                    [int, str],
                ],
                description="SDP channel & server MAC",
            )
    vis_stn_beams.add_field(
        "integration_ms", int, description="milliseconds integration"
    )
    return vis_stn_beams


def get_vis_descr_outer_for_oso_tmc(
    version: str,
    strict: bool,
    tmc_schema_uri: str = "",
) -> TMSchema:
    """Low.CBF visibilities schema for OSO-TMC interface.

    :param version: Interface Version URI
    :param strict: Schema strictness
    :param tmc_schema_uri: tmc URI version. If defined, the CSP configure
        schema for the OSO-TMC interface is returned. Otherwise, the one
        for the TMC-CSP.
    """
    vis_outer = TMSchema.new("Visibilities description", version, strict)
    vis_outer.add_field(
        "fsp",
        get_fsp_descr_for_oso_tmc(version, strict),
        description="FSPs used for correlation",
    )
    vis_outer.add_field(
        "stn_beams",
        [
            get_vis_stn_beams_for_oso_tmc(version, strict, tmc_schema_uri),
        ],
        description="SDP visibility destinations",
    )
    return vis_outer


def get_fsp_descr_for_oso_tmc(version: str, strict: bool) -> TMSchema:
    """Low.CBF FSP description schema for OSO-TMC interface.

    :param version: Interface Version URI
    :param strict: Schema strictness
    """
    fsp_descr = TMSchema.new("FSP", version, strict)
    # nakshatra support: added to allow OSO-TMC interface tests.
    # change to mandatory when "firmware" field becomes deprecated
    fsp_descr.add_opt_field(
        "function_mode",
        str,
        description="Firmware name",
    )
    # when function-mode will be mandatory, keep "firmware" parameter for
    # retrocompatibility
    cbf_version = split_interface_version(version) < (1, 0)
    fsp_descr.add_field(
        ("firmware" if cbf_version else "function_mode"),
        str,
        description="Firmware name",
    )
    fsp_descr.add_field(
        "fsp_ids",
        [
            int,
        ],
        description="List of IDs (integer)",
    )

    return fsp_descr


def get_pst_beam_descr_outer_for_oso_tmc(
    version: str,
    strict: bool,
    tmc_schema_uri: str = "",
) -> TMSchema:
    """Low.CBF PST beam description schema.

    :param version: Interface Version URI
    :param strict: Schema strictness
    :param tmc_schema_uri: tmc URI version. If defined, the CSP configure
        schema for the OSO-TMC interface is returned. Otherwise, the one
        for the TMC-CSP.
    :return: Schema
    """
    (tmc_major, tmc_minor) = split_interface_version(tmc_schema_uri)
    pst_beam_descr_outer = TMSchema.new(
        "outer", version, strict, as_reference=True
    )
    pst_beam_descr = TMSchema.new(
        "PST beams description", version, strict, as_reference=True
    )
    pst_beam_descr.add_field(
        "stn_beam_id", int, description="Station beam ID for pst beamforming"
    )

    if (tmc_major, tmc_minor) >= (4, 1):
        pst_beam_descr.add_opt_field(
            "field",
            get_skydirection(version, strict, as_reference=True),
            description="field description",
        )
    pst_beam_descr.add_field("pst_beam_id", int, description="PST beam ID")

    if (tmc_major, tmc_minor) < (4, 0):
        # jones key is not required in OSO-TMC interface starting
        # from tmc interface version 4.0
        pst_beam_descr.add_field(
            "jones", str, description="Jones matrix source URI"
        )
    pst_beam_descr.add_field(
        "stn_weights",
        [
            float,
        ],
        description="weights for each station",
    )
    pst_beam_descr.add_opt_field(
        "rfi_enable",
        [
            bool,
        ],
        description="Master enable for RFI flagging",
    )
    pst_beam_descr.add_opt_field(
        "rfi_static_chans",
        [
            int,
        ],
        description="Freqency IDs to be always flagged",
    )
    pst_beam_descr.add_opt_field(
        "rfi_dynamic_chans",
        [
            int,
        ],
        description="Frequency IDs to be dynamically flagged",
    )
    pst_beam_descr.add_opt_field(
        "rfi_weighted", float, description="Parameter for dynamic flagging"
    )
    if version.startswith(lowcbf_configurescan_uri(0, 1)):
        _get_pst_beam_descr_outer_v1_for_oso_tmc(pst_beam_descr)

    pst_beam_descr_outer.add_field(
        "beams",
        [
            pst_beam_descr,
        ],
        description="inner",
    )
    _add_fsp_description_to_schema_for_oso_tmc(
        version, strict, pst_beam_descr_outer
    )

    return pst_beam_descr_outer


def _get_pst_beam_descr_outer_v1_for_oso_tmc(pst_beam_descr: TMSchema) -> None:
    """
    Add the PST beam descritpion to the main configuration schema.

    :param pst_beam_descr: The schema to add the current field.
    """
    pst_beam_descr.add_opt_field("firmware", str, description="Firmware name")
    pst_beam_descr.add_field(
        "offset_dly_poly", str, description="Delay polynomial source URI"
    )


def _add_fsp_description_to_schema_for_oso_tmc(
    version: str, strict: bool, schema: TMSchema
) -> None:
    """
    Add the field with the FSP configuration parmeters.

    :param version: Interface Version URI
    :param strict: Schema strictness
    :param schema: The schema to add the current field
    """
    if not version.startswith(lowcbf_configurescan_uri(0, 1)):
        schema.add_field(
            "fsp",
            get_fsp_descr(version, strict),
            description="FSPs used by PST",
        )
