import copy

from ska_telmodel.csp.low_version import (
    lowcsp_assignresources_uri,
    lowcsp_configure_uri,
    lowcsp_releaseresources_uri,
    lowcsp_scan_uri,
)

LOW_CSP_ASSIGNRESOURCES_2_0 = {
    "interface": "https://schema.skao.int/ska-low-csp-assignresources/2.0",
    "common": {"subarray_id": 1},
    "lowcbf": {
        "resources": [
            {
                "device": "fsp_01",
                "shared": True,
                "fw_image": "pst",
                "fw_mode": "unused",
            },
            {
                "device": "p4_01",
                "shared": True,
                "fw_image": "p4.bin",
                "fw_mode": "p4",
            },
        ]
    },
    "pss": {"beams_id": [1, 2, 3]},
    "pst": {"beams_id": [1]},
}

LOW_CSP_ASSIGNRESOURCES_3_0 = {
    "interface": "https://schema.skao.int/ska-low-csp-assignresources/3.0",
    "common": {"subarray_id": 1},
    "lowcbf": {},
    "pss": {"beams_id": [1, 2, 3]},
    "pst": {"beams_id": [1]},
}

LOW_CSP_ASSIGNRESOURCES_3_2 = {
    "interface": "https://schema.skao.int/ska-low-csp-assignresources/3.2",
    "transaction_id": "txn-....-00001",
    "common": {"subarray_id": 1},
    "lowcbf": {},
    "pss": {"beams_id": [1, 2, 3]},
    "pst": {"beams_id": [1]},
}

LOW_CSP_CONFIGURE_2_0 = {
    "interface": "https://schema.skao.int/ska-low-csp-configure/2.0",
    "subarray": {"subarray_name": "science period 23"},
    "common": {
        "config_id": "sbi-mvp01-20200325-00001-science_A",
        "subarray_id": 1,
        "eb_id": "eb-x449-20231105-34696",
    },
    "lowcbf": {
        "stations": {
            "stns": [[1, 0], [2, 0], [3, 0], [4, 0]],
            "stn_beams": [
                {
                    "beam_id": 1,
                    "freq_ids": [64, 65, 66, 67, 68, 69, 70, 71],
                    "boresight_dly_poly": (
                        "tango://delays.skao.int/low/stn-beam/1"
                    ),
                }
            ],
        },
    },
    "pss": {},
    "pst": {"beams": []},
}

LOW_CSP_CONFIGURE_2_0_PST_SCAN_VOLTAGE_RECORDER_2_4 = {
    "interface": "https://schema.skao.int/ska-low-csp-configure/2.0",
    "subarray": {"subarray_name": "science period 23"},
    "common": {
        "config_id": "sbi-mvp01-20200325-00001-science_A",
        "subarray_id": 1,
        "eb_id": "eb-x449-20231105-34696",
    },
    "lowcbf": {
        "stations": {
            "stns": [[1, 0], [2, 0], [3, 0], [4, 0]],
            "stn_beams": [
                {
                    "beam_id": 1,
                    "freq_ids": [64, 65, 66, 67, 68, 69, 70, 71],
                    "boresight_dly_poly": (
                        "tango://delays.skao.int/low/stn-beam/1"
                    ),
                }
            ],
        },
        "timing_beams": {
            "beams": [
                {
                    "pst_beam_id": 13,
                    "stn_beam_id": 1,
                    "offset_dly_poly": "url",
                    "stn_weights": [0.9, 1.0, 1.0, 0.9],
                    "jones": "url",
                    "rfi_enable": [True, True, True],
                    "rfi_static_chans": [1, 206, 997],
                    "rfi_dynamic_chans": [242, 1342],
                    "rfi_weighted": 0.87,
                }
            ]
        },
        "search_beams": "tbd",
        "zooms": "tbd",
    },
    "pss": {},
    "pst": {
        "beams": [
            {
                "beam_id": 1,
                "scan": {
                    "activation_time": "2022-01-19T23:07:45Z",
                    "bits_per_sample": 32,
                    "num_of_polarizations": 2,
                    "udp_nsamp": 32,
                    "wt_nsamp": 32,
                    "udp_nchan": 24,
                    "num_frequency_channels": 432,
                    "centre_frequency": 200000000.0,
                    "total_bandwidth": 1562500.0,
                    "observation_mode": "VOLTAGE_RECORDER",
                    "observer_id": "jdoe",
                    "project_id": "project1",
                    "pointing_id": "pointing1",
                    "source": "J1921+2153",
                    "itrf": [5109360.133, 2006852.586, -3238948.127],
                    "receiver_id": "receiver3",
                    "feed_polarization": "LIN",
                    "feed_handedness": 1,
                    "feed_angle": 1.234,
                    "feed_tracking_mode": "FA",
                    "feed_position_angle": 10.0,
                    "oversampling_ratio": [8, 7],
                    "coordinates": {
                        "equinox": 2000.0,
                        "ra": "19:21:44.815",
                        "dec": "21:53:02.400",
                    },
                    "max_scan_length": 20000.0,
                    "subint_duration": 30.0,
                    "receptors": ["receptor1", "receptor2"],
                    "receptor_weights": [0.4, 0.6],
                    "num_channelization_stages": 2,
                    "channelization_stages": [
                        {
                            "num_filter_taps": 1,
                            "filter_coefficients": [1.0],
                            "num_frequency_channels": 1024,
                            "oversampling_ratio": [32, 27],
                        },
                        {
                            "num_filter_taps": 1,
                            "filter_coefficients": [1.0],
                            "num_frequency_channels": 256,
                            "oversampling_ratio": [4, 3],
                        },
                    ],
                },
            },
        ],
    },
}

LOW_CSP_CONFIGURE_3_0 = {
    "interface": "https://schema.skao.int/ska-low-csp-configure/3.0",
    "subarray": {"subarray_name": "science period 23"},
    "common": {
        "config_id": "sbi-mvp01-20200325-00001-science_A",
        "subarray_id": 1,
        "eb_id": "eb-x449-20231105-34696",
    },
    "lowcbf": {
        "stations": {
            "stns": [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1]],
            "stn_beams": [
                {
                    "beam_id": 1,
                    "freq_ids": [400],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                }
            ],
        },
        "vis": {
            "fsp": {"firmware": "vis", "fsp_ids": [1]},
            "stn_beams": [
                {
                    "stn_beam_id": 1,
                    "host": [[0, "192.168.0.1"]],
                    "port": [[0, 9000, 1]],
                    "mac": [[0, "02-03-04-0a-0b-0c"]],
                    "integration_ms": 849,
                }
            ],
        },
    },
    "pss": {},
    "pst": {"beams": []},
}

LOW_CSP_CONFIGURE_3_0_PST_SCAN_FT_2_4 = {
    "interface": "https://schema.skao.int/ska-low-csp-configure/3.0",
    "subarray": {"subarray_name": "science period 23"},
    "common": {
        "config_id": "sbi-mvp01-20200325-00001-science_A",
        "subarray_id": 1,
        "eb_id": "eb-x449-20231105-34696",
    },
    "lowcbf": {
        "stations": {
            "stns": [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1]],
            "stn_beams": [
                {
                    "beam_id": 1,
                    "freq_ids": [400],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                }
            ],
        },
        "vis": {
            "fsp": {"firmware": "vis", "fsp_ids": [1]},
            "stn_beams": [
                {
                    "stn_beam_id": 1,
                    "host": [[0, "192.168.0.1"]],
                    "port": [[0, 9000, 1]],
                    "mac": [[0, "02-03-04-0a-0b-0c"]],
                    "integration_ms": 849,
                }
            ],
        },
        "timing_beams": {
            "fsp": {"firmware": "pst", "fsp_ids": [2]},
            "beams": [
                {
                    "pst_beam_id": 11,
                    "stn_beam_id": 1,
                    "stn_weights": [0.9, 1.0, 1.0, 1.0, 0.9, 1.0],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                    "jones": "tango://jones.skao.int/low/stn-beam/1",
                }
            ],
        },
    },
    "pss": {},
    "pst": {
        "beams": [
            {
                "beam_id": 11,
                "scan": {
                    "activation_time": "2022-01-19T23:07:45Z",
                    "bits_per_sample": 32,
                    "num_of_polarizations": 2,
                    "udp_nsamp": 32,
                    "wt_nsamp": 32,
                    "udp_nchan": 24,
                    "num_frequency_channels": 432,
                    "centre_frequency": 200000000.0,
                    "total_bandwidth": 1562500.0,
                    "observation_mode": "FLOW_THROUGH",
                    "observer_id": "jdoe",
                    "project_id": "project1",
                    "pointing_id": "pointing1",
                    "source": "J1921+2153",
                    "itrf": [5109360.133, 2006852.586, -3238948.127],
                    "receiver_id": "receiver3",
                    "feed_polarization": "CIRC",
                    "feed_handedness": 1,
                    "feed_angle": 1.234,
                    "feed_tracking_mode": "FA",
                    "feed_position_angle": 10.0,
                    "oversampling_ratio": [8, 7],
                    "coordinates": {
                        "equinox": 2000.0,
                        "ra": "19:21:44.815",
                        "dec": "21:53:02.400",
                    },
                    "max_scan_length": 20000.0,
                    "subint_duration": 30.0,
                    "receptors": ["receptor1", "receptor2"],
                    "receptor_weights": [0.4, 0.6],
                    "num_rfi_frequency_masks": 0,
                    "rfi_frequency_masks": [],
                    "destination_address": ["192.168.178.26", 9021],
                    "num_channelization_stages": 2,
                    "channelization_stages": [
                        {
                            "num_filter_taps": 1,
                            "filter_coefficients": [1.0],
                            "num_frequency_channels": 1024,
                            "oversampling_ratio": [32, 27],
                        },
                        {
                            "num_filter_taps": 1,
                            "filter_coefficients": [1.0],
                            "num_frequency_channels": 256,
                            "oversampling_ratio": [4, 3],
                        },
                    ],
                    "ft": {
                        "num_bits_out": 32,
                        "num_channels": 1,
                        "channels": [1],
                        "requantisation_scale": 1.0,
                        "requantisation_length": 1.0,
                    },
                },
            }
        ],
    },
}

LOW_CSP_CONFIGURE_3_0_PST_SCAN_PT_2_4 = {
    "interface": "https://schema.skao.int/ska-low-csp-configure/3.0",
    "subarray": {"subarray_name": "science period 23"},
    "common": {
        "config_id": "sbi-mvp01-20200325-00001-science_A",
        "subarray_id": 1,
        "eb_id": "eb-x449-20231105-34696",
    },
    "lowcbf": {
        "stations": {
            "stns": [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1]],
            "stn_beams": [
                {
                    "beam_id": 1,
                    "freq_ids": [400],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                }
            ],
        },
        "vis": {
            "fsp": {"firmware": "vis", "fsp_ids": [1]},
            "stn_beams": [
                {
                    "stn_beam_id": 1,
                    "host": [[0, "192.168.0.1"]],
                    "port": [[0, 9000, 1]],
                    "mac": [[0, "02-03-04-0a-0b-0c"]],
                    "integration_ms": 849,
                }
            ],
        },
        "timing_beams": {
            "fsp": {"firmware": "pst", "fsp_ids": [2]},
            "beams": [
                {
                    "pst_beam_id": 1,
                    "stn_beam_id": 1,
                    "stn_weights": [0.9, 1.0, 1.0, 1.0, 0.9, 1.0],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                    "jones": "tango://jones.skao.int/low/stn-beam/1",
                }
            ],
        },
    },
    "pss": {},
    "pst": {
        "beams": [
            {
                "beam_id": 1,
                "scan": {
                    "activation_time": "2022-01-19T23:07:45Z",
                    "bits_per_sample": 32,
                    "num_of_polarizations": 2,
                    "udp_nsamp": 32,
                    "wt_nsamp": 32,
                    "udp_nchan": 24,
                    "num_frequency_channels": 432,
                    "centre_frequency": 200000000.0,
                    "total_bandwidth": 1562500.0,
                    "observation_mode": "PULSAR_TIMING",
                    "observer_id": "jdoe",
                    "project_id": "project1",
                    "pointing_id": "pointing1",
                    "source": "J1921+2153",
                    "itrf": [5109360.133, 2006852.586, -3238948.127],
                    "receiver_id": "receiver3",
                    "feed_polarization": "CIRC",
                    "feed_handedness": 1,
                    "feed_angle": 1.234,
                    "feed_tracking_mode": "FA",
                    "feed_position_angle": 10.0,
                    "oversampling_ratio": [8, 7],
                    "coordinates": {
                        "equinox": 2000.0,
                        "ra": "19:21:44.815",
                        "dec": "21:53:02.400",
                    },
                    "max_scan_length": 10000.5,
                    "subint_duration": 30.0,
                    "receptors": ["receptor1", "receptor2"],
                    "receptor_weights": [0.4, 0.6],
                    "num_rfi_frequency_masks": 0,
                    "rfi_frequency_masks": [],
                    "destination_address": ["192.168.178.26", 9021],
                    "num_channelization_stages": 2,
                    "channelization_stages": [
                        {
                            "num_filter_taps": 1,
                            "filter_coefficients": [1.0],
                            "num_frequency_channels": 1024,
                            "oversampling_ratio": [32, 27],
                        },
                        {
                            "num_filter_taps": 1,
                            "filter_coefficients": [1.0],
                            "num_frequency_channels": 256,
                            "oversampling_ratio": [4, 3],
                        },
                    ],
                    "pt": {
                        "dispersion_measure": 100.0,
                        "rotation_measure": 0.0,
                        "ephemeris": "",
                        "pulsar_phase_predictor": "",
                        "output_frequency_channels": 1,
                        "output_phase_bins": 64,
                        "num_sk_config": 1,
                        "sk_config": [
                            {
                                "sk_range": [0.8, 0.9],
                                "sk_integration_limit": 100,
                                "sk_excision_limit": 25.0,
                            }
                        ],
                        "target_snr": 0.0,
                    },
                },
            }
        ]
    },
}

LOW_CSP_CONFIGURE_3_0_PST_SCAN_DYNAMIC_SPECTRUM_2_4 = {
    "interface": "https://schema.skao.int/ska-low-csp-configure/3.0",
    "subarray": {"subarray_name": "science period 23"},
    "common": {
        "config_id": "sbi-mvp01-20200325-00001-science_A",
        "subarray_id": 1,
        "eb_id": "eb-x449-20231105-34696",
    },
    "lowcbf": {
        "stations": {
            "stns": [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1]],
            "stn_beams": [
                {
                    "beam_id": 1,
                    "freq_ids": [400],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                }
            ],
        },
        "vis": {
            "fsp": {"firmware": "vis", "fsp_ids": [1]},
            "stn_beams": [
                {
                    "stn_beam_id": 1,
                    "host": [[0, "192.168.0.1"]],
                    "port": [[0, 9000, 1]],
                    "mac": [[0, "02-03-04-0a-0b-0c"]],
                    "integration_ms": 849,
                }
            ],
        },
        "timing_beams": {
            "fsp": {"firmware": "pst", "fsp_ids": [2]},
            "beams": [
                {
                    "pst_beam_id": 2,
                    "stn_beam_id": 1,
                    "stn_weights": [0.9, 1.0, 1.0, 1.0, 0.9, 1.0],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                    "jones": "tango://jones.skao.int/low/stn-beam/1",
                }
            ],
        },
    },
    "pss": {},
    "pst": {
        "beams": [
            {
                "beam_id": 2,
                "scan": {
                    "activation_time": "2022-01-19T23:07:45Z",
                    "bits_per_sample": 32,
                    "num_of_polarizations": 2,
                    "udp_nsamp": 32,
                    "wt_nsamp": 32,
                    "udp_nchan": 24,
                    "num_frequency_channels": 432,
                    "centre_frequency": 200000000.0,
                    "total_bandwidth": 1562500.0,
                    "observation_mode": "DYNAMIC_SPECTRUM",
                    "observer_id": "jdoe",
                    "project_id": "project1",
                    "pointing_id": "pointing1",
                    "source": "J1921+2153",
                    "itrf": [5109360.133, 2006852.586, -3238948.127],
                    "receiver_id": "receiver3",
                    "feed_polarization": "CIRC",
                    "feed_handedness": 1,
                    "feed_angle": 1.234,
                    "feed_tracking_mode": "FA",
                    "feed_position_angle": 10.0,
                    "oversampling_ratio": [8, 7],
                    "coordinates": {
                        "equinox": 2000.0,
                        "ra": "19:21:44.815",
                        "dec": "21:53:02.400",
                    },
                    "max_scan_length": 13000.2,
                    "subint_duration": 30.0,
                    "receptors": ["receptor1", "receptor2"],
                    "receptor_weights": [0.4, 0.6],
                    "num_rfi_frequency_masks": 0,
                    "rfi_frequency_masks": [],
                    "destination_address": ["192.168.178.26", 9021],
                    "num_channelization_stages": 2,
                    "channelization_stages": [
                        {
                            "num_filter_taps": 1,
                            "filter_coefficients": [1.0],
                            "num_frequency_channels": 1024,
                            "oversampling_ratio": [32, 27],
                        },
                        {
                            "num_filter_taps": 1,
                            "filter_coefficients": [1.0],
                            "num_frequency_channels": 256,
                            "oversampling_ratio": [4, 3],
                        },
                    ],
                    "ds": {
                        "dispersion_measure": 100.0,
                        "output_frequency_channels": 1,
                        "stokes_parameters": "Q",
                        "num_bits_out": 16,
                        "time_decimation_factor": 10,
                        "frequency_decimation_factor": 4,
                        "requantisation_scale": 1.0,
                        "requantisation_length": 1.0,
                    },
                },
            }
        ]
    },
}


LOW_CSP_CONFIGURE_4_0 = {
    "interface": "https://schema.skao.int/ska-low-csp-configure/4.0",
    "subarray": {"subarray_name": "science period 23"},
    "common": {
        "config_id": "sbi-mvp01-20200325-00001-science_A",
        "subarray_id": 1,
        "eb_id": "eb-x449-20231105-34696",
    },
    "lowcbf": {
        "stations": {
            "stns": [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1]],
            "stn_beams": [
                {
                    "stn_beam_id": 1,
                    "freq_ids": [400],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                }
            ],
        },
        "vis": {
            "fsp": {"function_mode": "vis", "fsp_ids": [1]},
            "stn_beams": [
                {
                    "stn_beam_id": 1,
                    "host": [[0, "192.168.0.1"]],
                    "port": [[0, 9000, 1]],
                    "mac": [[0, "02-03-04-0a-0b-0c"]],
                    "integration_ms": 849,
                }
            ],
        },
    },
    "pss": {},
    "pst": {"beams": []},
}

LOW_CSP_CONFIGURE_3_1 = {
    "interface": "https://schema.skao.int/ska-low-csp-configure/3.1",
    "subarray": {"subarray_name": "science period 23"},
    "common": {
        "config_id": "sbi-mvp01-20200325-00001-science_A",
        "subarray_id": 1,
        "eb_id": "eb-x449-20231105-34696",
    },
    "lowcbf": {
        "stations": {
            "stns": [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1]],
            "stn_beams": [
                {
                    "beam_id": 1,
                    "freq_ids": [400],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                }
            ],
        },
        "vis": {
            "fsp": {"firmware": "vis", "fsp_ids": [1]},
            "stn_beams": [
                {
                    "stn_beam_id": 1,
                    "host": [[0, "192.168.0.1"]],
                    "port": [[0, 9000, 1]],
                    "mac": [[0, "02-03-04-0a-0b-0c"]],
                    "integration_ms": 849,
                }
            ],
        },
    },
    "pss": {},
    "pst": {"beams": []},
}

# timing beams FT
LOW_CSP_CONFIGURE_3_1_PST_SCAN_FT_2_5 = {
    "interface": "https://schema.skao.int/ska-low-csp-configure/3.1",
    "subarray": {"subarray_name": "science period 23"},
    "common": {
        "config_id": "sbi-mvp01-20200325-00001-science_A",
        "subarray_id": 1,
        "eb_id": "eb-x449-20231105-34696",
    },
    "lowcbf": {
        "stations": {
            "stns": [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1]],
            "stn_beams": [
                {
                    "beam_id": 1,
                    "freq_ids": [400],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                }
            ],
        },
        "timing_beams": {
            "fsp": {"firmware": "pst", "fsp_ids": [2]},
            "beams": [
                {
                    "pst_beam_id": 1,
                    "stn_beam_id": 1,
                    "stn_weights": [0.9, 1.0, 1.0, 1.0, 0.9, 1.0],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                    "jones": "tango://jones.skao.int/low/stn-beam/1",
                }
            ],
        },
        "vis": {
            "fsp": {"firmware": "vis", "fsp_ids": [1]},
            "stn_beams": [
                {
                    "stn_beam_id": 1,
                    "host": [[0, "192.168.0.1"]],
                    "port": [[0, 9000, 1]],
                    "mac": [[0, "02-03-04-0a-0b-0c"]],
                    "integration_ms": 849,
                }
            ],
        },
    },
    "pst": {
        "beams": [
            {
                "beam_id": 1,
                "scan": {
                    "activation_time": "2022-01-19T23:07:45Z",
                    "bits_per_sample": 32,
                    "num_of_polarizations": 2,
                    "udp_nsamp": 32,
                    "wt_nsamp": 32,
                    "udp_nchan": 24,
                    "num_frequency_channels": 432,
                    "centre_frequency": 200000000.0,
                    "total_bandwidth": 1562500.0,
                    "observation_mode": "FLOW_THROUGH",
                    "observer_id": "jdoe",
                    "project_id": "project1",
                    "pointing_id": "pointing1",
                    "source": "J1921+2153",
                    "itrf": [5109360.133, 2006852.586, -3238948.127],
                    "receiver_id": "receiver3",
                    "feed_polarization": "CIRC",
                    "feed_handedness": 1,
                    "feed_angle": 1.234,
                    "feed_tracking_mode": "FA",
                    "feed_position_angle": 10.0,
                    "oversampling_ratio": [8, 7],
                    "coordinates": {
                        "equinox": 2000.0,
                        "ra": "19:21:44.815",
                        "dec": "21:53:02.400",
                    },
                    "max_scan_length": 20000.0,
                    "subint_duration": 30.0,
                    "receptors": ["receptor1", "receptor2"],
                    "receptor_weights": [0.4, 0.6],
                    "num_rfi_frequency_masks": 0,
                    "rfi_frequency_masks": [],
                    "destination_address": ["192.168.178.26", 9021],
                    "num_channelization_stages": 2,
                    "channelization_stages": [
                        {
                            "num_filter_taps": 1,
                            "filter_coefficients": [1.0],
                            "num_frequency_channels": 1024,
                            "oversampling_ratio": [32, 27],
                        },
                        {
                            "num_filter_taps": 1,
                            "filter_coefficients": [1.0],
                            "num_frequency_channels": 256,
                            "oversampling_ratio": [4, 3],
                        },
                    ],
                    "ft": {
                        "num_bits_out": 4,
                        "channels": [0, 24299],
                        "polarizations": "Both",
                        "requantisation_scale": 1.0,
                        "requantisation_init_time": 1.0,
                    },
                },
            },
        ],
    },
}

LOW_CSP_CONFIGURE_3_1_PST_SCAN_PT_2_5 = {
    "interface": "https://schema.skao.int/ska-low-csp-configure/3.1",
    "subarray": {"subarray_name": "science period 23"},
    "common": {
        "config_id": "sbi-mvp01-20200325-00001-science_A",
        "subarray_id": 1,
        "eb_id": "eb-x449-20231105-34696",
    },
    "lowcbf": {
        "stations": {
            "stns": [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1]],
            "stn_beams": [
                {
                    "beam_id": 1,
                    "freq_ids": [400],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                }
            ],
        },
        "timing_beams": {
            "fsp": {"firmware": "pst", "fsp_ids": [2]},
            "beams": [
                {
                    "pst_beam_id": 1,
                    "stn_beam_id": 1,
                    "stn_weights": [0.9, 1.0, 1.0, 1.0, 0.9, 1.0],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                    "jones": "tango://jones.skao.int/low/stn-beam/1",
                }
            ],
        },
        "vis": {
            "fsp": {"firmware": "vis", "fsp_ids": [1]},
            "stn_beams": [
                {
                    "stn_beam_id": 1,
                    "host": [[0, "192.168.0.1"]],
                    "port": [[0, 9000, 1]],
                    "mac": [[0, "02-03-04-0a-0b-0c"]],
                    "integration_ms": 849,
                }
            ],
        },
    },
    "pst": {
        "beams": [
            {
                "beam_id": 1,
                "scan": {
                    "activation_time": "2022-01-19T23:07:45Z",
                    "bits_per_sample": 32,
                    "num_of_polarizations": 2,
                    "udp_nsamp": 32,
                    "wt_nsamp": 32,
                    "udp_nchan": 24,
                    "num_frequency_channels": 432,
                    "centre_frequency": 200000000.0,
                    "total_bandwidth": 1562500.0,
                    "observation_mode": "PULSAR_TIMING",
                    "observer_id": "jdoe",
                    "project_id": "project1",
                    "pointing_id": "pointing1",
                    "source": "J1921+2153",
                    "itrf": [5109360.133, 2006852.586, -3238948.127],
                    "receiver_id": "receiver3",
                    "feed_polarization": "CIRC",
                    "feed_handedness": 1,
                    "feed_angle": 1.234,
                    "feed_tracking_mode": "FA",
                    "feed_position_angle": 10.0,
                    "oversampling_ratio": [8, 7],
                    "coordinates": {
                        "equinox": 2000.0,
                        "ra": "19:21:44.815",
                        "dec": "21:53:02.400",
                    },
                    "max_scan_length": 10000.5,
                    "subint_duration": 30.0,
                    "receptors": ["receptor1", "receptor2"],
                    "receptor_weights": [0.4, 0.6],
                    "num_rfi_frequency_masks": 0,
                    "rfi_frequency_masks": [],
                    "destination_address": ["192.168.178.26", 9021],
                    "num_channelization_stages": 2,
                    "channelization_stages": [
                        {
                            "num_filter_taps": 1,
                            "filter_coefficients": [1.0],
                            "num_frequency_channels": 1024,
                            "oversampling_ratio": [32, 27],
                        },
                        {
                            "num_filter_taps": 1,
                            "filter_coefficients": [1.0],
                            "num_frequency_channels": 256,
                            "oversampling_ratio": [4, 3],
                        },
                    ],
                    "pt": {
                        "dispersion_measure": 100.0,
                        "rotation_measure": 0.0,
                        "ephemeris": "",
                        "pulsar_phase_predictor": "",
                        "output_frequency_channels": 1,
                        "output_phase_bins": 64,
                        "num_sk_config": 1,
                        "sk_config": [
                            {
                                "sk_range": [0.8, 0.9],
                                "sk_integration_limit": 100,
                                "sk_excision_limit": 25.0,
                            }
                        ],
                        "target_snr": 0.0,
                    },
                },
            },
        ],
    },
}

LOW_CSP_CONFIGURE_3_1_PST_SCAN_VOLTAGE_RECORDER_2_5 = {
    "interface": "https://schema.skao.int/ska-low-csp-configure/3.1",
    "subarray": {"subarray_name": "science period 23"},
    "common": {
        "config_id": "sbi-mvp01-20200325-00001-science_A",
        "subarray_id": 1,
        "eb_id": "eb-x449-20231105-34696",
    },
    "lowcbf": {
        "stations": {
            "stns": [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1]],
            "stn_beams": [
                {
                    "beam_id": 1,
                    "freq_ids": [400],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                }
            ],
        },
        "timing_beams": {
            "fsp": {"firmware": "pst", "fsp_ids": [2]},
            "beams": [
                {
                    "pst_beam_id": 1,
                    "stn_beam_id": 1,
                    "stn_weights": [0.9, 1.0, 1.0, 1.0, 0.9, 1.0],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                    "jones": "tango://jones.skao.int/low/stn-beam/1",
                }
            ],
        },
        "vis": {
            "fsp": {"firmware": "vis", "fsp_ids": [1]},
            "stn_beams": [
                {
                    "stn_beam_id": 1,
                    "host": [[0, "192.168.0.1"]],
                    "port": [[0, 9000, 1]],
                    "mac": [[0, "02-03-04-0a-0b-0c"]],
                    "integration_ms": 849,
                }
            ],
        },
    },
    "pst": {
        "beams": [
            {
                "beam_id": 1,
                "scan": {
                    "activation_time": "2022-01-19T23:07:45Z",
                    "bits_per_sample": 32,
                    "num_of_polarizations": 2,
                    "udp_nsamp": 32,
                    "wt_nsamp": 32,
                    "udp_nchan": 24,
                    "num_frequency_channels": 432,
                    "centre_frequency": 200000000.0,
                    "total_bandwidth": 1562500.0,
                    "observation_mode": "VOLTAGE_RECORDER",
                    "observer_id": "jdoe",
                    "project_id": "project1",
                    "pointing_id": "pointing1",
                    "source": "J1921+2153",
                    "itrf": [5109360.133, 2006852.586, -3238948.127],
                    "receiver_id": "receiver3",
                    "feed_polarization": "LIN",
                    "feed_handedness": 1,
                    "feed_angle": 1.234,
                    "feed_tracking_mode": "FA",
                    "feed_position_angle": 10.0,
                    "oversampling_ratio": [8, 7],
                    "coordinates": {
                        "equinox": 2000.0,
                        "ra": "19:21:44.815",
                        "dec": "21:53:02.400",
                    },
                    "max_scan_length": 20000.0,
                    "subint_duration": 30.0,
                    "receptors": ["receptor1", "receptor2"],
                    "receptor_weights": [0.4, 0.6],
                    "num_channelization_stages": 2,
                    "channelization_stages": [
                        {
                            "num_filter_taps": 1,
                            "filter_coefficients": [1.0],
                            "num_frequency_channels": 1024,
                            "oversampling_ratio": [32, 27],
                        },
                        {
                            "num_filter_taps": 1,
                            "filter_coefficients": [1.0],
                            "num_frequency_channels": 256,
                            "oversampling_ratio": [4, 3],
                        },
                    ],
                },
            },
        ],
    },
}

LOW_CSP_CONFIGURE_3_1_PST_SCAN_DYNAMIC_SPECTRUM_2_5 = {
    "interface": "https://schema.skao.int/ska-low-csp-configure/3.1",
    "subarray": {"subarray_name": "science period 23"},
    "common": {
        "config_id": "sbi-mvp01-20200325-00001-science_A",
        "subarray_id": 1,
        "eb_id": "eb-x449-20231105-34696",
    },
    "lowcbf": {
        "stations": {
            "stns": [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1]],
            "stn_beams": [
                {
                    "beam_id": 1,
                    "freq_ids": [400],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                }
            ],
        },
        "timing_beams": {
            "fsp": {"firmware": "pst", "fsp_ids": [2]},
            "beams": [
                {
                    "pst_beam_id": 1,
                    "stn_beam_id": 1,
                    "stn_weights": [0.9, 1.0, 1.0, 1.0, 0.9, 1.0],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                    "jones": "tango://jones.skao.int/low/stn-beam/1",
                }
            ],
        },
        "vis": {
            "fsp": {"firmware": "vis", "fsp_ids": [1]},
            "stn_beams": [
                {
                    "stn_beam_id": 1,
                    "host": [[0, "192.168.0.1"]],
                    "port": [[0, 9000, 1]],
                    "mac": [[0, "02-03-04-0a-0b-0c"]],
                    "integration_ms": 849,
                }
            ],
        },
    },
    "pst": {
        "beams": [
            {
                "beam_id": 1,
                "scan": {
                    "activation_time": "2022-01-19T23:07:45Z",
                    "bits_per_sample": 32,
                    "num_of_polarizations": 2,
                    "udp_nsamp": 32,
                    "wt_nsamp": 32,
                    "udp_nchan": 24,
                    "num_frequency_channels": 432,
                    "centre_frequency": 200000000.0,
                    "total_bandwidth": 1562500.0,
                    "observation_mode": "DYNAMIC_SPECTRUM",
                    "observer_id": "jdoe",
                    "project_id": "project1",
                    "pointing_id": "pointing1",
                    "source": "J1921+2153",
                    "itrf": [5109360.133, 2006852.586, -3238948.127],
                    "receiver_id": "receiver3",
                    "feed_polarization": "CIRC",
                    "feed_handedness": 1,
                    "feed_angle": 1.234,
                    "feed_tracking_mode": "FA",
                    "feed_position_angle": 10.0,
                    "oversampling_ratio": [8, 7],
                    "coordinates": {
                        "equinox": 2000.0,
                        "ra": "19:21:44.815",
                        "dec": "21:53:02.400",
                    },
                    "max_scan_length": 13000.2,
                    "subint_duration": 30.0,
                    "receptors": ["receptor1", "receptor2"],
                    "receptor_weights": [0.4, 0.6],
                    "num_rfi_frequency_masks": 0,
                    "rfi_frequency_masks": [],
                    "destination_address": ["192.168.178.26", 9021],
                    "num_channelization_stages": 2,
                    "channelization_stages": [
                        {
                            "num_filter_taps": 1,
                            "filter_coefficients": [1.0],
                            "num_frequency_channels": 1024,
                            "oversampling_ratio": [32, 27],
                        },
                        {
                            "num_filter_taps": 1,
                            "filter_coefficients": [1.0],
                            "num_frequency_channels": 256,
                            "oversampling_ratio": [4, 3],
                        },
                    ],
                    "ds": {
                        "dispersion_measure": 100.0,
                        "output_frequency_channels": 1,
                        "stokes_parameters": "Q",
                        "num_bits_out": 16,
                        "time_decimation_factor": 10,
                        "frequency_decimation_factor": 4,
                        "requantisation_scale": 1.0,
                        "requantisation_length": 1.0,
                    },
                },
            },
        ],
    },
}


LOW_CSP_CONFIGURE_3_2 = {
    "interface": "https://schema.skao.int/ska-low-csp-configure/3.2",
    "transaction_id": "txn-....-00001",
    "subarray": {"subarray_name": "science period 23"},
    "common": {
        "config_id": "sbi-mvp01-20200325-00001-science_A",
        "subarray_id": 1,
        "eb_id": "eb-x449-20231105-34696",
    },
    "lowcbf": {
        "stations": {
            "stns": [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1]],
            "stn_beams": [
                {
                    "beam_id": 1,
                    "freq_ids": [400],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                }
            ],
        },
        "vis": {
            "fsp": {"firmware": "vis", "fsp_ids": [1]},
            "stn_beams": [
                {
                    "stn_beam_id": 1,
                    "host": [[0, "192.168.0.1"]],
                    "port": [[0, 9000, 1]],
                    "mac": [[0, "02-03-04-0a-0b-0c"]],
                    "integration_ms": 849,
                }
            ],
        },
    },
}

LOW_CSP_CONFIGURE_3_2_PST_SCAN_VOLTAGE_RECORDER_2_5 = {
    "interface": "https://schema.skao.int/ska-low-csp-configure/3.2",
    "transaction_id": "txn-....-00001",
    "subarray": {"subarray_name": "science period 23"},
    "common": {
        "config_id": "sbi-mvp01-20200325-00001-science_A",
        "subarray_id": 1,
        "eb_id": "eb-x449-20231105-34696",
    },
    "lowcbf": {
        "stations": {
            "stns": [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1]],
            "stn_beams": [
                {
                    "beam_id": 1,
                    "freq_ids": [400],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                }
            ],
        },
        "timing_beams": {
            "fsp": {"firmware": "pst", "fsp_ids": [2]},
            "beams": [
                {
                    "pst_beam_id": 1,
                    "stn_beam_id": 1,
                    "stn_weights": [0.9, 1.0, 1.0, 1.0, 0.9, 1.0],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                    "jones": "tango://jones.skao.int/low/stn-beam/1",
                }
            ],
        },
        "vis": {
            "fsp": {"firmware": "vis", "fsp_ids": [1]},
            "stn_beams": [
                {
                    "stn_beam_id": 1,
                    "host": [[0, "192.168.0.1"]],
                    "port": [[0, 9000, 1]],
                    "mac": [[0, "02-03-04-0a-0b-0c"]],
                    "integration_ms": 849,
                }
            ],
        },
    },
    "pst": {
        "beams": [
            {
                "beam_id": 1,
                "scan": {
                    "activation_time": "2022-01-19T23:07:45Z",
                    "bits_per_sample": 32,
                    "num_of_polarizations": 2,
                    "udp_nsamp": 32,
                    "wt_nsamp": 32,
                    "udp_nchan": 24,
                    "num_frequency_channels": 432,
                    "centre_frequency": 200000000.0,
                    "total_bandwidth": 1562500.0,
                    "observation_mode": "VOLTAGE_RECORDER",
                    "observer_id": "jdoe",
                    "project_id": "project1",
                    "pointing_id": "pointing1",
                    "source": "J1921+2153",
                    "itrf": [5109360.133, 2006852.586, -3238948.127],
                    "receiver_id": "receiver3",
                    "feed_polarization": "LIN",
                    "feed_handedness": 1,
                    "feed_angle": 1.234,
                    "feed_tracking_mode": "FA",
                    "feed_position_angle": 10.0,
                    "oversampling_ratio": [8, 7],
                    "coordinates": {
                        "equinox": 2000.0,
                        "ra": "19:21:44.815",
                        "dec": "21:53:02.400",
                    },
                    "max_scan_length": 20000.0,
                    "subint_duration": 30.0,
                    "receptors": ["receptor1", "receptor2"],
                    "receptor_weights": [0.4, 0.6],
                    "num_channelization_stages": 2,
                    "channelization_stages": [
                        {
                            "num_filter_taps": 1,
                            "filter_coefficients": [1.0],
                            "num_frequency_channels": 1024,
                            "oversampling_ratio": [32, 27],
                        },
                        {
                            "num_filter_taps": 1,
                            "filter_coefficients": [1.0],
                            "num_frequency_channels": 256,
                            "oversampling_ratio": [4, 3],
                        },
                    ],
                },
            },
        ],
    },
}

LOW_CSP_RELEASERESOURCES_2_0 = {
    "interface": "https://schema.skao.int/ska-low-csp-releaseresources/2.0",
    "common": {"subarray_id": 1},
    "lowcbf": {
        "resources": [
            {"device": "fsp_01"},
        ]
    },
    "pst": {"beams_id": [1]},
}

LOW_CSP_RELEASERESOURCES_3_0 = {
    "interface": "https://schema.skao.int/ska-low-csp-releaseresources/3.0",
    "common": {"subarray_id": 1},
    "lowcbf": {},
    "pst": {"beams_id": [1]},
}

LOW_CSP_RELEASERESOURCES_3_2 = {
    "interface": "https://schema.skao.int/ska-low-csp-releaseresources/3.2",
    "transaction_id": "txn-....-00001",
    "common": {"subarray_id": 1},
    "lowcbf": {},
    "pst": {"beams_id": [1]},
}

LOW_CSP_SCAN_2_0 = {
    "interface": "https://schema.skao.int/ska-low-csp-scan/2.0",
    "common": {"subarray_id": 1},
    "lowcbf": {"scan_id": 987654321},
}

LOW_CSP_SCAN_3_2 = {
    "interface": "https://schema.skao.int/ska-low-csp-scan/3.2",
    "transaction_id": "txn-....-00001",
    "common": {"subarray_id": 1},
    "lowcbf": {"scan_id": 987654321},
}

LOW_CSP_SCAN_4_0 = {
    "interface": "https://schema.skao.int/ska-low-csp-scan/4.0",
    "common": {"subarray_id": 1},
    "scan_id": 987654321,
}


# Functions returning copies of the appropriate version of LOW CSP JSON strings
def get_low_csp_assignresources_example(version: str):
    """Get appropriate version example of LOW CSP assign resources string.

    :param version: Version URI of configuration format
    """
    if version.startswith(lowcsp_assignresources_uri(2, 0)):
        return copy.deepcopy(LOW_CSP_ASSIGNRESOURCES_2_0)
    elif version.startswith(lowcsp_assignresources_uri(3, 0)):
        return copy.deepcopy(LOW_CSP_ASSIGNRESOURCES_3_0)
    elif version.startswith(lowcsp_assignresources_uri(3, 1)):
        return copy.deepcopy(LOW_CSP_ASSIGNRESOURCES_3_0)
    elif version.startswith(lowcsp_assignresources_uri(3, 2)):
        return copy.deepcopy(LOW_CSP_ASSIGNRESOURCES_3_2)
    #    elif version.startswith(lowcsp_assignresources_uri(4, 0)):
    #        return copy.deepcopy(LOW_CSP_ASSIGNRESOURCES_3_0)
    raise ValueError(f"Could not generate example for schema {version})!")


def get_low_csp_configure_example(version: str, scan_type=None):
    """Get appropriate version example of LOW CSP configure string.

    :param version: Version URI of configuration format
    """
    if version.startswith(lowcsp_configure_uri(2, 0)):
        if not scan_type:
            return copy.deepcopy(LOW_CSP_CONFIGURE_2_0)
        elif scan_type == "pst_scan_vr":
            return copy.deepcopy(
                LOW_CSP_CONFIGURE_2_0_PST_SCAN_VOLTAGE_RECORDER_2_4
            )
    elif version.startswith(lowcsp_configure_uri(3, 0)):
        if not scan_type:
            return copy.deepcopy(LOW_CSP_CONFIGURE_3_0)
        elif scan_type == "pst_scan_ft":
            return copy.deepcopy(LOW_CSP_CONFIGURE_3_0_PST_SCAN_FT_2_4)
        elif scan_type == "pst_scan_pt":
            return copy.deepcopy(LOW_CSP_CONFIGURE_3_0_PST_SCAN_PT_2_4)
        elif scan_type == "pst_scan_ds":
            return copy.deepcopy(
                LOW_CSP_CONFIGURE_3_0_PST_SCAN_DYNAMIC_SPECTRUM_2_4
            )
    elif version.startswith(lowcsp_configure_uri(3, 1)):
        if not scan_type:
            return copy.deepcopy(LOW_CSP_CONFIGURE_3_1)
        elif scan_type == "pst_scan_ft":
            return copy.deepcopy(LOW_CSP_CONFIGURE_3_1_PST_SCAN_FT_2_5)
        elif scan_type == "pst_scan_pt":
            return copy.deepcopy(LOW_CSP_CONFIGURE_3_1_PST_SCAN_PT_2_5)
        elif scan_type == "pst_scan_vr":
            return copy.deepcopy(
                LOW_CSP_CONFIGURE_3_1_PST_SCAN_VOLTAGE_RECORDER_2_5
            )
        elif scan_type == "pst_scan_ds":
            return copy.deepcopy(
                LOW_CSP_CONFIGURE_3_1_PST_SCAN_DYNAMIC_SPECTRUM_2_5
            )
    elif version.startswith(lowcsp_configure_uri(3, 2)):
        if not scan_type:
            return copy.deepcopy(LOW_CSP_CONFIGURE_3_2)
        elif scan_type == "pst_scan_vr":
            return copy.deepcopy(
                LOW_CSP_CONFIGURE_3_2_PST_SCAN_VOLTAGE_RECORDER_2_5
            )
    elif version.startswith(lowcsp_configure_uri(4, 0)):
        if not scan_type:
            return copy.deepcopy(LOW_CSP_CONFIGURE_4_0)
    raise ValueError(f"Could not generate example for schema {version})!")


def get_low_csp_releaseresources_example(version: str):
    """Get appropriate version example of LOW CSP release resources string.

    :param version: Version URI of configuration format
    """
    if version.startswith(lowcsp_releaseresources_uri(2, 0)):
        return copy.deepcopy(LOW_CSP_RELEASERESOURCES_2_0)
    elif version.startswith(lowcsp_releaseresources_uri(3, 0)):
        return copy.deepcopy(LOW_CSP_RELEASERESOURCES_3_0)
    elif version.startswith(lowcsp_releaseresources_uri(3, 1)):
        return copy.deepcopy(LOW_CSP_RELEASERESOURCES_3_0)
    elif version.startswith(lowcsp_releaseresources_uri(3, 2)):
        return copy.deepcopy(LOW_CSP_RELEASERESOURCES_3_2)
    #    elif version.startswith(lowcsp_releaseresources_uri(4, 0)):
    #        return copy.deepcopy(LOW_CSP_RELEASERESOURCES_3_0)
    raise ValueError(f"Could not generate example for schema {version})!")


def get_low_csp_scan_example(version: str):
    """Get appropriate version example of LOW CSP scan string.

    :param version: Version URI of configuration format
    """

    if version.startswith(lowcsp_scan_uri(2, 0)):
        return copy.deepcopy(LOW_CSP_SCAN_2_0)
    elif version.startswith(lowcsp_scan_uri(3, 0)):
        return copy.deepcopy(LOW_CSP_SCAN_2_0)
    elif version.startswith(lowcsp_scan_uri(3, 1)):
        return copy.deepcopy(LOW_CSP_SCAN_2_0)
    elif version.startswith(lowcsp_scan_uri(3, 2)):
        return copy.deepcopy(LOW_CSP_SCAN_3_2)
    elif version.startswith(lowcsp_scan_uri(4, 0)):
        return copy.deepcopy(LOW_CSP_SCAN_4_0)
    raise ValueError(f"Could not generate example for schema {version})!")
