import contextlib
import datetime
import glob
import logging
import os
import pathlib
import shutil
import tarfile
import tempfile

import appdirs

LOGGING = logging.getLogger(__name__)


def cache_path(name: str, env: list) -> pathlib.Path:
    """
    Composes path to use for storing cached telescope model data

    :param name: Name / path of cached data
    :param env: List of environment variables (if None uses os.environ)
    :returns: Full path for cached data
    """

    if not env:
        env = os.environ  # pragma: no cover
    cache_dir = env.get("SKA_TELMODEL_CACHE")
    if cache_dir is None:
        cache_dir = appdirs.user_cache_dir("ska-telmodel", "ska")
    return pathlib.Path(cache_dir, name)


def cache_exists(name: str, env: list) -> bool:
    """
    Check whether a certain file is cached locally

    :param path: Name / path of cached data
    :param env: List of environment variables (if None uses os.environ)
    :returns: Full path for cached data
    """

    file_name = cache_path(name, env)
    return pathlib.Path(file_name).exists()


def get_cache_time(name: str, env: list) -> datetime:
    """
    Get time a cache was created / last touched

    :param name: Name / path of cached data
    :param env: List of environment variables (if None uses os.environ)
    :returns: Datetime object with time
    """

    return datetime.datetime.fromtimestamp(
        os.path.getmtime(cache_path(name, env))
    )


def create_from_tarball(
    name: str, tarball_fd, env: list, filename_patterns=("*",)
) -> str:
    """
    Extract tarball into specific location

    Note that this assumes that the tarball has a top

    :param name: Sub-path to use for data
    :param tarball_fd: Open file descriptor of tarball
    :param strip_path: Strip a top-level directory of given name
    :param filename_patterns: Files to extract
    :param env: List of environment variables (if None uses os.environ)
    :returns: Full path where extracted tarball was placed
    """

    with tempfile.TemporaryDirectory(prefix="tmdata") as tmpdir:
        # Extract tarballs contents and move all contents into CLONE_DIRECTORY
        with contextlib.closing(tarfile.open(fileobj=tarball_fd)) as tar:
            # Get name of top-level directory, ensure that we are not
            # dealing with a tar bomb.
            names = tar.getnames()

            # Check that we have a consistent top-level directory
            top_level_dir = names[0]
            if "/" in top_level_dir or "." in top_level_dir:
                raise ValueError(
                    f"Tarball has strange top-level directory {top_level_dir}!"
                )  # pragma: no cover
            # ... and that all files (apart from the MANIFEST) are under it
            if not all(
                name.startswith(top_level_dir) or name == "MANIFEST.skao.int"
                for name in names
            ):
                raise ValueError(
                    "Tarball does not have common top-level directory!"
                )  # pragma: no cover

            # Extract into directory
            tar.extractall(path=tmpdir)

        # Move all files & directories specified
        path = cache_path(name, env)
        path.mkdir(exist_ok=True)
        for filename_pattern in filename_patterns:
            g = os.path.join(tmpdir, filename_pattern)
            for filename in glob.glob(g):
                new_path = path / pathlib.Path(filename).name
                LOGGING.debug(f"Moving {filename} -> {new_path}")
                shutil.move(filename, new_path)

        return path
