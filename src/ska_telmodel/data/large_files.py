import datetime
import getpass
import hashlib
import logging
import os
import time
from base64 import b64encode
from pathlib import Path
from sys import version_info as python_version_info

import requests
from filelock import FileLock

from .cache import cache_path

CAR_REST_PATH = os.environ.get(
    "CAR_REST_PATH", "https://artefact.skao.int/service/rest"
)
CAR_REPO = os.environ.get("CAR_REPO", "sts-608")
CAR_URL = os.environ.get("CAR_URL", "https://artefact.skao.int/repository")

logger = logging.getLogger(__name__)


def upload_large_file(repo: str, file: Path, key: str) -> str:
    hash_of_file = large_file_hash(file)
    file_extension = file.suffix
    file_exists, download_url, _ = large_file_search(
        hash_of_file, file_extension, repo
    )
    if not file_exists:
        logger.debug("File doesn't exist, so uploading")
        _, download_url = large_file_upload_to_car(file, hash_of_file, repo)
    else:
        logger.debug("File hash already exists, skipping upload")

    return large_file_link_content(
        hash_of_file, download_url, key_path=key, file_size=file.stat().st_size
    )


def large_file_hash(file: Path) -> str:
    """Calculate the hash of a file"""
    h = hashlib.sha256()
    with file.open("rb") as file_p:
        chunk = None
        while chunk != b"":
            chunk = file_p.read(1024)
            h.update(chunk)
    return h.hexdigest()


def _create_download_url(
    hash_of_file: str, file_extension: str, repo: str
) -> str:
    # Build up download path
    if not file_extension.startswith("."):
        file_extension = "." + file_extension

    return (
        f"{CAR_URL}/{CAR_REPO}/gitlab/{repo}/largefiles/"
        f"{hash_of_file}{file_extension}"
    )


def large_file_search(
    hash_of_file: str, file_extension: str, repo: str
) -> tuple[bool, str, int]:
    """Check if a file exists with the given hash on the CAR."""

    parameter_list = {"repository": CAR_REPO, "sha256": hash_of_file}
    search_response = requests.get(
        f"{CAR_REST_PATH}/v1/search/assets",
        params=parameter_list,
    )

    if search_response.status_code != 200:
        return (
            False,
            _create_download_url(hash_of_file, file_extension, repo),
            0,
        )

    response = search_response.json()
    if len(response["items"]) == 0:
        return (
            False,
            _create_download_url(hash_of_file, file_extension, repo),
            0,
        )

    return (
        True,
        response["items"][0]["downloadUrl"],
        response["items"][0]["fileSize"],
    )


def _basic_auth(user: str) -> str:
    """Create basic auth token from user."""
    token = b64encode(user.encode("utf-8")).decode("ascii")
    return f"Basic {token}"


def large_file_upload_to_car(
    file: Path, file_hash, repo: str = None
) -> tuple[bool, str]:
    """Post request to upload file to CAR."""

    user = _get_user_details()

    parameter_list = {"repository": CAR_REPO}

    url = f"{CAR_REST_PATH}/v1/components"

    payload = {
        "raw.directory": f"gitlab/{repo}/largefiles",
        "raw.asset1.filename": f"{file_hash}{file.suffix}",
    }

    files = [
        (
            "raw.asset1",
            (file.name, file.open("rb"), "application/binary"),
        )
    ]

    headers = {"Authorization": _basic_auth(user)}

    response = requests.request(
        "POST",
        url,
        params=parameter_list,
        headers=headers,
        data=payload,
        files=files,
    )

    if response.status_code != 204:
        logger.error(
            f"Error when uploading file. Status code is {response.status_code}"
        )
        return False, None

    logger.info("File uploaded successfully to CAR.")
    return True, _create_download_url(file_hash, file.suffix, repo)


def _get_user_details():
    """Obtain user details for CAR."""

    username = os.environ.get("CAR_TMDATA_USERNAME", None)
    password = os.environ.get("CAR_TMDATA_PASSWORD", None)
    if username is None or password is None:
        print("Please enter details of your SKA CAR account")
        if username is None:
            username = str(input("Username: "))
        if password is None:
            password = getpass.getpass()

    return f"{username}:{password}"


def is_large_file_cached(
    file_hash: str,
) -> tuple[bool, Path]:
    """Return if the large file is cached, and it's location"""
    download_path = cache_path(name="large_files", env=None)

    # This will also create the parents
    lock = FileLock(f"{download_path}/{file_hash}.lock", timeout=-1)
    file_path = download_path / file_hash
    with lock:
        return file_path.exists(), file_path


def large_file_download(file_hash: str, print_progress: bool = False) -> Path:
    """Download a file based on the hash to a given path,
    or the cache directory"""

    cached, file_path = is_large_file_cached(file_hash)
    if cached:
        return file_path

    # Lock is placed here so that only a single search happens
    lock = FileLock(f"{file_path}.lock", timeout=-1)
    with lock:
        # Check if this file exists in the local cache directory

        found, url, file_size = large_file_search(file_hash, "", "")
        if not found:
            logger.error("File could not be found")
            return None

        download_response = requests.get(url, stream=True, timeout=120)
        if download_response.status_code != 200:
            logger.error(f"File could not be downloaded using url: {url}")
            return None

        # We are downloading to a non-cache directory
        file_size_mb = file_size / 1024 / 1024
        chunk_size = 10 * 1024
        logger.info(
            "Downloading file hash to cache: "
            f"{file_hash[:16]} [{file_size_mb:.1f} MB]"
        )
        start = time.time()
        with file_path.open(mode="wb") as file:
            chunks = 0
            for chunk in download_response.iter_content(chunk_size=chunk_size):
                chunks += 1
                if print_progress:  # pragma: no cover
                    progress = min(
                        100, 100 * (chunks * chunk_size) / file_size
                    )
                    print(
                        f"Downloading: {progress:.1f} %",
                        end="\r",
                    )
                file.write(chunk)
        end = time.time()
        if print_progress:  # pragma: no cover
            time_taken = end - start
            data_rate = file_size / time_taken / 1024 / 1024
            print(
                "Download complete: "
                f"{time_taken:.2f} s @ {data_rate:.2f} MB/s)"
            )
        return file_path


def _get_utc_time():  # pragma: no cover
    # pylint: disable=no-member
    if python_version_info.major >= 3 and python_version_info.minor >= 11:
        return datetime.datetime.now(datetime.UTC).isoformat()
    else:
        return datetime.datetime.utcnow().isoformat()


def large_file_link_content(
    file_hash: str,
    download_url: str,
    key_path: str = None,
    file_size: int = -1,
) -> str:
    """Create the content of a link file."""
    filename = "<filename>"
    if key_path is not None:
        filename = key_path

    utc_time = _get_utc_time()
    return (
        "# This is a link file.\n"
        "# The original file is considered too large to be stored in GitLab.\n"
        "\n"
        "# To download the contents do one of the following:\n"
        f"# - use the CLI: `ska-telmodel cat {filename}`\n"
        f"# - use the CLI: `ska-telmodel cp {filename} <location>`\n"
        "# - Use the `downloadUrl` value to download the file directly\n"
        "# - Use the `ska-telmodel` library as normal (with or without the "
        "`.link` suffix)\n"
        f"# File added on {utc_time} UTC\n"
        "\n"
        f'file_hash = "{file_hash}"\n'
        f'downloadUrl = "{download_url}"\n'
        f"fileSize = {file_size}\n"
    )
