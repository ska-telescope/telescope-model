# Default sources to use.
DEFAULT_SOURCES = [
    "car:ska-telmodel-data?main",
    "car:ska-telmodel?master",
    "car:mccs/ska-low-mccs?main",
    "car:ost/ska-ost-osd?main",
    "car:sdp/ska-sdp-script?master",
]

# Default way to complete URI
# (i.e. car:[...] -> car://gitlab.com/ska-telescope/[...]#tmdata)
DEFAULT_NETLOC = "gitlab.com"
DEFAULT_PATH = "/ska-telescope/"
DEFAULT_FRAGMENT = "tmdata"

__all__ = [
    "DEFAULT_SOURCES",
    "DEFAULT_NETLOC",
    "DEFAULT_PATH",
    "DEFAULT_FRAGMENT",
]
