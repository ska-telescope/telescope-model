"""
This file contains example configuration strings for LOWCBF subarray commands
"""

import copy

from .version import (
    lowcbf_assignresources_uri,
    lowcbf_configurescan_uri,
    lowcbf_releaseresources_uri,
    lowcbf_scan_uri,
)

# Example for AssignResources command v0.1
LOWCBF_ASSIGNRESOURCES_EX_0_1 = {
    "interface": "https://schema.skao.int/ska-low-cbf-assignresources/0.0",
    "lowcbf": {
        "resources": [
            {
                "device": "fsp_01",
                "shared": True,
                "fw_image": "pst",
                "fw_mode": "unused",
            },
            {
                "device": "p4_01",
                "shared": True,
                "fw_image": "p4.bin",
                "fw_mode": "p4",
            },
        ]
    },
}
# Example for AssignResources command v0.2
LOWCBF_ASSIGNRESOURCES_EX_0_2 = {
    "interface": "https://schema.skao.int/ska-low-cbf-assignresources/0.2",
    "lowcbf": {},
}
# Example for ConfigureScan command v0.1
LOWCBF_CONFIGURESCAN_EX_0_1 = {
    "interface": "https://schema.skao.int/ska-low-cbf-configurescan/0.0",
    "lowcbf": {
        "stations": {
            "stns": [[1, 0], [2, 0], [3, 0], [4, 0]],
            "stn_beams": [
                {
                    "beam_id": 1,
                    "freq_ids": [64, 65, 66, 67, 68, 69, 70, 71],
                    "boresight_dly_poly": (
                        "tango://delays.skao.int/low/stn-beam/1"
                    ),
                }
            ],
        },
        "timing_beams": {
            "beams": [
                {
                    "pst_beam_id": 13,
                    "stn_beam_id": 1,
                    "offset_dly_poly": "url",
                    "stn_weights": [0.9, 1.0, 1.0, 0.9],
                    "jones": "url",
                    "dest_ip": ["10.22.0.1:2345", "10.22.0.3:3456"],
                    "dest_chans": [128, 256],
                    "rfi_enable": [True, True, True],
                    "rfi_static_chans": [1, 206, 997],
                    "rfi_dynamic_chans": [242, 1342],
                    "rfi_weighted": 0.87,
                }
            ]
        },
        "search_beams": "tbd",
        "zooms": "tbd",
    },
}
# Example for ConfigureScan command v0.2
LOWCBF_CONFIGURESCAN_EX_0_2 = {
    "interface": "https://schema.skao.int/ska-low-cbf-configurescan/0.2",
    "lowcbf": {
        "stations": {
            "stns": [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1]],
            "stn_beams": [
                {
                    "beam_id": 1,
                    "freq_ids": [400],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                }
            ],
        },
        "vis": {
            "fsp": {"firmware": "vis", "fsp_ids": [1]},
            "stn_beams": [
                {
                    "stn_beam_id": 1,
                    "host": [[0, "192.168.1.00"]],
                    "port": [[0, 9000, 1]],
                    "mac": [[0, "02-03-04-0a-0b-0c"]],
                    "integration_ms": 849,
                }
            ],
        },
        "timing_beams": {
            "fsp": {"firmware": "pst", "fsp_ids": [2]},
            "beams": [
                {
                    "pst_beam_id": 1,
                    "stn_beam_id": 1,
                    "stn_weights": [0.9, 1.0, 1.0, 1.0, 0.9, 1.0],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                    "jones": "tango://jones.skao.int/low/stn-beam/1",
                    "destinations": [
                        {
                            "data_host": "10.0.3.2",
                            "data_port": 9000,
                            "start_channel": 0,
                            "num_channels": 24,
                        }
                    ],
                }
            ],
        },
    },
}
# Example for ConfigureScan command v0.3
LOWCBF_CONFIGURESCAN_EX_0_3 = {
    "interface": "https://schema.skao.int/ska-low-cbf-configurescan/0.3",
    "lowcbf": {
        "stations": {
            "stns": [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1]],
            "stn_beams": [
                {
                    "beam_id": 1,
                    "freq_ids": [400],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                }
            ],
        },
        "vis": {
            "fsp": {"firmware": "vis", "fsp_ids": [1]},
            "stn_beams": [
                {
                    "stn_beam_id": 1,
                    "host": [[0, "192.168.1.00"]],
                    "port": [[0, 9000, 1]],
                    "mac": [[0, "02-03-04-0a-0b-0c"]],
                    "integration_ms": 849,
                }
            ],
        },
        "timing_beams": {
            "fsp": {"firmware": "pst", "fsp_ids": [2]},
            "beams": [
                {
                    "pst_beam_id": 1,
                    "stn_beam_id": 1,
                    "stn_weights": [0.9, 1.0, 1.0, 1.0, 0.9, 1.0],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                    "jones": "tango://jones.skao.int/low/stn-beam/1",
                    "destinations": [
                        {
                            "data_host": "10.0.3.2",
                            "data_port": 9000,
                            "start_channel": 0,
                            "num_channels": 24,
                        }
                    ],
                }
            ],
        },
        "search_beams": {
            "fsp": {"firmware": "pss", "fsp_ids": [3]},
            "beams": [
                {
                    "pss_beam_id": 1,
                    "stn_beam_id": 1,
                    "stn_weights": [0.9, 1.0, 1.0, 1.0, 0.9, 1.0],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                    "jones": "tango://jones.skao.int/low/stn-beam/1",
                    "destinations": [
                        {
                            "data_host": "10.0.3.2",
                            "data_port": 9000,
                            "start_channel": 0,
                            "num_channels": 24,
                        }
                    ],
                }
            ],
        },
    },
}
# Example for ConfigureScan command v1.0 (nakshatra)
LOWCBF_CONFIGURESCAN_EX_1_0 = {
    "interface": "https://schema.skao.int/ska-low-cbf-configurescan/1.0",
    "lowcbf": {
        "stations": {
            "stns": [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1]],
            "stn_beams": [
                {
                    "stn_beam_id": 1,
                    "freq_ids": [400],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                }
            ],
        },
        "vis": {
            "fsp": {"function_mode": "vis", "fsp_ids": [1]},
            "stn_beams": [
                {
                    "stn_beam_id": 1,
                    "host": [[0, "192.168.1.00"]],
                    "port": [[0, 9000, 1]],
                    "mac": [[0, "02-03-04-0a-0b-0c"]],
                    "integration_ms": 849,
                }
            ],
        },
        "timing_beams": {
            "fsp": {"function_mode": "pst", "fsp_ids": [2]},
            "beams": [
                {
                    "pst_beam_id": 1,
                    "stn_beam_id": 1,
                    "stn_weights": [0.9, 1.0, 1.0, 1.0, 0.9, 1.0],
                    "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                    "jones": "tango://jones.skao.int/low/stn-beam/1",
                    "destinations": [
                        {
                            "data_host": "10.0.3.2",
                            "data_port": 9000,
                            "start_channel": 0,
                            "num_channels": 24,
                        }
                    ],
                }
            ],
        },
    },
}


# Example for ReleaseResources command v0.1
LOWCBF_RELEASERESOURCES_EX_0_1 = {
    "interface": "https://schema.skao.int/ska-low-cbf-releaseresources/0.0",
    "lowcbf": {
        "resources": [
            {"device": "fsp_01"},
        ]
    },
}

# Example for ReleaseResources command v0.2
LOWCBF_RELEASERESOURCES_EX_0_2 = {
    "interface": "https://schema.skao.int/ska-low-cbf-releaseresources/0.2",
    "lowcbf": {},
}

# Example for Scan command v0.1 and v0.2
LOWCBF_SCAN_EX_0_1 = {
    "interface": "https://schema.skao.int/ska-low-cbf-scan/0.0",
    "lowcbf": {
        "scan_id": 1357924680,
    },
}


# Functions returning copies of the appropriate version of LOWCBF JSON strings
def get_lowcbf_assignresources_example(version: str):
    """Get appropriate version example of LOWCBF assign resources string.

    :param version: Version URI of configuration format
    """
    if version.startswith(lowcbf_assignresources_uri(0, 1)):
        return copy.deepcopy(LOWCBF_ASSIGNRESOURCES_EX_0_1)
    if version.startswith(lowcbf_assignresources_uri(0, 2)):
        return copy.deepcopy(LOWCBF_ASSIGNRESOURCES_EX_0_2)
    if version.startswith(lowcbf_assignresources_uri(0, 3)):
        return copy.deepcopy(LOWCBF_ASSIGNRESOURCES_EX_0_2)  # 0.3 & 0.2 same

    raise ValueError(f"Could not generate example for schema {version})!")


def get_lowcbf_configurescan_example(version: str):
    """Get appropriate version example of LOWCBF configurescan string.

    :param version: Version URI of configuration format
    """
    if version.startswith(lowcbf_configurescan_uri(0, 1)):
        return copy.deepcopy(LOWCBF_CONFIGURESCAN_EX_0_1)
    elif version.startswith(lowcbf_configurescan_uri(0, 2)):
        return copy.deepcopy(LOWCBF_CONFIGURESCAN_EX_0_2)
    elif version.startswith(lowcbf_configurescan_uri(0, 3)):
        return copy.deepcopy(LOWCBF_CONFIGURESCAN_EX_0_3)
    elif version.startswith(lowcbf_configurescan_uri(1, 0)):
        return copy.deepcopy(LOWCBF_CONFIGURESCAN_EX_1_0)

    raise ValueError(f"Could not generate example for schema {version})!")


def get_lowcbf_releaseresources_example(version: str):
    """Get appropriate version example of LOWCBF release resources string.

    :param version: Version URI of configuration format
    """
    if version.startswith(lowcbf_releaseresources_uri(0, 1)):
        return copy.deepcopy(LOWCBF_RELEASERESOURCES_EX_0_1)
    if version.startswith(lowcbf_releaseresources_uri(0, 2)):
        return copy.deepcopy(LOWCBF_RELEASERESOURCES_EX_0_2)
    if version.startswith(lowcbf_releaseresources_uri(0, 3)):
        return copy.deepcopy(LOWCBF_RELEASERESOURCES_EX_0_2)  # 0.3 & 0.2 same

    raise ValueError(f"Could not generate example for schema {version})!")


def get_lowcbf_scan_example(version: str):
    """Get appropriate version example of LOWCBF scan string.

    :param version: Version URI of configuration format
    """
    if version.startswith(lowcbf_scan_uri(0, 1)):
        return copy.deepcopy(LOWCBF_SCAN_EX_0_1)
    if version.startswith(lowcbf_scan_uri(0, 2)):
        return copy.deepcopy(LOWCBF_SCAN_EX_0_1)
    if version.startswith(lowcbf_scan_uri(0, 3)):
        return copy.deepcopy(LOWCBF_SCAN_EX_0_1)  # 0.3 & 0.1 same

    raise ValueError(f"Could not generate example for schema {version})!")
