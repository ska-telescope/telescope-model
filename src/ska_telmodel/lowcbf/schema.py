"""
This file contains functions that can be called to create a schema for LOWCBF
subarray commands, then the schema can be used to check JSON input to the
command.
"""
from inspect import cleandoc

from .._common import TMSchema, split_interface_version
from ..skydirection import get_skydirection
from .version import (  # lowcbf_scan_uri, # unused, all scan versions same
    lowcbf_assignresources_uri,
    lowcbf_configurescan_uri,
    lowcbf_releaseresources_uri,
)


def get_lowcbf_assignresources_schema(version: str, strict: bool) -> TMSchema:
    """SKA LOWCBF Assign Resources schema

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: TMSchema
    """
    items = TMSchema.new("LOWCBF assign resources", version, strict)
    items.add_field(
        "interface",
        str,
        description="URI of JSON schema for this command's JSON payload.",
    )
    schema = _get_lowcbf_assignresources_schema(version, strict)

    items.add_field(
        "lowcbf",
        schema,
        description="LOWCBF assign resources",
    )
    return items


def _get_lowcbf_assignresources_schema(version: str, strict: bool) -> TMSchema:
    """Retrieve assign resources schema without interface information.

    This method is invoked by Low CSP to build-up the
    assignresources schema for TM-CSP interface.

    :param version: Interface Version URI
    :param strict: Schema strictness
    """

    return (
        get_lowcbf_assignresources_v1(version, strict)
        if version.startswith(lowcbf_assignresources_uri(0, 1))
        else get_lowcbf_assignresources_v2(version, strict)
    )


def get_lowcbf_assignresources_v2(version: str, strict: bool) -> TMSchema:
    """SKA LOWCBF Assign Resources schema for version 0.2

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: Schema
    """
    items = TMSchema.new(
        "LOWCBF assign resources", version, strict, as_reference=True
    )
    items.add_opt_field(
        "dummy_param",
        str,
        description="LOWCBF assign resources (unused, empty)",
    )
    return items


def get_lowcbf_assignresources_v1(version: str, strict: bool) -> TMSchema:
    """SKA LOWCBF Assign Resources schema for version 0.1

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: TMSchema
    """
    # Description of each individual resource
    lowcbf_resource_elems = TMSchema.new(
        "LOWCBF resources", version, strict, as_reference=True
    )
    lowcbf_resource_elems.add_field(
        "device", str, description="Name of FSP or P4 device"
    )
    lowcbf_resource_elems.add_field(
        "shared",
        bool,
        description="Whether device is shared with other subarrays",
    )
    lowcbf_resource_elems.add_opt_field(
        "fw_image", str, description="Name of firmware image to load on device"
    )
    lowcbf_resource_elems.add_opt_field(
        "fw_mode", str, description="Mode in which firmware runs"
    )

    # Description of array holding multiple resources
    lowcbf_resource_blk = TMSchema.new("LOWCBF resources", version, strict)
    lowcbf_resource_blk.add_field(
        "resources",
        [lowcbf_resource_elems],
        description="array of LOWCBF resources",
    )
    return lowcbf_resource_blk


def get_lowcbf_configurescan_schema(version: str, strict: bool) -> TMSchema:
    """SKA LOWCBF configuration schema

    :param version: Interface Version URI
    :param strict: Schema strictness

    :return: TMSchema
    """
    # if_strict = mk_if(strict)

    items = TMSchema.new(
        "LOWCBF configurescan",
        version,
        strict,
    )
    items.add_field(
        "interface",
        str,
        description="URI of JSON schema for this command's JSON payload.",
    )
    schema = _get_lowcbf_configurescan_schema(
        version,
        strict,
        address_required=True,
    )
    items.add_field(
        "lowcbf",
        schema,
        description="LOWCBF configuration for scan",
    )
    return items


def _get_lowcbf_configurescan_schema(
    version: str,
    strict: bool,
    address_required=True,
) -> TMSchema:
    """Retrieve configure resources schema without interface information.

    This method is invoked by by both Low CSP and Low TMC to create the
    configuration schema for the TMC-CSP interface and the OSO-TMC interface,
    respectively.

    :param version: Interface Version URI
    :param strict: Schema strictness
    :param address_required: whether the PST beams addresses have to
        be specified in the schema. This information is mandatory for
        Low CBF when it works in function_mode=PST.
    :return: TMSchema
    """

    lowcbf = TMSchema.new(
        "LOWCBF subarray configurescan",
        version,
        strict,
        description=cleandoc(
            """
            Correlator and Beamformer specific parameters. This section
            contains the parameters relevant only for CBF
            sub-system. This section is forwarded only to CBF
            subelement.
            """
        ),
        as_reference=True,
    )
    lowcbf.add_field(
        "stations",
        get_all_stn_beams_descr(version, strict),
        description="Subarray Stations and station beam input" "descriptions",
    )
    lowcbf.add_opt_field(
        "timing_beams",
        get_pst_beam_descr_outer(version, strict, address_required),
        description="PST beam outputs descriptions",
    )
    iface_ver = split_interface_version(version)
    if iface_ver in [(0, 1), (0, 2), (1, 0)]:
        # original versions without PSS details
        lowcbf.add_opt_field(
            "search_beams",
            str,  # contents TBD
            description="PSS beam outputs descriptions",
        )
    else:
        lowcbf.add_opt_field(
            "search_beams",
            get_pss_beam_descr_outer(version, strict, address_required),
            description="PSS beam outputs descriptions",
        )
    cbf_version = split_interface_version(version) == (0, 1)
    lowcbf.add_opt_field(
        ("visibilities" if cbf_version else "vis"),
        get_vis_descr_outer(version, strict),
        description="Visibility output descriptions",
    )
    lowcbf.add_opt_field(
        "zooms",
        str,  # contents TBD
        description="Zoom visibility output descriptions",
    )
    return lowcbf


def get_all_stn_beams_descr(
    version: str,
    strict: bool,
) -> TMSchema:
    """Low.CBF station beams description schema.

    :param version: Interface Version URI
    :param strict: Schema strictness
    """
    stn_beam_descr = TMSchema.new("Subarray station beams", version, strict)
    # modify key-word from beam_id to stn_beam_id in version >= 1.0
    cbf_version = split_interface_version(version) < (1, 0)
    stn_beam_descr.add_field(
        ("beam_id" if cbf_version else "stn_beam_id"),
        int,
        description="station beam id",
    )
    stn_beam_descr.add_field(
        "freq_ids",
        [
            int,
        ],
        description="list of station beam frequency ids",
    )
    if version.startswith(lowcbf_configurescan_uri(0, 1)):
        stn_beam_descr.add_field("boresight_dly_poly", str, description="URL")
    else:
        stn_beam_descr.add_field("delay_poly", str, description="URL")
    all_stn_beams_descr = TMSchema.new(
        "Subarray stations and station beams",
        version,
        strict,
        description=cleandoc("Station and station beams parameters"),
        as_reference=True,
    )
    all_stn_beams_descr.add_field(
        "stns",
        [
            [int, int],  # ie [station_id, substation_id],
        ],
        description="",
    )
    all_stn_beams_descr.add_field(
        "stn_beams",
        [
            stn_beam_descr,
        ],
        description="",
    )
    return all_stn_beams_descr


def get_pst_dest(version: str, strict: bool) -> TMSchema:
    """Description of PST host destination - IP, port, channels sent
    :param version: Interface Version URI
    :param strict: Schema strictness

    :return: the schema with PST destination addresses.
    """
    pst_dest = TMSchema.new("PST beams description", version, strict)
    pst_dest.add_field("data_host", str, description="dotted ipv4 address")
    pst_dest.add_field("data_port", int, description="UDP port number")
    pst_dest.add_field("start_channel", int, description="first chan to host")
    pst_dest.add_field("num_channels", int, description="no. chans to host")
    return pst_dest


def get_pst_beam_descr_outer(
    version: str, strict: bool, address_required: bool = True
) -> TMSchema:
    """Low.CBF PST beam description schema.

    :param version: Interface Version URI
    :param strict: Schema strictness
    :param address_required: whether the PST beams addresses have to
        be specified in the schema. This information is mandatory for
        Low CBF when it works in function_mode=PST.
    :return: Schema
    """
    pst_beam_descr_outer = TMSchema.new(
        "outer", version, strict, as_reference=True
    )
    pst_beam_descr = TMSchema.new(
        "PST beams description", version, strict, as_reference=True
    )
    pst_beam_descr.add_field(
        "stn_beam_id", int, description="Station beam ID for pst beamforming"
    )
    pst_beam_descr.add_field("pst_beam_id", int, description="PST beam ID")
    pst_beam_descr.add_opt_field(
        "field",
        # setting as_reference=True causes issues duplicate reference in the
        # documentation
        get_skydirection(version, strict, as_reference=False),
        description="field description",
    )
    pst_beam_descr.add_field(
        "jones", str, description="Jones matrix source URI"
    )
    pst_beam_descr.add_field(
        "stn_weights",
        [
            float,
        ],
        description="weights for each station",
    )
    pst_beam_descr.add_opt_field(
        "rfi_enable",
        [
            bool,
        ],
        description="Master enable for RFI flagging",
    )
    pst_beam_descr.add_opt_field(
        "rfi_static_chans",
        [
            int,
        ],
        description="Freqency IDs to be always flagged",
    )
    pst_beam_descr.add_opt_field(
        "rfi_dynamic_chans",
        [
            int,
        ],
        description="Frequency IDs to be dynamically flagged",
    )
    pst_beam_descr.add_opt_field(
        "rfi_weighted", float, description="Parameter for dynamic flagging"
    )
    if version.startswith(lowcbf_configurescan_uri(0, 1)):
        _get_pst_beam_descr_outer_v1(
            pst_beam_descr, address_required=address_required
        )
    else:
        _get_pst_beam_descr_outer_v2(
            version, strict, pst_beam_descr, address_required
        )

    pst_beam_descr_outer.add_field(
        "beams",
        [
            pst_beam_descr,
        ],
        description="inner",
    )
    _add_fsp_description_to_schema(version, strict, pst_beam_descr_outer)

    return pst_beam_descr_outer


def get_pss_beam_descr_outer(
    version: str, strict: bool, address_required: bool = True
) -> TMSchema:
    """Low.CBF PSS beam description schema.

    :param version: Interface Version URI - note that
        v0.1, v0.2, v1.0 never call here
    :param strict: Schema strictness
    :param address_required: whether the PSS beams addresses have to
        be specified in the schema. This information is mandatory for
        Low CBF when it works in function_mode=PSS.
    :return: Schema
    """
    pss_beam_descr_outer = TMSchema.new(
        "outer", version, strict, as_reference=True
    )
    pss_beam_descr = TMSchema.new(
        "PSS beams description", version, strict, as_reference=True
    )
    pss_beam_descr.add_field(
        "stn_beam_id", int, description="Station beam ID for pst beamforming"
    )
    pss_beam_descr.add_field("pss_beam_id", int, description="PSS beam ID")
    pss_beam_descr.add_field(
        "jones", str, description="Jones matrix source URI"
    )
    pss_beam_descr.add_field(
        "stn_weights",
        [
            float,
        ],
        description="weights for each station",
    )
    pss_beam_descr.add_opt_field(
        "rfi_enable",
        [
            bool,
        ],
        description="Master enable for RFI flagging",
    )
    pss_beam_descr.add_opt_field(
        "rfi_static_chans",
        [
            int,
        ],
        description="Freqency IDs to be always flagged",
    )
    pss_beam_descr.add_opt_field(
        "rfi_dynamic_chans",
        [
            int,
        ],
        description="Frequency IDs to be dynamically flagged",
    )
    pss_beam_descr.add_opt_field(
        "rfi_weighted", float, description="Parameter for dynamic flagging"
    )

    # TODO separate this for PSS when we have details
    _get_pst_beam_descr_outer_v2(
        version, strict, pss_beam_descr, address_required
    )

    pss_beam_descr_outer.add_field(
        "beams",
        [
            pss_beam_descr,
        ],
        description="inner",
    )
    _add_fsp_description_to_schema(version, strict, pss_beam_descr_outer)

    return pss_beam_descr_outer


def _add_fsp_description_to_schema(
    version: str, strict: bool, schema: TMSchema
) -> None:
    """
    Add the field with the FSP configuration parmeters.

    :param version: Interface Version URI
    :param strict: Schema strictness
    :param schema: The schema to add the current field
    """
    if not version.startswith(lowcbf_configurescan_uri(0, 1)):
        schema.add_field(
            "fsp",
            get_fsp_descr(version, strict),
            description="FSPs used by PST",
        )


def _get_pst_beam_descr_outer_v1(
    pst_beam_descr: TMSchema, address_required: bool = True
) -> None:
    """
    Add the PST beam descritpion to the main configuration schema.

    :param pst_beam_descr: The schema to add the current field.
    :param address_required: whether the PST beams addresses have to
       be specified in the schema. This information is mandatory for
       Low CBF when it works in function_mode=PST.
    """
    pst_beam_descr.add_opt_field("firmware", str, description="Firmware name")
    pst_beam_descr.add_field(
        "offset_dly_poly", str, description="Delay polynomial source URI"
    )
    if address_required:
        pst_beam_descr.add_field(
            "dest_ip",
            [
                str,
            ],
            description="Beam destination [ip_addr:port]",
        )
        pst_beam_descr.add_field(
            "dest_chans",
            [
                int,
            ],
            description="Number of fine chans to a destination",
        )


def _get_pst_beam_descr_outer_v2(
    version: str,
    strict: str,
    pst_beam_descr: TMSchema,
    address_required: bool = True,
):
    """
    Add the fields with the PST beam descritpion to the main configuration
    schema.

    :param version: Interface Version URI
    :param strict: Schema strictness
    :param pst_beam_descr: The schema to add the current field.
    :param address_required: whether the PST beams addresses have to
       be specified in the schema. This information is mandatory for
       Low CBF when it works in function_mode=PST.
    """

    pst_beam_descr.add_field(
        "delay_poly", str, description="Delay polynomial source URI"
    )
    if address_required:
        pst_beam_descr.add_field(
            "destinations",
            [
                get_pst_dest(version, strict),
            ],
            description="PST server addrs",
        )


def get_fsp_descr(version: str, strict: bool) -> TMSchema:
    """Low.CBF FSP description schema.

    :param version: Interface Version URI
    :param strict: Schema strictness
    """
    fsp_descr = TMSchema.new("FSP", version, strict)
    cbf_version = split_interface_version(version) < (1, 0)
    # nakshatra support
    fsp_descr.add_field(
        ("firmware" if cbf_version else "function_mode"),
        str,
        description="Firmware name",
    )
    fsp_descr.add_field(
        "fsp_ids",
        [
            int,
        ],
        description="List of IDs (integer)",
    )

    return fsp_descr


def get_vis_stn_beams(version: str, strict: bool) -> TMSchema:
    """Low.CBF schema for station beams from which to calculate visibilities.

    :param version: Interface Version URI
    :param strict: Schema strictness
    """
    vis_stn_beams = TMSchema.new(
        "Station beams to correlate", version, strict, as_reference=True
    )
    vis_stn_beams.add_field("stn_beam_id", int, description="Station Beam ID")
    vis_stn_beams.add_field(
        "integration_ms", int, description="milliseconds integration"
    )
    vis_stn_beams.add_field(
        "host",
        [
            [int, str],
        ],
        description="SDP channel & IP Address",
    )
    vis_stn_beams.add_field(
        "port",
        [
            [int, int, int],
        ],
        description="SDP chan & UDP port, stride",
    )
    vis_stn_beams.add_opt_field(
        "mac",
        [
            [int, str],
        ],
        description="SDP channel & server MAC",
    )
    return vis_stn_beams


def get_vis_descr_outer(version: str, strict: bool) -> TMSchema:
    """Low.CBF visibilities schema.

    :param version: Interface Version URI
    :param strict: Schema strictness
    """
    vis_outer = TMSchema.new("Visibilities description", version, strict)
    vis_outer.add_field(
        "fsp",
        get_fsp_descr(version, strict),
        description="FSPs used for correlation",
    )
    vis_outer.add_field(
        "stn_beams",
        [
            get_vis_stn_beams(version, strict),
        ],
        description="SDP visibility destinations",
    )
    return vis_outer


def get_lowcbf_releaseresources_schema(version: str, strict: bool) -> TMSchema:
    """SKA LOWCBF release resources schema

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: TMSchema
    """
    # if_strict = mk_if(strict)

    items = TMSchema.new("LOWCBF configurescan", version, strict)
    items.add_field(
        "interface",
        str,
        description="URI of JSON schema for this command's JSON payload.",
    )
    schema = _get_lowcbf_releaseresources_schema(version, strict)
    items.add_field(
        "lowcbf",
        schema,
        description="LOWCBF configuration for scan",
    )
    return items


def _get_lowcbf_releaseresources_schema(
    version: str, strict: bool
) -> TMSchema:
    """Retrieve releaseresources schema without interface information.

    This method is invoked by Low CSP to build-up the
    releaseresources schema for TMC-CSP interface.

    :param version: Interface Version URI
    :param strict: Schema strictness
    """

    return (
        get_lowcbf_releaseresources_v1(version, strict)
        if version.startswith(lowcbf_releaseresources_uri(0, 1))
        else get_lowcbf_releaseresources_v2(version, strict)
    )


def get_lowcbf_releaseresources_v2(version: str, strict: bool) -> TMSchema:
    """SKA LOWCBF releasereources schema version 0.2

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: Schema
    """
    items = TMSchema.new(
        "LOWCBF release resources", version, strict, as_reference=True
    )
    items.add_opt_field(
        "dummy_param",
        {},
        description="LOWCBF dummy string param (unused, empty)",
    )
    return items


def get_lowcbf_releaseresources_v1(version: str, strict: bool) -> TMSchema:
    """SKA LOWCBF releasereources schema version 0.1

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: Schema
    """
    # Description of each individual resource
    lowcbf_resource_elems = TMSchema.new(
        "LOWCBF resources", version, strict, as_reference=True
    )
    lowcbf_resource_elems.add_field(
        "device", str, description="Name of FSP or P4 device"
    )

    # Description of array holding multiple resources
    lowcbf_resource_blk = TMSchema.new("LOWCBF resources", version, strict)
    lowcbf_resource_blk.add_field(
        "resources",
        [
            lowcbf_resource_elems,
        ],
        description="array of LOWCBF resources",
    )

    return lowcbf_resource_blk


def get_lowcbf_scan_schema(version: str, strict: bool) -> TMSchema:
    """SKA LOWCBF scan schema

    Currently same schema for any version of interface

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: Schema
    """
    scan_descr = _get_lowcbf_scan_schema(version, strict)

    items = TMSchema.new("LOWCBF scan", version, strict)
    items.add_field(
        "interface",
        str,
        description="URI of JSON schema for this command's JSON payload..",
    )
    items.add_field("lowcbf", scan_descr, description="LOWCBF scan arguments")
    return items


def _get_lowcbf_scan_schema(version: str, strict: bool) -> TMSchema:
    """Retrieve scan schema without interface information.

    This method is invoked by Low CSP to build-up the
    scan schema for TMC-CSP interface.

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: Schema
    """
    scan_descr = TMSchema.new(
        "LOWCBF scan description", version, strict, as_reference=True
    )
    scan_descr.add_field("scan_id", int, description="Scan ID")

    return scan_descr
