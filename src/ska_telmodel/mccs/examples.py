import copy

from .version import (
    mccs_assignedres_uri,
    mccs_assignres_uri,
    mccs_configure_uri,
    mccs_releaseres_uri,
    mccs_scan_uri,
)

MCCS_ASSIGNEDRES_1_0 = {
    "interface": (
        "https://schema.skatelescope.org/ska-low-mccs-assignedresources/1.0"
    ),
    "subarray_beam_ids": [1],
    "station_ids": [[1, 2]],
    "channel_blocks": [3],
}

MCCS_ASSIGNEDRES_EMPTY_1_0 = {
    "interface": (
        "https://schema.skatelescope.org/ska-low-mccs-assignedresources/1.0"
    ),
    "subarray_beam_ids": [],
    "station_ids": [],
    "channel_blocks": [],
}

MCCS_ASSIGNRES_1_0 = {
    "interface": (
        "https://schema.skatelescope.org/ska-low-mccs-assignresources/1.0"
    ),
    "subarray_id": 1,
    "subarray_beam_ids": [1],
    "station_ids": [[1, 2]],
    "channel_blocks": [3],
}

MCCS_RELEASERES_1_0 = {
    "interface": (
        "https://schema.skatelescope.org/ska-low-mccs-releaseresources/1.0"
    ),
    "subarray_id": 1,
    "release_all": True,
}

MCCS_CONFIGURE_1_0 = {
    "interface": "https://schema.skatelescope.org/ska-low-mccs-configure/1.0",
    "stations": [{"station_id": 1}, {"station_id": 2}],
    "subarray_beams": [
        {
            "subarray_beam_id": 1,
            "station_ids": [1, 2],
            "update_rate": 0.0,
            "channels": [[0, 8, 1, 1], [8, 8, 2, 1], [24, 16, 2, 1]],
            "sky_coordinates": [0.0, 180.0, 0.0, 45.0, 0.0],
            "antenna_weights": [1.0, 1.0, 1.0],
            "phase_centre": [0.0, 0.0],
        }
    ],
}

MCCS_SCAN_1_0 = {
    "interface": "https://schema.skatelescope.org/ska-low-mccs-scan/1.0",
    "scan_id": 1,
    "start_time": 0.0,
}


def get_mccs_assignedres_example(version: str, empty: bool = False):
    """Generate examples for MCCS assigned resources strings.

    :param version: Version URI of configuration format
    :param empty: True if example is for empty sub-array
    """
    if version.startswith(mccs_assignedres_uri(1, 0)):
        if empty:
            return copy.deepcopy(MCCS_ASSIGNEDRES_EMPTY_1_0)
        return copy.deepcopy(MCCS_ASSIGNEDRES_1_0)

    raise ValueError(f"Could not generate example for schema {version})!")


def get_mccs_assignres_example(version: str):
    """Generate examples for MCCS assign resources strings.

    :param version: Version URI of configuration format
    """
    if version.startswith(mccs_assignres_uri(1, 0)):
        return copy.deepcopy(MCCS_ASSIGNRES_1_0)

    raise ValueError(f"Could not generate example for schema {version})!")


def get_mccs_releaseres_example(version: str):
    """Generate examples for MCCS resource release strings.

    :param version: Version URI of configuration format
    """
    if version.startswith(mccs_releaseres_uri(1, 0)):
        return copy.deepcopy(MCCS_RELEASERES_1_0)

    raise ValueError(f"Could not generate example for schema {version})!")


def get_mccs_configure_example(version: str):
    """Generate examples for MCCS configuration strings.

    :param version: Version URI of configuration format
    """
    if version.startswith(mccs_configure_uri(1, 0)):
        return copy.deepcopy(MCCS_CONFIGURE_1_0)

    raise ValueError(f"Could not generate example for schema {version})!")


def get_mccs_scan_example(version: str):
    """Generate examples for MCCS scan strings.

    :param version: Version URI of configuration format
    """
    if version.startswith(mccs_scan_uri(1, 0)):
        return copy.deepcopy(MCCS_SCAN_1_0)

    raise ValueError(f"Could not generate example for schema {version})!")
