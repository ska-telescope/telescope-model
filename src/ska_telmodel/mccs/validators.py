"""
mccs.validators module defines constants and functions for
validating MCCS fields in schemas.
"""

from typing import Dict, List

MAX_NO_SUBARRAY_BEAMS = 48
MAX_NO_LOGICAL_BANDS = 48
MAX_NO_STATIONS = 512
MAX_FIRST_SUBARRAY_CHANNEL = 376
MAX_NO_SUBARRAY_BEAM_IDS = 768

MAX_NO_APERATURES = 512

MAX_NO_CHANNEL_BLOCKS = 48
MAX_START_CHANNEL = 376
MIN_NO_CHANNELS = 8
MAX_NO_CHANNELS = 48
MAX_NO_HARDWARE_BEAM = 48

MAX_ANTENNA_WEIGHT = 256.0  # maximum number of antennas in a beam
MAX_NO_ANTENNA_WEIGHTS = 512  # 256 antennas per beam * 2 pol per station

MIN_PHASE_CENTRE = -20.0
MAX_PHASE_CENTRE = 20.0

MIN_START_CHANNEL_VALUE = 2
MAX_START_CHANNEL_VALUE = 504

MIN_NUMBER_CHANNEL_VALUE = 8
MAX_NUMBER_CHANNEL_VALUE = 384

MIN_FIRST_COORDINATE_VALUE = 0.0
MAX_FIRST_COORDINATE_VALUE = 360.0

MIN_SECOND_COORDINATE_VALUE = -90.0
MAX_SECOND_COORDINATE_VALUE = 90.0


def validate_subarray_id(subarray_id: int) -> bool:
    """Check the sub-array ID is valid.

    :param subarray_id: Sub-array ID to validate
    :returns: True if sub-array ID is valid
    :rtype: bool
    """
    return 1 <= subarray_id <= MAX_NO_SUBARRAY_BEAMS


def validate_subarray_beam_id(subarray_beam_id: int) -> bool:
    """Check the sub-array beam ID is valid.

    :param subarray_beam_id: Sub-array beam ID to validate
    :returns: True if sub-array beam ID is valid
    :rtype: bool
    """
    return 1 <= subarray_beam_id <= MAX_NO_SUBARRAY_BEAMS


def validate_subarray_beam_ids(subarray_beam_ids: List[int]) -> bool:
    """Check the number of sub-array beam IDs is valid.

    :param subarray_beam_ids: List of sub-array beam IDs
    :returns: True if the number of IDs is within allowed range
    :rtype: bool
    """
    return len(subarray_beam_ids) <= 1


def validate_station_id(station_id: int) -> bool:
    """Check the station ID is valid.

    :param station_id: Station ID to validate
    :returns: True if station ID is valid
    :rtype: bool
    """
    return 1 <= station_id <= MAX_NO_STATIONS


def validate_channel_blocks(channel_blocks: List[int]) -> bool:
    """Check the number of channel blocks is valid.

    :param channel_blocks: List of channel blocks
    :returns: True if the number of blocks is within allowed range
    :rtype: bool
    """
    return len(channel_blocks) < MAX_NO_CHANNEL_BLOCKS


def validate_stations(stations: List[Dict]) -> bool:
    """Check the number of stations is valid.

    :param stations: List of stations
    :returns: True if the number of stations is within allowed range
    :rtype: bool
    """
    return len(stations) <= MAX_NO_STATIONS


def validate_channel(channel: List[int]) -> bool:
    """Check the sub-array beam ID is valid.

    :param channel: List defining values for the channel
    :returns: True if all channel values are valid
    :rtype: bool
    """
    return (
        len(channel) == 4
        and channel[0] % 8 == 0
        and 0 <= channel[0] <= MAX_START_CHANNEL
        and MIN_NO_CHANNELS <= channel[1] <= MAX_NO_CHANNELS
        and 1 <= channel[2] <= MAX_NO_SUBARRAY_BEAMS
        and 1 <= channel[3] <= 8
    )


def validate_channels(channels: List[List[int]]) -> bool:
    """Check the number of defined channels is valid.

    :param channels: List of channel definitions
    :returns: True if the number of channels is within allowed range
    :rtype: bool
    """
    return len(channels) <= MAX_NO_CHANNEL_BLOCKS


def validate_update_rate(update_rate: float) -> bool:
    """Check the update rate is valid.

    :param update_rate: Update rate value to validate
    :returns: True if value is valid
    :rtype: bool
    """
    return update_rate >= 0.0


def validate_antenna_weight(antenna_weight: float) -> bool:
    """Check the antenna weight is valid.

    :param antenna_weight: Antenna weight value to validate
    :returns: True if value is valid
    :rtype: bool
    """
    return 0.0 <= antenna_weight <= MAX_ANTENNA_WEIGHT


def validate_antenna_weights(antenna_weights: List[float]) -> bool:
    """Check the number of antenna weights is valid.

    :param antenna_weights: List of antenna weights
    :returns: True if the number of weights is within allowed range
    :rtype: bool
    """
    return len(antenna_weights) <= MAX_NO_ANTENNA_WEIGHTS


def validate_phase_centre_value(phase_centre_value: float) -> bool:
    """Check the phase centre value is valid.

    :param phase_centre_value: Phase centre value to validate
    :returns: True if value is valid
    :rtype: bool
    """
    return MIN_PHASE_CENTRE <= phase_centre_value <= MAX_PHASE_CENTRE


def validate_phase_centre(phase_centre: List[float]) -> bool:
    """Check the phase centre is defined by allowed number of values.

    :param phase_centre: List of phase centre values
    :returns: True if the number of values is within allowed range
    :rtype: bool
    """
    return len(phase_centre) <= 2


def validate_scan_start_time(start_time: float) -> bool:
    """Check that the MCCS scan start time is positive.

    :param start_time: scan start time (units unknown)
    :returns True: if the start time is valid
    :rtype: bool
    """
    return start_time >= 0.0


def validate_start_channel(start_channel_value: int) -> bool:
    """
    Check that the MCCS start channel value is
    in correct range.

    :param start_channel_value: int
    :returns True: if the start_channel_value is valid
    :rtype: bool
    """
    return (
        MIN_START_CHANNEL_VALUE
        <= start_channel_value
        <= MAX_START_CHANNEL_VALUE
    )


def validate_number_of_channel(number_of_channel: int) -> bool:
    """
    Check that the MCCS number of channels value is
    in correct range.

    :param number_of_channel: int
    :returns True: if the number_of_channel is valid
    :rtype: bool
    """
    return (
        MIN_NUMBER_CHANNEL_VALUE
        <= number_of_channel
        <= MAX_NUMBER_CHANNEL_VALUE
    )


def validate_aperture_id(aperture_id: str) -> bool:
    """
    Check that the MCCS aperture value is
    in correct format.

    :param aperture_id: str
    :returns True: if the aperture_id is valid
    :rtype: bool
    """
    return len(aperture_id) == 8 and aperture_id[:2] == "AP"


def validate_first_coordinate(first_coordinate: str) -> bool:
    """
    Check that the MCCS first coordinate value is
    in correct range.

    :param first_coordinate: str
    :returns True: if the first_coordinate is valid
    :rtype: bool
    """
    return (
        MIN_FIRST_COORDINATE_VALUE
        <= first_coordinate
        <= MAX_FIRST_COORDINATE_VALUE
    )


def validate_second_coordinate(second_coordinate: str) -> bool:
    """
    Check that the MCCS second coordinate value is
    in correct range.

    :param second_coordinate: str
    :returns True: if the second_coordinate is valid
    :rtype: bool
    """
    return (
        MIN_SECOND_COORDINATE_VALUE
        <= second_coordinate
        <= MAX_SECOND_COORDINATE_VALUE
    )


def validate_reference_frame(reference_frame: str) -> bool:
    """
    Check that the MCCS reference frame value is valid.

    :param reference_frame: str
    :returns True: if the reference_frame is valid
    :rtype: bool
    """
    return reference_frame in ["topocentric", "ICRS", "galactic"]


def validate_low_subarray_beam_ids(subarray_beam_id: int) -> bool:
    """Check the sub-array beam ID is valid.

    :param subarray_beam_id: Sub-array beam ID to validate
    :returns: True if sub-array beam ID is valid
    :rtype: bool
    """
    return 1 <= subarray_beam_id <= MAX_NO_SUBARRAY_BEAM_IDS
