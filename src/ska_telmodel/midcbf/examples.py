from .version import midcbf_initsysparam_uri


def get_midcbf_initsysparam_1_0(format: str) -> dict:
    if format is None:
        initsysparam_1_0 = {
            "interface": (
                "https://schema.skao.int/ska-midcbf-initsysparam/1.0"
            ),
            "dish_parameters": {
                "SKA001": {"vcc": 1, "k": 11},
                "SKA100": {"vcc": 2, "k": 101},
                "SKA036": {"vcc": 3, "k": 1127},
                "SKA063": {"vcc": 4, "k": 620},
            },
        }
    elif format == "uri":
        initsysparam_1_0 = {
            "interface": (
                "https://schema.skao.int/ska-mid-cbf-initsysparam/1.0"
            ),
            "tm_data_sources": ["car:ska-telmodel-data?1.0.0#tmdata"],
            "tm_data_filepath": "instrument/ska1_mid_psi/"
            + "ska-mid-cbf-system-parameters.json",
        }
    else:
        raise ValueError(
            "Could not generate initsysparam "
            + f"example for format {format})!"
        )

    return initsysparam_1_0


def get_midcbf_initsysparam_1_1(format: str) -> dict:
    initsysparam_1_1 = get_midcbf_initsysparam_1_0(format)
    initsysparam_1_1[
        "interface"
    ] = "https://schema.skao.int/ska-midcbf-initsysparam/1.1"
    return initsysparam_1_1


def get_midcbf_initsysparam_example(version: str, format: str = None) -> dict:
    """Generate examples for MID CBF InitSysParam

    :param version: Version URI of configuration format
    :param empty: True if example is for empty sub-array
    """
    if version.startswith(midcbf_initsysparam_uri(1, 0)):
        return get_midcbf_initsysparam_1_0(format)
    elif version.startswith(midcbf_initsysparam_uri(1, 1)):
        return get_midcbf_initsysparam_1_1(format)
    raise ValueError(f"Could not generate example for schema {version})!")
