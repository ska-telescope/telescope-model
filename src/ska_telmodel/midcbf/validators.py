"""
midcbf.validators module defines constants and functions for
validating Mid.CBF fields in schemas.
"""


def validate_unique_dish_mapping_values(dish_parameters_dict: dict) -> bool:
    """Checks if the dish parameters contain all unique vcc IDs and k values.

    :param dish_parameters_dict: The dict containing the dish parameters
    """

    vcc_id_to_dish_id = {}
    for dish_id, dish_mapping in dish_parameters_dict.items():
        curr_vcc_id = dish_mapping["vcc"]

        # if the current vcc ID value is already assigned to a dish ID
        # then the values are not unique and validation should fail.
        if curr_vcc_id in vcc_id_to_dish_id:
            raise Exception(
                f"Dish {dish_id} cannot use vcc ID {curr_vcc_id}."
                + "Already assigned to dish {vcc_id_to_dish_id[curr_vcc_id]}"
            )

        vcc_id_to_dish_id[curr_vcc_id] = dish_id

    return True
