"""
This file defines the URI stem for each MIDCBF command schema
and the functions to append major, minor version indicators to the schema URI
given, as a list, the [major, minor] version integers
"""

from functools import partial

from .._common import interface_uri

MIDCBF_INITSYSPARAM_PREFIX = (
    "https://schema.skao.int/ska-mid-cbf-initsysparam/"
)

# Functions to create interface URI for each command, each function
# taking a list argument containing major,minor version integers to be
# incorporated into the URI
midcbf_initsysparam_uri = partial(interface_uri, MIDCBF_INITSYSPARAM_PREFIX)
