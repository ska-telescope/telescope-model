from schema import Or

from .._common import TMSchema, split_interface_version


def get_pss_config_schema(
    version: str,
    strict: bool,
) -> TMSchema:
    """Pulsar Search specific items

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON Schema for the PSS configuration.
    """

    major_pss, minor_pss = split_interface_version(version)

    elems = TMSchema.new("PSS configuration schema", version, strict)

    if (major_pss, minor_pss) < (0, 1):
        elems.add_opt_field("dummy_param", str)
    else:
        _get_pss_config_schema(elems, version, strict)
    return elems


# TODO Rename this here and in `get_pss_config_schema`
def _get_pss_config_schema(
    elems: TMSchema, version: str, strict: bool
) -> None:
    """
    Build the PSS configuration schema for newer versions.

    :parm elems: the main TMSchema.
    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON Schema for the PSS configuration.
    """

    elems.add_field(
        "interface",
        str,
        description=("URI of JSON schema for this command's JSON payload.."),
    )

    elems.add_field("beam", [get_pss_beam_config_schema(version, strict)])

    major, minor = split_interface_version(version)

    if (major, minor) >= (1, 0):
        elems.add_field(
            "beams", [get_pss_beams_config_schema(version, strict)]
        )
        elems.add_field("ddtr", get_pss_ddtr_config_schema(version, strict))
        elems.add_field(
            "id",
            int,
            description="ID.",
        )
        elems.add_field("sps", get_pss_sps_config_schema(version, strict))


def get_pss_beam_config_schema(version: str, strict: bool) -> TMSchema:
    """Pulsar Search Beam specific items

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON Schema for the PSS beam configuration.
    """

    elems = TMSchema.new("PSS beam config", version, strict, as_reference=True)
    elems.add_field(
        "beam_id",
        int,
        check_strict=lambda n: n >= 1 and n <= 1500,
        description="Search Beam ID.",
    )
    elems.add_opt_field(
        "ra",
        float,
        description="Right Ascension of sub-array beam target, in degrees.",
    )
    elems.add_opt_field(
        "dec",
        float,
        description="Declination of sub-array beam target, in degrees.",
    )
    elems.add_opt_field(
        "reference_frame",
        str,
        check_strict=Or("ICRS", "HORIZON"),
        description="reference frame for pointing coordinates",
    )
    elems.add_field(
        "centre_frequency",
        float,
        description="Centre frequency of the search beam.",
    )
    elems.add_field(
        "beam_delay_centre",
        Or(float, str),
        description="Beam delay center, relative to the array delay center.",
    )
    elems.add_opt_field(
        "dest_host",
        str,
        description="Per beam destination host address for PSS output.",
    )
    elems.add_opt_field(
        "dest_port",
        int,
        description="Per beam destination port for PSS output.",
    )

    return elems


def get_pss_beams_config_schema(version: str, strict: bool) -> TMSchema:
    """Pulsar Search Beams specific items

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON Schema for the PSS beams configuration.
    """

    elems = TMSchema.new(
        "PSS beams config", version, strict, as_reference=True
    )
    elems.add_field("beam", get_pss_pss_beam_config_schema(version, strict))

    return elems


def get_pss_pss_beam_config_schema(version: str, strict: bool) -> TMSchema:
    """Pulsar Search PSS Beam specific items

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON Schema for the PSS pss_beam configuration.
    """

    elems = TMSchema.new(
        "PSS pss_beam config", version, strict, as_reference=True
    )
    elems.add_field(
        "active",
        bool,
        description="enable this beam.",
    )

    elems.add_field("sinks", get_pss_sinks_config_schema(version, strict))
    elems.add_field("source", get_pss_source_config_schema(version, strict))
    elems.add_field(
        "id",
        int,
        description="id.",
    )

    return elems


def get_pss_source_config_schema(version: str, strict: bool) -> TMSchema:
    """Pulsar Search source specific items

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON Schema for the PSS source configuration.
    """

    elems = TMSchema.new(
        "PSS source config", version, strict, as_reference=True
    )
    elems.add_field("sigproc", get_pss_sigproc_config_schema(version, strict))

    return elems


def get_pss_sigproc_config_schema(version: str, strict: bool) -> TMSchema:
    """Pulsar Search sigproc specific items

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON Schema for the PSS sigproc configuration.
    """

    elems = TMSchema.new(
        "PSS sigproc config", version, strict, as_reference=True
    )
    elems.add_field(
        "file",
        str,
        description="specify the sigproc file(s) to read as input data.",
    )
    elems.add_field(
        "chunk_samples",
        int,
        description="the number of time samples in each chunk.",
    )
    elems.add_field(
        "default-nbits",
        int,
        description="specify the default number of bits to use.",
    )

    return elems


def get_pss_sinks_config_schema(version: str, strict: bool) -> TMSchema:
    """Pulsar Search Sinks specific items

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON Schema for the PSS sinks configuration.
    """

    elems = TMSchema.new(
        "PSS sinks config", version, strict, as_reference=True
    )
    elems.add_field(
        "channels", get_pss_channels_config_schema(version, strict)
    )
    elems.add_field(
        "sink_configs", get_pss_sink_configs_config_schema(version, strict)
    )
    return elems


def get_pss_sink_configs_config_schema(version: str, strict: bool) -> TMSchema:
    """Pulsar Search sink_configs specific items

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON Schema for the sink_configs configuration.
    """

    elems = TMSchema.new(
        "PSS sink_configs config", version, strict, as_reference=True
    )
    elems.add_field(
        "spccl_files", get_pss_spccl_files_config_schema(version, strict)
    )
    elems.add_field(
        "spccl_sigproc_files",
        get_pss_spccl_sigproc_files_config_schema(version, strict),
    )

    return elems


def get_pss_spccl_sigproc_files_config_schema(
    version: str, strict: bool
) -> TMSchema:
    """Pulsar Search spccl_sigproc_files specific items

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON Schema for the spccl_sigproc_files configuration.
    """

    elems = TMSchema.new(
        "PSS spccl_sigproc_files config", version, strict, as_reference=True
    )
    elems.add_field(
        "spectra_per_file",
        int,
        description="spectra per file.",
    )
    elems.add_field(
        "dir",
        str,
        description="directory.",
    )
    elems.add_field(
        "extension",
        str,
        description="extension.",
    )
    elems.add_field(
        "candidate_window",
        get_pss_candidate_window_config_schema(version, strict),
    )
    elems.add_field(
        "id",
        str,
        description="id.",
    )

    return elems


def get_pss_candidate_window_config_schema(
    version: str, strict: bool
) -> TMSchema:
    """Pulsar Search candidate_window specific items

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON Schema for the candidate_window configuration.
    """

    elems = TMSchema.new(
        "PSS candidate_window config", version, strict, as_reference=True
    )
    elems.add_field(
        "ms_before",
        float,
        description="Number of milliseconds before the candidate start.",
    )
    elems.add_field(
        "ms_after",
        float,
        description="Number of milliseconds after the candidate end time.",
    )

    return elems


def get_pss_spccl_files_config_schema(version: str, strict: bool) -> TMSchema:
    """Pulsar Search spccl_files specific items

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON Schema for the spccl_files configuration.
    """

    elems = TMSchema.new(
        "PSS spccl_files config", version, strict, as_reference=True
    )
    elems.add_field(
        "extension",
        str,
        description="extension.",
    )
    elems.add_field(
        "dir",
        str,
        description="directory.",
    )
    elems.add_field(
        "id",
        str,
        description="id.",
    )

    return elems


def get_pss_channels_config_schema(version: str, strict: bool) -> TMSchema:
    """Pulsar Search Channels specific items

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON Schema for the channels configuration.
    """

    elems = TMSchema.new(
        "PSS channels config", version, strict, as_reference=True
    )
    elems.add_field(
        "sps_events", get_pss_sps_events_config_schema(version, strict)
    )

    return elems


# optional field
def get_pss_sps_events_config_schema(version: str, strict: bool) -> TMSchema:
    """Pulsar Search sps_events specific items

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON Schema for the sps_events configuration.
    """

    elems = TMSchema.new(
        "PSS sps_events config", version, strict, as_reference=True
    )
    elems.add_field(
        "active",
        bool,
        description="activate the channel.",
    )
    elems.add_field(
        "sink",
        [{"id": str}],
        description="activate the channel.",
    )

    return elems


def get_pss_ddtr_config_schema(version: str, strict: bool) -> TMSchema:
    """Pulsar Search DDTR specific items

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON Schema for the PSS ddtr configuration.
    """

    elems = TMSchema.new("PSS ddtr config", version, strict, as_reference=True)

    elems.add_field(
        "astroaccelerate",
        TMSchema(
            {
                "active": bool,
                "copy_dmtrials_to_host": bool,
            }
        ),
        description="An nvidia CUDA gpu algorithm for dedispersion.",
    )
    elems.add_field(
        "cpu",
        TMSchema(
            {
                "active": bool,
            },
            strict=strict,
        ),
        description="dedispersion of time frequency data on CPU.",
    )
    elems.add_field(
        "fpga",
        TMSchema(
            {
                "active": bool,
            }
        ),
        description="Dedispersion of time frequency data on CPU.",
    )
    elems.add_field(
        "gpu_bruteforce",
        TMSchema(
            {
                "active": bool,
                "copy_dmtrials_to_host": bool,
            }
        ),
        description="A GPU brute force algorithm to perform DDTR.",
    )
    elems.add_field(
        "klotski",
        TMSchema(
            {
                "active": bool,
            }
        ),
        description="A Klotski algorithm to perform DDTR.",
    )
    elems.add_field(
        "klotski_bruteforce",
        TMSchema(
            {
                "active": bool,
            }
        ),
        description="A Klotski brute force algorithm to perform DDTR.",
    )
    elems.add_field(
        "dedispersion", [get_pss_dedispersion_config_schema(version, strict)]
    )

    return elems


def get_pss_sps_config_schema(version: str, strict: bool) -> TMSchema:
    """Pulsar Search SPS specific items

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON Schema for the PSS ddtr configuration.
    """

    elems = TMSchema.new("PSS sps config", version, strict, as_reference=True)

    elems.add_field(
        "cpu",
        TMSchema(
            {
                "active": bool,
                "samples_per_iteration": int,
                "number_of_widths": int,
            }
        ),
        description="dedispersion of time frequency data on CPU.",
    )
    elems.add_field(
        "threshold",
        float,
        check_strict=lambda n: n >= 8.0,
        description="single pulse detection threshold in sigmas.",
    )
    elems.add_field("klotski", get_pss_klotski_config_schema(version, strict))
    elems.add_field(
        "klotski_bruteforce", get_pss_klotski_config_schema(version, strict)
    )

    return elems


def get_pss_klotski_config_schema(version: str, strict: bool) -> TMSchema:
    """Pulsar Search Klotski specific items

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON Schema for the PSS ddtr configuration.
    """

    elems = TMSchema.new(
        "PSS klotski config", version, strict, as_reference=True
    )
    elems.add_field(
        "active",
        bool,
        description="Enable klotski sps mode.",
    )
    elems.add_field(
        "pulse_widths",
        [int],
        check_strict=lambda x: all(1 <= n <= 16383 for n in x),
        description="widths.",
    )
    return elems


def get_pss_dedispersion_config_schema(version: str, strict: bool) -> TMSchema:
    """Pulsar Search Dedispersion specific items

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON Schema for the PSS dedispersion configuration.
    """

    elems = TMSchema.new(
        "PSS dedispersion config", version, strict, as_reference=True
    )
    elems.add_field(
        "start",
        float,
        description="start DM in cm^-3 pc.",
    )
    elems.add_field(
        "end",
        float,
        description="end DM in cm^-3 pc (inclusive).",
    )
    elems.add_field(
        "step",
        float,
        description="DM step size in cm^-3 pc.",
    )

    return elems
