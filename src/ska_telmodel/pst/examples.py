from .low_examples import get_low_pst_config_example
from .mid_examples import get_mid_pst_config_example
from .version import check_interface_version


def get_pst_config_example(version: str, scan_type: str = None) -> dict:
    """Generate examples for PST configuration strings

    This will delegate to the appropriate telescope example (i.e. Mid or Low)
    depending on the prefix of the ``scan_type`` parameter. If the
    ``scan_type`` is prefixed with ``mid_`` then a SKAMid PST scan config
    example is returned else a SKALow PST scan config example is returned.

    Valid values of ``scan_type`` are:
        * pst_scan_vr
        * pst_scan_pt
        * pst_scan_ft
        * pst_scan_ds
        * low_pst_scan_vr
        * low_pst_scan_pt
        * low_pst_scan_ft
        * low_pst_scan_ds
        * mid_pst_scan_vr
        * mid_pst_scan_pt
        * mid_pst_scan_ft
        * mid_pst_scan_ds

    :param version: Version URI of configuration format
    :param scan: Includes SDP receive addresses for a scan? `None`
       means that this is "template" configuration as passed to
       TMC.
    """
    check_interface_version(version)

    if scan_type.startswith("mid_"):
        return get_mid_pst_config_example(version, scan_type)
    else:
        return get_low_pst_config_example(version, scan_type)
