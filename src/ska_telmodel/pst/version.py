from functools import partial
from typing import List, Union

from .._common import interface_uri, split_interface_version

PST_CONFIGURE_PREFIX = "https://schema.skao.int/ska-pst-configure/"

PST_CONFIG_VER0 = PST_CONFIGURE_PREFIX + "0"
PST_CONFIG_VER2_4 = PST_CONFIGURE_PREFIX + "2.4"
PST_CONFIG_VER2_5 = PST_CONFIGURE_PREFIX + "2.5"

PST_CONFIG_VERSIONS = sorted(
    [PST_CONFIG_VER2_4, PST_CONFIG_VER2_5],
    key=split_interface_version,
)

_ALLOWED_URI_PREFIXES = [
    PST_CONFIGURE_PREFIX,
]


pst_configure_uri = partial(interface_uri, PST_CONFIGURE_PREFIX)


def check_interface_version(
    version: str,
    allowed_prefixes: Union[str, List[str]] = _ALLOWED_URI_PREFIXES,
) -> str:
    """
    Check PST interface version.

    Checks that the interface URI has one of the allowed prefixes. If it does,
    the version number is returned. If not, a ValueError exception is raised.

    :param version: PST interface URI
    :param allowed_prefixes: allowed URI prefix(es)
    :returns: version number

    """
    if not isinstance(allowed_prefixes, list):
        allowed_prefixes = [allowed_prefixes]

    # Valid?
    for prefix in allowed_prefixes:
        if version.startswith(prefix):
            number = version[len(prefix) :]
            return number

    raise ValueError(f"PST interface URI not allowed '{version}'")
