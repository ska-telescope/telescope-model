"""SDP schema 0.1 examples."""

import copy

__all__ = [
    "get_sdp_recvaddrs",
]

SDP_RECVADDRS = {
    "scanId": 1,
    "totalChannels": 7,
    "receiveAddresses": [
        {
            "phaseBinId": 0,
            "fspId": 1,
            "hosts": [
                {
                    "host": "192.168.0.0",
                    "channels": [
                        {
                            "portOffset": 9153,
                            "startChannel": 153,
                            "numChannels": 1,
                        },
                        {
                            "portOffset": 9273,
                            "startChannel": 273,
                            "numChannels": 1,
                        },
                        {
                            "portOffset": 9313,
                            "startChannel": 313,
                            "numChannels": 1,
                        },
                        {
                            "portOffset": 9529,
                            "startChannel": 529,
                            "numChannels": 1,
                        },
                        {
                            "portOffset": 9665,
                            "startChannel": 665,
                            "numChannels": 1,
                        },
                        {
                            "portOffset": 9681,
                            "startChannel": 681,
                            "numChannels": 2,
                        },
                    ],
                }
            ],
        }
    ],
}


def get_sdp_recvaddrs(version: str):
    return copy.deepcopy(SDP_RECVADDRS)
