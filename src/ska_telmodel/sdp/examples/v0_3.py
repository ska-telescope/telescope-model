"""SDP schema 0.3 examples."""

import copy

from .v0_2 import get_sdp_recvaddrs

__all__ = [
    "get_sdp_assignres",
    "get_sdp_configure",
    "get_sdp_recvaddrs",
    "get_sdp_scan",
]

SDP_ASSIGNRES = {
    "eb_id": "eb-mvp01-20210623-00000",
    "max_length": 100.0,
    "scan_types": [
        {
            "scan_type_id": "science",
            "reference_frame": "ICRS",
            "ra": "02:42:40.771",
            "dec": "-00:00:47.84",
            "channels": [
                {
                    "count": 744,
                    "start": 0,
                    "stride": 2,
                    "freq_min": 0.35e9,
                    "freq_max": 0.368e9,
                    "link_map": [[0, 0], [200, 1], [744, 2], [944, 3]],
                },
                {
                    "count": 744,
                    "start": 2000,
                    "stride": 1,
                    "freq_min": 0.36e9,
                    "freq_max": 0.368e9,
                    "link_map": [[2000, 4], [2200, 5]],
                },
            ],
        },
        {
            "scan_type_id": "calibration",
            "reference_frame": "ICRS",
            "ra": "12:29:06.699",
            "dec": "02:03:08.598",
            "channels": [
                {
                    "count": 744,
                    "start": 0,
                    "stride": 2,
                    "freq_min": 0.35e9,
                    "freq_max": 0.368e9,
                    "link_map": [[0, 0], [200, 1], [744, 2], [944, 3]],
                },
                {
                    "count": 744,
                    "start": 2000,
                    "stride": 1,
                    "freq_min": 0.36e9,
                    "freq_max": 0.368e9,
                    "link_map": [[2000, 4], [2200, 5]],
                },
            ],
        },
    ],
    "processing_blocks": [
        {
            "pb_id": "pb-mvp01-20210623-00000",
            "workflow": {
                "kind": "realtime",
                "name": "vis_receive",
                "version": "0.1.0",
            },
            "parameters": {},
        },
        {
            "pb_id": "pb-mvp01-20210623-00001",
            "workflow": {
                "kind": "realtime",
                "name": "test_realtime",
                "version": "0.1.0",
            },
            "parameters": {},
        },
        {
            "pb_id": "pb-mvp01-20210623-00002",
            "workflow": {"kind": "batch", "name": "ical", "version": "0.1.0"},
            "parameters": {},
            "dependencies": [
                {"pb_id": "pb-mvp01-20210623-00000", "kind": ["visibilities"]}
            ],
        },
        {
            "pb_id": "pb-mvp01-20210623-00003",
            "workflow": {"kind": "batch", "name": "dpreb", "version": "0.1.0"},
            "parameters": {},
            "dependencies": [
                {"pb_id": "pb-mvp01-20210623-00002", "kind": ["calibration"]}
            ],
        },
    ],
}

SDP_CONFIGURE = {"scan_type": "science"}

SDP_CONFIGURE_NEW_SCAN = {
    "new_scan_types": [
        {
            "scan_type_id": "new_calibration",
            "channels": [
                {
                    "count": 372,
                    "start": 0,
                    "stride": 2,
                    "freq_min": 0.35e9,
                    "freq_max": 0.358e9,
                    "link_map": [[0, 0], [200, 1]],
                }
            ],
        }
    ],
    "scan_type": "new_calibration",
}

SDP_SCAN = {"scan_id": 1}


def get_sdp_assignres(version: str):
    return copy.deepcopy(SDP_ASSIGNRES)


def get_sdp_configure(version: str, scan_type: str = "science"):
    if scan_type == "new_calibration":
        return copy.deepcopy(SDP_CONFIGURE_NEW_SCAN)
    else:
        config = copy.deepcopy(SDP_CONFIGURE)
        config["scan_type"] = scan_type
        return config


def get_sdp_scan(version: str, scan_id: int = 1):
    scan = copy.deepcopy(SDP_SCAN)
    scan["scan_id"] = scan_id
    return scan
