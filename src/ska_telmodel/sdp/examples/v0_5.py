"""SDP schema 0.5 examples."""

import copy

from .v0_4 import SDP_RECVADDRS as SDP_RECVADDRS_0_4
from .v0_4 import (
    get_sdp_assignres,
    get_sdp_configure,
    get_sdp_releaseres,
    get_sdp_scan,
)

__all__ = [
    "get_sdp_assignres",
    "get_sdp_configure",
    "get_sdp_recvaddrs",
    "get_sdp_releaseres",
    "get_sdp_scan",
]

SDP_RECVADDRS = copy.deepcopy(SDP_RECVADDRS_0_4)
SDP_RECVADDRS["science"]["vis0"][
    "pointing_cal"
] = "tango://low-sdp/queueconnector/01/pointing_offsets_{dish_id}"
SDP_RECVADDRS["calibration"]["vis0"][
    "pointing_cal"
] = "tango://low-sdp/queueconnector/01/pointing_offsets_{dish_id}"
SDP_RECVADDRS["science"]["vis0"]["delay_cal"] = "low-sdp/telstate/rcal0/delay"
SDP_RECVADDRS["calibration"]["vis0"][
    "delay_cal"
] = "low-sdp/telstate/rcal0/delay"


def get_sdp_recvaddrs(version: str):
    return copy.deepcopy(SDP_RECVADDRS)
