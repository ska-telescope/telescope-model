"""SDP schema 1.0 examples."""

import copy

from .v0_4 import SDP_ASSIGNRES as SDP_ASSIGNRES_0_4
from .v0_5 import (
    get_sdp_configure,
    get_sdp_recvaddrs,
    get_sdp_releaseres,
    get_sdp_scan,
)

__all__ = [
    "get_sdp_assignres",
    "get_sdp_configure",
    "get_sdp_scan",
    "get_sdp_recvaddrs",
    "get_sdp_releaseres",
]

FIELDS = [
    {
        "field_id": "field_a",
        "phase_dir": {
            "target_name": "target_a",
            "reference_frame": "icrs",
            "attrs": {
                "c1": 201.365,
                "c2": -43.0191667,
            },
        },
        "pointing_fqdn": "low-tmc/telstate/0/pointing",
    },
    {
        "field_id": "field_b",
        "phase_dir": {
            "target_name": "Zenith",
            "reference_frame": "altaz",
            "attrs": {
                "c1": 180.0,
                "c2": 90.0,
            },
        },
        "pointing_fqdn": "low-tmc/telstate/0/pointing",
    },
    {
        "field_id": "field_c",
        "phase_dir": {
            "target_name": "Cass-A",
            "reference_frame": "galactic",
            "attrs": {
                "c1": 111.734745,
                "c2": -02.129570,
            },
        },
        "pointing_fqdn": "low-tmc/telstate/0/pointing",
    },
    {
        "field_id": "field_d",
        "phase_dir": {
            "target_name": "Sun",
            "reference_frame": "special",
        },
        "pointing_fqdn": "low-tmc/telstate/0/pointing",
    },
    {
        "field_id": "field_e",
        "phase_dir": {
            "target_name": "ISS (ZARYA)",
            "reference_frame": "tle",
            "attrs": {
                "line1": (
                    "1 25544U 98067A   08264.51782528 -.00002182  00000-0 "
                    "-11606-4 0  2927"
                ),
                "line2": (
                    "2 25544  51.6416 247.4627 0006703 130.5360 325.0288 "
                    "15.72125391563537"
                ),
            },
        },
        "pointing_fqdn": "low-tmc/telstate/0/pointing",
    },
]

SDP_ASSIGNRES = copy.deepcopy(SDP_ASSIGNRES_0_4)
SDP_ASSIGNRES["execution_block"]["fields"] = FIELDS


def get_sdp_assignres(version: str):
    return copy.deepcopy(SDP_ASSIGNRES)
