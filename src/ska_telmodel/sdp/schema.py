"""SDP schema definitions."""

from .._common import lookup_schema
from . import schemas


def get_sdp_recvaddrs_schema(version: str, strict: int):
    return lookup_schema(schemas, version, strict, "get_sdp_recvaddrs")


def get_sdp_scan_schema(version: str, strict: int):
    return lookup_schema(schemas, version, strict, "get_sdp_scan")


def get_sdp_configure_schema(version: str, strict: int):
    return lookup_schema(schemas, version, strict, "get_sdp_configure")


def get_sdp_assignres_schema(version: str, strict: int):
    return lookup_schema(schemas, version, strict, "get_sdp_assignres")


def get_sdp_releaseres_schema(version: str, strict: int):
    return lookup_schema(schemas, version, strict, "get_sdp_releaseres")
