"""Version 0.4 of SDP schema"""

from inspect import cleandoc

from schema import Literal, Optional, Schema

from ska_telmodel._common import TMSchema, get_channel_map_schema
from ska_telmodel.sdp.common import (
    get_beam_function_pattern,
    get_eb_name_schema,
    get_sbi_name_schema,
    get_txn_name_schema,
)

from . import v0_3
from .v0_1 import get_external_resources_schema, get_spectral_window
from .v0_2 import RECVADDRS_DESCRIPTION
from .v0_3 import assign_res_common, get_sdp_configure, get_sdp_scan

__all__ = [
    "get_sdp_assignres",
    "get_sdp_configure",
    "get_sdp_scan",
    "get_sdp_recvaddrs",
]


def get_scan_channels(version: str, strict: bool) -> TMSchema:
    schema = TMSchema.new(
        "Scan channels",
        version,
        strict,
        description="Spectral windows per channel configuration.",
        schema={
            "channels_id": Schema(str),
        },
        as_reference=True,
    )
    schema.add_field(
        "spectral_windows", [get_spectral_window(version, strict)]
    )
    return schema


def get_scan_type(version: str, strict: bool) -> TMSchema:
    schema = TMSchema.new(
        "Scan type",
        version,
        strict,
        schema={
            "scan_type_id": str,
            Optional("derive_from"): str,
            # More here?
            Optional("beams"): dict,
        },
    )
    return schema


def get_processing_block(version: str, strict: bool) -> TMSchema:
    schema = v0_3.get_processing_block(version, strict, script_name="script")
    schema.add_opt_field(
        "sbi_ids",
        [get_sbi_name_schema(version, strict)],
        description=cleandoc(
            """
            Scheduling block instances that the processing block
            belongs to.
            """
        ),
    )
    return schema


def get_beam(version: str, strict: bool) -> TMSchema:
    # Can/should this be tied down more?
    schema = TMSchema.new(
        "Beam",
        version,
        strict,
        schema={},
        description=cleandoc(
            """
            Beam parameters for the purpose of the Science Data
            Processor.
            """
        ),
        as_reference=True,
    )
    schema.add_field(
        "beam_id",
        str,
        description="Name to identify the beam within the SDP configuration.",
    )
    schema.add_field(
        "function",
        get_beam_function_pattern(strict),
        description=cleandoc(
            """
            Identifies the type and origin of the generated beam data.  This
            corresponds to a certain kind of calibration or receive
            functionality SDP is meant to provide for it.

            Possible options:

            * `visibilities`: Correlated voltages from CBF used for
              calibration and imaging
            * `pulsar search`: SDP provides calibrations for tied-array
              beam as well as post-processes and delivers pulsar search
              data products
            * `pulsar timing`: SDP provides calibrations for tied-array
              beam as well as post-processes and delivers pulsar timing
              data products
            * `vlbi`: SDP provides calibrations for tied-array
              beam
            * `transient buffer`: SDP receives and delivers transient
              buffer data dumps
            """
        ),
    )
    for beam_type in ("search", "timing", "vlbi", "visibility"):
        schema.add_opt_field(f"{beam_type}_beam_id", int)
    return schema


def get_polarisation(version: str, strict: bool) -> TMSchema:
    return TMSchema.new(
        "polarisation",
        version,
        strict,
        schema={"polarisations_id": str, "corr_type": [str]},
        description="Polarisation definition.",
    )


def get_phase_dir(version: str, strict: bool) -> TMSchema:
    return TMSchema.new(
        "phase_dir",
        version,
        strict,
        schema={
            "ra": [Schema(lambda x: 0.0 <= x < 360.0)],
            "dec": [Schema(lambda x: -90.0 <= x <= 90.0)],
            "reference_time": str,
            "reference_frame": "ICRF3",
        },
        description="Phase direction",
    )


def get_field(version: str, strict: bool) -> TMSchema:
    return TMSchema.new(
        "fields",
        version,
        strict,
        schema={
            "field_id": str,
            "phase_dir": get_phase_dir(version, strict),
            Optional("pointing_fqdn"): str,
        },
        description="Fields / Targets",
    )


def get_execution_block(version: str, strict: bool) -> TMSchema:
    schema = TMSchema.new(
        "Execution block",
        version,
        strict,
        schema={
            "eb_id": get_eb_name_schema(version, strict),
            "max_length": float,
            Literal(
                "context",
                description="Free-form information from OET, see ADR-54",
            ): Schema(dict),
        },
        as_reference=True,
    )
    schema.add_field(
        "beams", [get_beam(version, strict)], description="Beam parameters"
    )
    schema.add_field(
        "scan_types",
        [get_scan_type(version, strict)],
        description=cleandoc(
            """
            Scan types. Associates scans with per-beam
            fields & channel configurations
            """
        ),
    ),
    schema.add_field(
        "channels",
        [get_scan_channels(version, strict)],
        description="Channels",
    )
    schema.add_field(
        "polarisations",
        [get_polarisation(version, strict)],
        description="Polarisation definitions",
    )
    schema.add_field(
        "fields", [get_field(version, strict)], description="Fields / targets"
    )
    return schema


def get_sdp_assignres(version: str, strict: bool) -> TMSchema:
    schema = assign_res_common(version, strict)
    schema.add_opt_field(
        "execution_block",
        get_execution_block(version, strict),
        description="Execution block",
    )
    schema.add_opt_field(
        "resources",
        get_external_resources_schema(version, strict),
        description="External resources",
    )
    schema.add_opt_field(
        "processing_blocks",
        [get_processing_block(version, strict)],
        description="Processing blocks",
    )
    return schema


def get_sdp_releaseres(version: str, strict: bool) -> TMSchema:
    schema = TMSchema.new(
        "SDP release resources",
        version,
        strict,
        description="Used for releasing resources for an SDP subarray.",
    )
    schema.add_opt_field("interface", str)
    schema.add_opt_field(
        "transaction_id", get_txn_name_schema(version, strict)
    )
    schema.add_field(
        "resources", get_external_resources_schema(version, strict)
    )
    return schema


def get_host() -> Literal:
    return Literal(
        "host",
        description=cleandoc(
            """
            Destination host names (as channel map)

            Note that these are not currently guaranteed to be IP
            addresses, so a DNS resolution  might be required.
            """
        ),
    )


def get_port() -> Literal:
    return Literal(
        "port",
        description="Destination ports (as channel map)",
    )


def get_recvaddrs_beam(version: str, strict: bool) -> TMSchema:
    schema = TMSchema.new(
        "Beam receive addresses",
        version,
        strict,
        schema={
            get_host(): get_channel_map_schema(str, 0, strict),
            get_port(): get_channel_map_schema(int, 0, strict),
        },
        description="Receive addresses associated with a certain beam",
        as_reference=True,
    )
    schema.add_opt_field(
        "mac",
        get_channel_map_schema(str, 0, strict),
        description=cleandoc(
            """
            Destination MAC addresses (as channel map)

            Likely not going to be used, downstream systems should use
            ARP to determine the MAC address using ``host`` instead. See ADR-36
            """
        ),
    )
    schema.add_field(
        "function",
        get_beam_function_pattern(strict),
        description=cleandoc(
            """
            Type of beam configured. Beam identity is then given by the
            appropriate `beam_id` field.
            """
        ),
    )
    schema.add_opt_field(
        "visibility_beam_id",
        int,
        description=cleandoc(
            """
            Identifies visibility beam

            Might get omitted for SKA Mid, as it is assumed to have
            only one visibility beam.
            """
        ),
    )
    schema.add_opt_field(
        "search_beam_id", int, description="Identifies pulsar search beam"
    )
    schema.add_opt_field(
        "timing_beam_id", int, description="Identifies pulsar timing beam"
    )
    schema.add_opt_field(
        "vlbi_beam_id",
        int,
        description="Identifies very long baseline interferometry beam",
    )
    schema.add_opt_field(
        "search_window_id",
        int,
        description="Identifies search window for transient data capture",
    )
    schema.add_opt_field(
        "jones_cal",
        get_channel_map_schema(str, 0, strict),
        description=cleandoc(
            """
            Tango FQDNs serving real-time calibration
            Jones matrices for CBF
            """
        ),
    )

    schema.add_opt_field(
        "delay_cal",
        get_channel_map_schema(str, 0, strict),
        description=cleandoc(
            """
            Tango FQDNs serving gain/
            delay calibration solutions for TMC
            """
        ),
    )

    return schema


def get_recvaddrs_beams(version: str, strict: bool) -> TMSchema:
    schema = TMSchema.new(
        "Beams",
        version,
        strict,
        description="Set of beams",
    )
    schema.add_field(
        str,
        get_recvaddrs_beam(version, strict),
        description="Beam",
    )
    return schema


def get_sdp_recvaddrs(version: str, strict: bool) -> TMSchema:
    return TMSchema.new(
        "SDP receive addresses map",
        version,
        strict,
        schema={
            Optional("interface"): str,
            str: get_recvaddrs_beams(version, strict),
        },
        description=RECVADDRS_DESCRIPTION,
    )
