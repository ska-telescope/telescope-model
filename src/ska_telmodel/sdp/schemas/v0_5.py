"""Version 0.5 of SDP schema"""

from inspect import cleandoc

from ska_telmodel._common import TMSchema

from . import v0_4
from .v0_4 import (
    get_sdp_assignres,
    get_sdp_configure,
    get_sdp_releaseres,
    get_sdp_scan,
)

__all__ = [
    "get_sdp_assignres",
    "get_sdp_configure",
    "get_sdp_scan",
    "get_sdp_recvaddrs",
    "get_sdp_releaseres",
]


def get_sdp_recvaddrs(version: str, strict: bool) -> TMSchema:
    # Get 0.4 version
    schema = v0_4.get_sdp_recvaddrs(version, strict)
    beam_schema = schema[str][str]

    beam_schema.add_opt_field(
        "pointing_cal",
        str,
        description=cleandoc(
            """
            Tango FQDNs serving pointing calibration
            offsets for TMC
            """
        ),
    )

    # Change type of delay_cal
    del beam_schema["delay_cal"]
    beam_schema.add_opt_field(
        "delay_cal",
        str,
        description=cleandoc(
            """
            Tango FQDNs serving gain/
            delay calibration solutions for TMC
            """
        ),
    )

    return schema
