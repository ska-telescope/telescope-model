import copy
import json
import logging

import pytest

from ska_telmodel._common import split_interface_version
from ska_telmodel.csp import make_csp_config
from ska_telmodel.csp.examples import get_csp_config_example
from ska_telmodel.csp.low_examples import get_low_csp_configure_example
from ska_telmodel.csp.low_version import LOWCSP_CONFIGURE_PREFIX
from ska_telmodel.csp.version import csp_config_versions
from ska_telmodel.schema import example_by_uri
from ska_telmodel.sdp.version import (
    SDP_RECVADDRS_PREFIX,
    sdp_interface_versions,
)


@pytest.mark.parametrize("csp_ver", csp_config_versions())
@pytest.mark.parametrize(
    "sdp_ver", sdp_interface_versions(SDP_RECVADDRS_PREFIX, min_ver=(0, 2))
)
@pytest.mark.parametrize(
    "example_name,scan_type",
    [("cal_a", "calibration"), ("science_a", "science")],
)
def test_receive_addrs(csp_ver, sdp_ver, example_name, scan_type):
    # Get examples
    csp_cfg_str = json.dumps(get_csp_config_example(csp_ver))
    csp_cfg_str_out = json.dumps(get_csp_config_example(csp_ver, example_name))
    sdp_recvaddrs = example_by_uri(sdp_ver, csp_ver)
    # print(sdp_recvaddrs)
    # Check result
    csp_config_a = make_csp_config(
        csp_ver, sdp_ver, scan_type, csp_cfg_str, sdp_recvaddrs
    )
    assert json.loads(csp_config_a) == json.loads(csp_cfg_str_out)

    # Check that we can also pass strings
    csp_config_a = make_csp_config(
        csp_ver,
        sdp_ver,
        scan_type,
        json.loads(csp_cfg_str),
        json.dumps(sdp_recvaddrs),
    )
    assert json.loads(csp_config_a) == json.loads(csp_cfg_str_out)


@pytest.mark.parametrize(
    "sdp_ver", sdp_interface_versions(SDP_RECVADDRS_PREFIX, min_ver=(0, 2))
)
@pytest.mark.parametrize(
    "scan_type",
    [("science_a")],
)
def test_receive_addrs_low_error(sdp_ver, scan_type):
    # Get examples
    csp_ver = LOWCSP_CONFIGURE_PREFIX + "3.2"
    csp_cfg_str = json.dumps(get_low_csp_configure_example(csp_ver))
    sdp_recvaddrs = example_by_uri(sdp_ver, csp_ver)

    # Check result
    with pytest.raises(
        ValueError,
        match=f"No receive addresses found for scan type '{scan_type}'!",
    ):
        make_csp_config(
            csp_ver,
            sdp_ver,
            scan_type,
            csp_cfg_str,
            sdp_recvaddrs,
            telescope_branch="low",
        )


@pytest.mark.parametrize(
    "sdp_ver",
    sdp_interface_versions(
        SDP_RECVADDRS_PREFIX, min_ver=(0, 2), max_ver=(0, 2)
    ),
)
@pytest.mark.parametrize(
    "scan_type",
    [("science")],
)
def test_receive_addrs_sdp_ver_not_in_range(sdp_ver, scan_type):
    # Get examples
    csp_ver = LOWCSP_CONFIGURE_PREFIX + "3.2"
    csp_cfg_str = json.dumps(get_low_csp_configure_example(csp_ver))
    sdp_recvaddrs = example_by_uri(sdp_ver, csp_ver)

    csp_cfg = make_csp_config(
        csp_ver,
        sdp_ver,
        scan_type,
        csp_cfg_str,
        sdp_recvaddrs,
        telescope_branch="low",
    )
    # same json is returned
    assert json.loads(csp_cfg_str) == json.loads(csp_cfg)


@pytest.mark.parametrize(
    "sdp_ver", sdp_interface_versions(SDP_RECVADDRS_PREFIX, min_ver=(0, 4))
)
@pytest.mark.parametrize(
    "scan_type",
    [("science")],
)
def test_receive_addrs_low(sdp_ver, scan_type):
    # Get examples
    csp_ver = LOWCSP_CONFIGURE_PREFIX + "3.2"
    csp_cfg_str = get_low_csp_configure_example(csp_ver)
    csp_cfg_ip_str = json.dumps(csp_cfg_str)
    sdp_recvaddrs = example_by_uri(sdp_ver)
    csp_cfg = get_low_csp_configure_example(csp_ver)
    csp_cfg["lowcbf"]["vis"]["stn_beams"][0].update(
        {
            "stn_beam_id": 1,
            "host": sdp_recvaddrs["science"]["vis0"]["host"],
            "port": sdp_recvaddrs["science"]["vis0"]["port"],
            "mac": sdp_recvaddrs["science"]["vis0"]["mac"],
        }
    )
    csp_cfg_str_out = json.dumps(csp_cfg)

    # Check result
    csp_config_a = make_csp_config(
        csp_ver,
        sdp_ver,
        scan_type,
        csp_cfg_ip_str,
        sdp_recvaddrs,
        telescope_branch="low",
    )
    assert json.loads(csp_config_a) == json.loads(csp_cfg_str_out)

    # Check that we can also pass strings
    csp_config_a = make_csp_config(
        csp_ver,
        sdp_ver,
        scan_type,
        json.loads(csp_cfg_ip_str),
        json.dumps(sdp_recvaddrs),
        telescope_branch="low",
    )
    assert json.loads(csp_config_a) == json.loads(csp_cfg_str_out)


@pytest.mark.parametrize(
    "sdp_ver", sdp_interface_versions(SDP_RECVADDRS_PREFIX, min_ver=(0, 4))
)
@pytest.mark.parametrize(
    "scan_type",
    [("science")],
)
def test_receive_addrs_empty(sdp_ver, scan_type):
    sdp_recvaddrs = example_by_uri(sdp_ver)
    csp_ver = LOWCSP_CONFIGURE_PREFIX + "3.2"
    sdp_recvaddrs[scan_type]["vis0"]["function"] = ""
    csp_cfg_str = json.dumps(get_low_csp_configure_example(csp_ver))
    csp_config_a = make_csp_config(
        csp_ver,
        sdp_ver,
        scan_type,
        json.loads(csp_cfg_str),
        json.dumps(sdp_recvaddrs),
        telescope_branch="low",
    )
    assert json.loads(csp_config_a) == json.loads(csp_cfg_str)


def _add_channel_offset_to_sdp_receive_addrs(obj, offset, start_channel=None):
    """
    Simply walks the receive addresses, adding an offset on
    any channel-map-like structure it finds

    :param obj: JSON-like object to walk
    :param offset: Channel offset to apply
    :param start_channel: First channel to apply to
    """

    # Looks like a channel map?
    if isinstance(obj, list):
        if isinstance(obj[0], list) and isinstance(obj[0][0], int):
            for entry in obj:
                if start_channel is None or entry[0] >= start_channel:
                    entry[0] += offset

    elif isinstance(obj, dict):
        for child in obj.values():
            _add_channel_offset_to_sdp_receive_addrs(
                child, offset, start_channel
            )

    return obj


@pytest.mark.parametrize("csp_ver", csp_config_versions())
@pytest.mark.parametrize(
    "sdp_ver", sdp_interface_versions(SDP_RECVADDRS_PREFIX, min_ver=(0, 2))
)
def test_receive_addrs_no_off(csp_ver, sdp_ver):
    csp_cfg_str = json.dumps(get_csp_config_example(csp_ver))
    csp_cfg_str_out_science_a = json.dumps(
        get_csp_config_example(csp_ver, "science_a")
    )
    sdp_recvaddrs = example_by_uri(sdp_ver, csp_ver)

    # Get Interface version number to check the expected values for
    # specific keys
    major, _ = split_interface_version(csp_ver)

    # Test not applicable to csp.config 4.0+, sdp_start_channel_id
    # (effectively channel_offset) is a required parameter
    if major >= 4:
        return

    # Remove output channel offset
    csp_config_no_offsets = json.loads(csp_cfg_str)
    if "cbf" in csp_config_no_offsets:
        fsp_config = csp_config_no_offsets["cbf"]["fsp"]
    else:
        fsp_config = csp_config_no_offsets["fsp"]

    # Determine name of channel offset by CSP interface version
    major, _ = split_interface_version(csp_ver)
    if major == 0 or major == 1:
        for fsp in fsp_config:
            del fsp["fspChannelOffset"]
    if major == 2 or major == 3:
        for fsp in fsp_config:
            del fsp["channel_offset"]

    # Removing offsets is going to move FSP 2 from the given offset
    # (744) to the default offset (14880). Adjust maps accordingly.
    # note: mid.CBF expects channels in groupings of 20, so offset of 740
    # is used in the examples.
    if major >= 3:
        offset = 740
    else:
        offset = 744

    recv_addr_no_offsets = _add_channel_offset_to_sdp_receive_addrs(
        copy.deepcopy(sdp_recvaddrs),
        offset=14880 - offset,
        start_channel=offset,
    )
    result_csp_config = make_csp_config(
        csp_ver,
        sdp_ver,
        "science",
        csp_config_no_offsets,
        recv_addr_no_offsets,
    )

    csp_config_expected = json.loads(csp_cfg_str_out_science_a)

    if major == 0 or major == 1:
        if "fsp" in csp_config_expected:
            csp_config_expected["fsp"][1]["fspChannelOffset"] = 14880
        else:
            csp_config_expected["cbf"]["fsp"][1]["fspChannelOffset"] = 14880
    if major == 2:
        if "fsp" in csp_config_expected:
            csp_config_expected["fsp"][1]["channel_offset"] = 14880
        else:
            csp_config_expected["cbf"]["fsp"][1]["channel_offset"] = 14880
    if major == 3:
        if "fsp" in csp_config_expected:
            csp_config_expected["fsp"][1]["channel_offset"] = 14880
        else:
            csp_config_expected["cbf"]["fsp"][1]["channel_offset"] = 14880

    assert json.loads(result_csp_config) == csp_config_expected


@pytest.mark.parametrize("csp_ver", csp_config_versions())
@pytest.mark.parametrize(
    "sdp_ver", sdp_interface_versions(SDP_RECVADDRS_PREFIX, min_ver=(0, 2))
)
def test_receive_addrs_no_channels(csp_ver, sdp_ver):
    csp_cfg_str = json.dumps(get_csp_config_example(csp_ver))
    csp_cfg_str_out_science_a = json.dumps(
        get_csp_config_example(csp_ver, "science_a")
    )
    sdp_recvaddrs = example_by_uri(sdp_ver, csp_ver)

    # Get Interface version number to check the expected values for
    # specific keys
    major, _ = split_interface_version(csp_ver)

    # Check "science" if SDP returns no channels
    sdp_recvaddrs_dict = copy.deepcopy(sdp_recvaddrs)
    for maps in sdp_recvaddrs_dict.values():
        if "host" in maps:
            maps["host"] = []
        else:  # SDP schema version 0.4 forward
            for nmaps in maps.values():
                nmaps["host"] = []
    cfg_str_out = json.loads(csp_cfg_str_out_science_a)
    if major == 0 or major == 1:
        for maps in cfg_str_out.get("cbf", cfg_str_out)["fsp"]:
            maps["outputHost"] = []
    if major == 2 or major == 3:
        for maps in cfg_str_out.get("cbf", cfg_str_out)["fsp"]:
            maps["output_host"] = []
    if major >= 4:
        for maps in cfg_str_out.get("midcbf", cfg_str_out)["correlation"][
            "processing_regions"
        ]:
            maps["output_host"] = []
    csp_config_a4 = make_csp_config(
        csp_ver,
        sdp_ver,
        "science",
        csp_cfg_str,
        json.dumps(sdp_recvaddrs_dict),
    )
    assert json.loads(csp_config_a4) == cfg_str_out


@pytest.mark.parametrize("csp_ver", csp_config_versions())
@pytest.mark.parametrize(
    "sdp_ver", sdp_interface_versions(SDP_RECVADDRS_PREFIX, min_ver=(0, 4))
)
def test_receive_addrs_no_beam_functions(csp_ver, sdp_ver):
    csp_cfg_str = json.dumps(get_csp_config_example(csp_ver))
    sdp_recvaddrs = example_by_uri(sdp_ver)

    # Check "science" if all beams functions are cleared
    sdp_recvaddrs_dict = copy.deepcopy(sdp_recvaddrs)
    for maps in sdp_recvaddrs_dict.values():
        for nmaps in maps.values():
            nmaps["function"] = ""
    csp_config_a5 = make_csp_config(
        csp_ver,
        sdp_ver,
        "science",
        csp_cfg_str,
        json.dumps(sdp_recvaddrs_dict),
    )
    # Expect that nothing would happen, as it it impossible to
    # fill CSP information.
    assert json.loads(csp_config_a5) == json.loads(csp_cfg_str)


@pytest.mark.parametrize("csp_ver", csp_config_versions())
@pytest.mark.parametrize(
    "sdp_ver",
    sdp_interface_versions(
        SDP_RECVADDRS_PREFIX, min_ver=(0, 2), max_ver=(0, 3)
    ),
)
def test_receive_addrs_invalid_scan_type(csp_ver, sdp_ver):
    csp_cfg_str = json.dumps(get_csp_config_example(csp_ver))
    sdp_recvaddrs = example_by_uri(sdp_ver)

    # Check exceptions
    with pytest.raises(ValueError, match=r".*calibration_B.*"):
        make_csp_config(
            csp_ver, sdp_ver, "calibration_B", csp_cfg_str, sdp_recvaddrs
        )


@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(2, 1)))
@pytest.mark.parametrize(
    "sdp_ver",
    sdp_interface_versions(SDP_RECVADDRS_PREFIX, min_ver=(0, 4)),
)
@pytest.mark.parametrize("scan_type", ["science", "calibration"])
def test_receive_addrs_pss(csp_ver, sdp_ver, scan_type):
    # Make CSP configuration for PSS
    csp_cfg = get_csp_config_example(csp_ver, "pss")
    sdp_recvaddrs = example_by_uri(sdp_ver)
    csp_config_new = json.loads(
        make_csp_config(csp_ver, sdp_ver, scan_type, csp_cfg, sdp_recvaddrs)
    )

    # Check that both PSS beams get the correct receive addresses
    assert csp_cfg != csp_config_new
    for beam_id in [0, 1]:
        csp_beam = csp_config_new["pss"]["beam"][beam_id]
        csp_beam_old = csp_cfg["pss"]["beam"][beam_id]
        sdp_beam = sdp_recvaddrs[scan_type][f"pss{beam_id + 1}"]
        assert csp_beam["beam_id"] == sdp_beam["search_beam_id"]
        assert csp_beam["beam_id"] == sdp_beam["search_beam_id"]
        assert csp_beam_old["dest_host"] != sdp_beam["host"][0][1]
        assert csp_beam_old["dest_port"] != sdp_beam["port"][0][1]
        assert csp_beam["dest_host"] == sdp_beam["host"][0][1]
        assert csp_beam["dest_port"] == sdp_beam["port"][0][1]


@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(2, 1)))
@pytest.mark.parametrize(
    "sdp_ver",
    sdp_interface_versions(SDP_RECVADDRS_PREFIX, min_ver=(0, 4)),
)
def test_receive_addrs_pss_beam_id_mismatch(csp_ver, sdp_ver, caplog):
    # Make CSP configuration for PSS
    scan_type = "science"
    csp_cfg = get_csp_config_example(csp_ver, "pss")
    sdp_recvaddrs = example_by_uri(sdp_ver)

    # Invalidate first beam ID in receive addresses
    sdp_recvaddrs[scan_type]["pss1"]["search_beam_id"] = 99

    # Generate CSP configuration
    caplog.set_level(logging.WARNING)
    csp_config_new = json.loads(
        make_csp_config(csp_ver, sdp_ver, scan_type, csp_cfg, sdp_recvaddrs)
    )
    assert (
        "Could not find receive addresses for PSS search beam 1" in caplog.text
    )

    # Check that first PSS beam has incorrect receive addresses
    csp_beam = csp_config_new["pss"]["beam"][0]
    csp_beam_old = csp_cfg["pss"]["beam"][0]
    sdp_beam = sdp_recvaddrs[scan_type]["pss1"]
    assert csp_beam["beam_id"] != sdp_beam["search_beam_id"]
    assert csp_beam["beam_id"] != sdp_beam["search_beam_id"]
    assert csp_beam["dest_host"] != sdp_beam["host"][0][1]
    assert csp_beam["dest_port"] != sdp_beam["port"][0][1]
    assert csp_beam["dest_host"] == csp_beam_old["dest_host"]
    assert csp_beam["dest_port"] == csp_beam_old["dest_port"]

    # Yet the second PSS beam still got the correct addresses
    csp_beam = csp_config_new["pss"]["beam"][1]
    csp_beam_old = csp_cfg["pss"]["beam"][1]
    sdp_beam = sdp_recvaddrs[scan_type]["pss2"]
    assert csp_beam["beam_id"] == sdp_beam["search_beam_id"]
    assert csp_beam["beam_id"] == sdp_beam["search_beam_id"]
    assert csp_beam_old["dest_host"] != sdp_beam["host"][0][1]
    assert csp_beam_old["dest_port"] != sdp_beam["port"][0][1]
    assert csp_beam["dest_host"] == sdp_beam["host"][0][1]
    assert csp_beam["dest_port"] == sdp_beam["port"][0][1]


@pytest.mark.parametrize(
    "example_type", ["pss", "pst_scan_pt", "pst_scan_ds", "pst_scan_ft"]
)
@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(2, 2)))
def test_receive_addrs_pss_old_sdpver(csp_ver, example_type):
    # Make CSP configuration for PSS
    csp_cfg = get_csp_config_example(csp_ver, example_type)

    # Get old-format SDP receive addresses (without PSS support)
    sdp_ver = "https://schema.skao.int/ska-sdp-recvaddrs/0.3"
    sdp_recvaddrs = example_by_uri(sdp_ver)
    csp_config_new = json.loads(
        make_csp_config(csp_ver, sdp_ver, "science", csp_cfg, sdp_recvaddrs)
    )

    # Nothing should happen
    assert csp_cfg.get("pss") == csp_config_new.get("pss")
    assert csp_cfg.get("pst") == csp_config_new.get("pst")


@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(2, 2)))
@pytest.mark.parametrize(
    "sdp_ver",
    sdp_interface_versions(SDP_RECVADDRS_PREFIX, min_ver=(0, 4)),
)
@pytest.mark.parametrize(
    "example_type", ["pst_scan_pt", "pst_scan_ds", "pst_scan_ft"]
)
@pytest.mark.parametrize("scan_type", ["science", "calibration"])
def test_receive_addrs_pst(csp_ver, sdp_ver, example_type, scan_type):
    # Make CSP configuration for PSS
    csp_cfg = get_csp_config_example(csp_ver, example_type)
    sdp_recvaddrs = example_by_uri(sdp_ver)
    csp_config_new = json.loads(
        make_csp_config(csp_ver, sdp_ver, scan_type, csp_cfg, sdp_recvaddrs)
    )

    # Check that PST gets the correct receive addresses
    pst_dest_addr = csp_config_new["pst"]["scan"]["destination_address"]
    sdp_addrs = sdp_recvaddrs[scan_type]["pst1"]
    assert pst_dest_addr[0] == sdp_addrs["host"][0][1]
    assert pst_dest_addr[1] == sdp_addrs["port"][0][1]


@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(2, 2)))
@pytest.mark.parametrize(
    "sdp_ver",
    sdp_interface_versions(SDP_RECVADDRS_PREFIX, min_ver=(0, 4)),
)
def test_receive_addrs_pst_missing(csp_ver, sdp_ver, caplog):
    # Make CSP configuration for PSS
    scan_type = "science"
    csp_cfg = get_csp_config_example(csp_ver, "pst_scan_ft")
    sdp_recvaddrs = example_by_uri(sdp_ver)

    # Remove all pulsar timing beams
    sdp_recvaddrs[scan_type] = {
        beam_name: addrs
        for beam_name, addrs in sdp_recvaddrs[scan_type].items()
        if "pst" not in beam_name
    }

    # Generate CSP configuration
    caplog.set_level(logging.WARNING)
    make_csp_config(csp_ver, sdp_ver, scan_type, csp_cfg, sdp_recvaddrs)
    assert "Could not find receive addresses for pulsar timing" in caplog.text
