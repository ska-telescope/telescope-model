import copy
from inspect import cleandoc
from typing import List, Union

import pytest

from ska_telmodel._common import split_interface_version
from ska_telmodel.csp import version
from ska_telmodel.csp.common_schema import (
    CHANNEL_WIDTH_VALUES,
    MAX_CHANNELS_PER_FSP,
    MAX_CORR_CHANNELS,
    MAX_PORT_VALUE,
    MAX_SIGNED_32BIT_INT,
    use_camel_case,
)
from ska_telmodel.csp.config import get_fsp_output_channel_offset
from ska_telmodel.csp.examples import (
    expand_output_port,
    get_csp_assignresources_example,
    get_csp_config_example,
    get_csp_releaseresources_example,
)
from ska_telmodel.csp.low_examples import get_low_csp_configure_example
from ska_telmodel.csp.low_version import (
    LOWCSP_ASSIGNRESOURCES_PREFIX,
    LOWCSP_CONFIGURE_PREFIX,
    LOWCSP_RELEASERESOURCES_PREFIX,
    LOWCSP_SCAN_PREFIX,
)
from ska_telmodel.csp.schema import get_csp_config_schema
from ska_telmodel.csp.version import (
    CSP_ASSIGNRESOURCES_PREFIX,
    CSP_CONFIG_PREFIX,
    CSP_CONFIG_VERSIONS,
    CSP_DELAYMODEL_PREFIX,
    CSP_ENDSCAN_PREFIX,
    CSP_LOW_DELAYMODEL_PREFIX,
    CSP_MID_DELAYMODEL_PREFIX,
    CSP_RELEASERESOURCES_PREFIX,
    CSP_SCAN_PREFIX,
    check_csp_interface_version,
    csp_config_versions,
)
from ska_telmodel.schema import example_by_uri, schema_by_uri, validate
from ska_telmodel.tmc.version import TMC_CONFIGURE_PREFIX

model_prefixes = [
    CSP_ASSIGNRESOURCES_PREFIX,
    CSP_DELAYMODEL_PREFIX,
    CSP_ENDSCAN_PREFIX,
    CSP_MID_DELAYMODEL_PREFIX,
    CSP_RELEASERESOURCES_PREFIX,
    CSP_SCAN_PREFIX,
]

model_uris_and_example_names = {
    "ASSIGNRESOURCES": [
        (CSP_ASSIGNRESOURCES_PREFIX + "2.2",),
        (CSP_ASSIGNRESOURCES_PREFIX + "2.3",),
        (CSP_ASSIGNRESOURCES_PREFIX + "3.0",),
    ],
    "DELAYMODEL": [
        (CSP_DELAYMODEL_PREFIX + "2.2",),
        (CSP_MID_DELAYMODEL_PREFIX + "3.0",),
    ],
    "ENDSCAN": [
        (CSP_ENDSCAN_PREFIX + "2.2",),
        (CSP_ENDSCAN_PREFIX + "2.3",),
    ],
    "RELEASERESOURCES": [
        (CSP_RELEASERESOURCES_PREFIX + "2.2",),
        (CSP_RELEASERESOURCES_PREFIX + "2.3",),
        (CSP_RELEASERESOURCES_PREFIX + "3.0",),
    ],
    "SCAN": [
        (CSP_SCAN_PREFIX + "2.2",),
        (CSP_SCAN_PREFIX + "2.3",),
    ],
}

CSP_ASSIGNRESOURCES_FIELD_BOUNDS = {("subarray_id", "subarrayID"): (1, 16)}

CSP_RELEASERESOURCES_FIELD_BOUNDS = {("subarray_id", "subarrayID"): (1, 16)}


# Deletes "key" in the sub dictionary "traverser".
# "Traverser" is a sub dictionary within "erronous_model".
# Then checks if it is a valid schema compared to the original
# "model_uri"
def validate_omitted_field(
    key: str, traverser: dict, model_uri: str, erronous_model: dict
):
    model_schema = schema_by_uri(model_uri)
    key_schema = model_schema.find_field_recursive(key)
    assert key_schema is not None, f"{key} was not found in {model_uri} schema"

    optional = key_schema.is_field_optional(key)
    temp = traverser[key]
    del traverser[key]

    if not optional:
        with pytest.raises(ValueError):
            validate(model_uri, erronous_model, 2)
    else:
        validate(model_uri, erronous_model, 2)

    # Adds back deleted reference to bring schema/dict back
    # to the original state
    traverser[key] = temp


# This will package up each path from extract_path_of_each_key()
# and associate it with a model before sending all of them
# back to pytest to unpack into individual tests
def parametrize_paths_of_each_key_in_each_model():
    path_parameters = []
    for model in model_uris_and_example_names:
        for model_uri_and_example_name in model_uris_and_example_names[model]:
            model_dict = example_by_uri(*model_uri_and_example_name)

            for path in extract_path_of_each_key(model_dict, []):
                path_parameters.append(
                    (model_uri_and_example_name, path.copy())
                )

    return path_parameters


# For every key in the dictionary and any nested dictionaries,
# this will return a list of the steps needed to reach each key
def extract_path_of_each_key(dictionary: dict, curr_key_path: list):
    for key, value in dictionary.items():
        if not curr_key_path:
            curr_key_path.append(key)
        else:
            curr_key_path[-1] = key

        yield curr_key_path
        # Only dive into a list if it contains more dictionaries,
        # otherwise we could be trying to look into actual values
        if isinstance(value, list) and all(isinstance(n, dict) for n in value):
            for i, item in enumerate(value):
                curr_key_path_copy = curr_key_path.copy()
                curr_key_path_copy.append(i)
                curr_key_path_copy.append(key)
                yield from extract_path_of_each_key(
                    item.copy(), curr_key_path_copy
                )
        elif isinstance(value, dict):
            curr_key_path_copy = curr_key_path.copy()
            curr_key_path_copy.append(key)
            yield from extract_path_of_each_key(
                value.copy(), curr_key_path_copy
            )


def test_csp_versions(caplog):
    assert (
        check_csp_interface_version(CSP_SCAN_PREFIX + "0.7", CSP_SCAN_PREFIX)
        == "0.7"
    )

    invalid_prefix = "http://schexma.example.org/invalid"
    with pytest.raises(
        ValueError, match=f"CSP interface URI '{invalid_prefix}' not allowed"
    ):
        check_csp_interface_version(invalid_prefix)


def validate_int_field_bounds(
    version: str, field_name: str, example: dict, bounds: tuple
):
    if type(bounds) is not tuple:
        return  # Non specified bounds, so skip checking

    minimum, maximum = bounds
    if type(minimum) is str:
        return  # String checking needs custom handling

    # Check below minimum
    working_example = copy.deepcopy(example)
    working_example[field_name] = minimum - 1
    with pytest.raises(ValueError):
        validate(version, working_example, 2)

    # Check inbounds
    working_example[field_name] = minimum
    validate(version, working_example, 2)

    # Check above maximum
    working_example[field_name] = maximum + 1
    with pytest.raises(ValueError):
        validate(version, working_example, 2)

    # Check inbounds
    working_example[field_name] = maximum
    validate(version, working_example, 2)


@pytest.mark.parametrize(
    "model_uri_and_example_name",
    [uri for key in model_uris_and_example_names.values() for uri in key],
)
def test_csp_model_normal_case(model_uri_and_example_name: tuple):
    """
    Test CSP model schemas correctly validates the expected JSON.
    """
    csp_model_golden = example_by_uri(*model_uri_and_example_name)

    # Check normal case validates
    validate(model_uri_and_example_name[0], csp_model_golden, 2)


@pytest.mark.parametrize(
    "model_uri_and_example_name, path_to_validate",
    parametrize_paths_of_each_key_in_each_model(),
)
def test_csp_model_erronous_case(
    model_uri_and_example_name: tuple, path_to_validate: List[Union[str, int]]
):
    """
    Test CSP model schemas for when each one of the fields are missing.
    """
    model_erronous = example_by_uri(*model_uri_and_example_name)
    traverser = model_erronous

    if len(path_to_validate) == 0:
        pass
    elif len(path_to_validate) == 1:
        validate_omitted_field(
            path_to_validate[-1],
            traverser,
            model_uri_and_example_name[0],
            model_erronous,
        )
    else:
        # Steps through traverser as a nested dictionary until reaching
        # the parent of the final key in "path_to_validate" so we can get
        # a reference to the child key instead of the value
        for i, key in enumerate(path_to_validate):
            traverser = traverser[key]

            if i == len(path_to_validate) - 2:
                validate_omitted_field(
                    path_to_validate[-1],
                    traverser,
                    model_uri_and_example_name[0],
                    model_erronous,
                )
                break


@pytest.mark.parametrize(
    "model_uri_and_example_name",
    [uri for key in model_uris_and_example_names.values() for uri in key],
)
def test_csp_model_interface_version_as_part_of_json(
    model_uri_and_example_name: tuple,
):
    """
    Test CSP model schemas when the interface version is given as
    part of the JSON
    """
    validate(None, example_by_uri(*model_uri_and_example_name), 2)


@pytest.mark.parametrize("model_prefix", model_prefixes)
def test_csp_model_invalid_version(model_prefix: str):
    """
    Test CSP model schemas when there is an invalid version
    """
    with pytest.raises(ValueError, match=r"Could not generate example"):
        example_by_uri(model_prefix + "0.0")


def test_newer_major_version():
    undefined_major_version = version.CSP_CONFIG_PREFIX + "9999.0"
    with pytest.raises(ValueError, match="Unknown major schema version"):
        validate(None, {"interface": undefined_major_version}, 2)


def test_nonstandard_version_formats():
    # Test conversion to string
    invalid_prefix = "http://schema.example.org/invalid"
    normalized_invalid_prefix = version.normalize_csp_config_version(
        None, {"interface": invalid_prefix}
    )
    with pytest.raises(
        ValueError, match=f"CSP interface URI '{invalid_prefix}' not allowed"
    ):
        check_csp_interface_version(normalized_invalid_prefix)

    # Test conversion of allowed alternate csp config prefix
    allowed_prefix = "https://schema.skatelescope.org/ska-csp-configure/2.0"
    normalized_prefix = version.normalize_csp_config_version(allowed_prefix)
    check_csp_interface_version(normalized_prefix)


@pytest.mark.parametrize("csp_ver", csp_config_versions())
def test_schema(csp_ver):
    csp_config_in = get_csp_config_example(csp_ver)
    csp_config_out_cal_a = get_csp_config_example(csp_ver, "cal_a")
    csp_config_out_science_a = get_csp_config_example(csp_ver, "science_a")

    validate(csp_ver, csp_config_in, 2)
    validate(csp_ver, csp_config_out_cal_a, 2)
    validate(csp_ver, csp_config_out_science_a, 2)
    validate(csp_ver, csp_config_in, 2)
    validate(csp_ver, csp_config_out_cal_a, 2)
    validate(csp_ver, csp_config_out_science_a, 2)

    # Make configurations invalid to test strict validation
    major, minor = split_interface_version(csp_ver)

    for cfg in [csp_config_in, csp_config_out_cal_a, csp_config_out_science_a]:
        if major == 0 or major == 1:
            cfg["functionMode"] = "FANTASY"
        if major >= 2:
            cfg["function_mode"] = "FANTASY"

        # Permissive validation: Should succeed with warning
        validate(csp_ver, cfg, 1)

        # Strict validation: Should fail
        if major >= 2:
            exc_msg = (
                f"Strict validation 'CSP config {major}.{minor}' "
                "Wrong key 'function_mode' in"
            )
        else:
            exc_msg = (
                f"Strict validation 'CSP config {major}.{minor}' "
                "Wrong key 'functionMode' in"
            )

        with pytest.raises(ValueError, match=exc_msg):
            validate(csp_ver, cfg, 2)
        if cfg is not csp_config_in:
            # Permissive validation: Should succeed with warning
            validate(csp_ver, cfg, 1)
            with pytest.raises(ValueError, match=exc_msg):
                validate(csp_ver, cfg, 2)


@pytest.mark.parametrize("csp_ver", csp_config_versions())
def test_example(csp_ver):
    example_by_uri(csp_ver)


def test_schema_validation_with_invalid_uri():
    csp_config_in = get_csp_config_example(CSP_CONFIG_VERSIONS[-1])
    csp_config_in["interface"] = "https://invalid_uri/2.0"
    msg = r"Unknown schema URI kind: https://invalid_uri/2.0!"
    with pytest.raises(ValueError, match=msg):
        validate(None, csp_config_in, 2)


def test_schema_validation_with_invalid_version():
    major, _ = split_interface_version(CSP_CONFIG_VERSIONS[-1])
    csp_config_in = get_csp_config_example(CSP_CONFIG_VERSIONS[-1])
    csp_config_in["interface"] = CSP_CONFIG_PREFIX + str(major + 1) + ".0"
    with pytest.raises(ValueError, match=r"Unknown major schema version*"):
        validate(None, csp_config_in, 2)


@pytest.mark.parametrize(
    "csp_ver", csp_config_versions(min_ver=(2, 0), max_ver=(2, 1))
)
def test_get_fsp_output_channel_offset_with_channel_offset_set(csp_ver):
    csp_config = example_by_uri(csp_ver)
    for fsp in csp_config["cbf"]["fsp"]:
        get_fsp_output_channel_offset(fsp, fsp["fsp_id"], "channel_offset")


def test_schema_example_with_invalid_version():
    # Check that an invalid version raises an exception
    uri = CSP_CONFIG_PREFIX + "1.2"
    msg = f"Could not generate example for schema {uri}!"
    with pytest.raises(ValueError, match=msg):
        get_csp_config_example(uri)


@pytest.mark.parametrize("uri", CSP_CONFIG_VERSIONS)
def test_schema_example_with_invalid_scan(uri):
    # Check that an invalid scan name raises an exception
    msg = "Invalid scan name: FANTASY"
    with pytest.raises(ValueError, match=msg):
        get_csp_config_example(uri, "FANTASY")


def test_schema_example_with_valid_scan_invalid_version():
    # Check that a valid scan name but with an invalid version
    # raises an exception
    version_number = "0.0"
    scan = "pss"
    msg = cleandoc(
        f"""
        Version: {version_number} for scan type: {scan} is too old.
        Scan type is not supported in this earlier version number.
        """
    )
    with pytest.raises(ValueError, match=msg):
        get_csp_config_example(CSP_CONFIG_PREFIX + version_number, scan)


@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(2, 1)))
def test_pss_schema(csp_ver):
    csp_config_pss = get_csp_config_example(csp_ver, "pss")
    validate(csp_ver, csp_config_pss, 2)


@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(2, 2)))
def test_pst_beam_schema(csp_ver):
    csp_config_pst_beam = get_csp_config_example(csp_ver, "pst_beam")
    validate(csp_ver, csp_config_pst_beam, 2)


@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(2, 2)))
def test_pst_scan_pulsar_timing_schema(csp_ver):
    csp_config_pst_pt = get_csp_config_example(csp_ver, "pst_scan_pt")
    validate(csp_ver, csp_config_pst_pt, 2)


@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(2, 2)))
def test_pst_scan_dynamic_spectrum_schema(csp_ver):
    csp_config_pst_ds = get_csp_config_example(csp_ver, "pst_scan_ds")
    validate(csp_ver, csp_config_pst_ds, 2)


@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(2, 2)))
def test_pst_scan_flow_through_schema(csp_ver):
    csp_config_pst_ft = get_csp_config_example(csp_ver, "pst_scan_ft")
    validate(csp_ver, csp_config_pst_ft, 2)


@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(2, 3)))
def test_pst_scan_voltage_recorder_schema(csp_ver):
    csp_config_pst_vr = get_csp_config_example(csp_ver, "pst_scan_vr")
    validate(csp_ver, csp_config_pst_vr, 2)


@pytest.mark.parametrize(
    "omitted_key",
    (
        "start_validity_sec",
        "cadence_sec",
        "validity_period_sec",
        "config_id",
        "station_beam",
        "subarray",
        "station_beam_delays",
    ),
)
def test_csp_low_delaymodel_with_omitted_key(omitted_key):
    """
    Test CSP low delaymodel schema correctly validates the expected JSON.
    """
    for uri_version in ["1.0"]:
        low_delaymodel_uri = CSP_LOW_DELAYMODEL_PREFIX + uri_version
        # Check normal case validates
        validate(low_delaymodel_uri, example_by_uri(low_delaymodel_uri), 2)

        # Check that an error is raised if key is omitted
        csp_low_delaymodel_erronous = example_by_uri(low_delaymodel_uri)
        del csp_low_delaymodel_erronous[omitted_key]
        with pytest.raises(ValueError):
            validate(low_delaymodel_uri, csp_low_delaymodel_erronous, 2)

        # # Check invalid version raises error
        with pytest.raises(ValueError, match=r"Could not generate example"):
            example_by_uri(CSP_LOW_DELAYMODEL_PREFIX + "0.0")


def test_csp_low_delaymodel_interface():
    """
    Test validation when interface version is given as part of JSON.
    """
    for uri_version in ["1.0"]:
        low_delaymodel_uri = CSP_LOW_DELAYMODEL_PREFIX + uri_version
    validate(None, example_by_uri(low_delaymodel_uri), 2)


@pytest.mark.parametrize(
    "omitted_key",
    ("station_id", "substation_id", "xypol_coeffs_ns", "ypol_offset_ns"),
)
def test_csp_low_delaymodel_station_beam_delays_key(omitted_key):
    """
    Test CSP low delaymodel schema correctly validates the expected JSON
    for station_beam_delays key.
    """
    for uri_version in ["1.0"]:
        low_delaymodel_uri = CSP_LOW_DELAYMODEL_PREFIX + uri_version
        # Check that an error is raised if key is omitted
        csp_low_delaymodel_erronous = example_by_uri(low_delaymodel_uri)
        for item in csp_low_delaymodel_erronous["station_beam_delays"]:
            del item[omitted_key]
        with pytest.raises(ValueError):
            validate(low_delaymodel_uri, csp_low_delaymodel_erronous, 2)


def test_low_csp_assignresources():
    """
    Test LOW CSP assignresources schema correctly validates the expected JSON.
    """
    for uri_version in ["2.0", "3.0", "3.1", "3.2"]:
        uri = LOWCSP_ASSIGNRESOURCES_PREFIX + uri_version
        # Check normal case validates
        validate(uri, example_by_uri(uri), 2)

        # Check that no error is raised if optional 'PST' is omitted
        csp_example_erronous = example_by_uri(uri)
        del csp_example_erronous["pst"]
        validate(uri, csp_example_erronous, 2)

        # Check that no error is raised if optional 'PSS' is omitted
        csp_example_erronous = example_by_uri(uri)
        del csp_example_erronous["pss"]
        validate(uri, csp_example_erronous, 2)

        # Check that an error is raised if 'subarray_id' is omitted
        csp_example_erronous = example_by_uri(uri)
        del csp_example_erronous["common"]
        with pytest.raises(ValueError):
            validate(uri, csp_example_erronous, 2)

        # Check that an error is raised if 'subarray_id' is omitted
        csp_example_erronous = example_by_uri(uri)
        del csp_example_erronous["common"]["subarray_id"]
        with pytest.raises(ValueError):
            validate(uri, csp_example_erronous, 2)

        # Check validation when interface version is given as part of JSON
        validate(None, example_by_uri(uri), 2)

        # Check invalid version raises error
        with pytest.raises(ValueError, match=r"Could not generate example"):
            example_by_uri(LOWCSP_ASSIGNRESOURCES_PREFIX + "0.0")


def test_low_csp_releaseresources():
    """
    Test LOW CSP releaseresources schema correctly validates the expected JSON.
    """
    for uri_version in ["2.0", "3.0", "3.1", "3.2"]:
        uri = LOWCSP_RELEASERESOURCES_PREFIX + uri_version

        # Check normal case validates
        validate(uri, example_by_uri(uri), 2)

        # Check that no error is raised if optional 'PST' is omitted
        csp_example_erronous = example_by_uri(uri)
        del csp_example_erronous["pst"]
        validate(uri, csp_example_erronous, 2)

        # Check that an error is raised if 'subarray_id' is omitted
        csp_example_erronous = example_by_uri(uri)
        del csp_example_erronous["common"]
        with pytest.raises(ValueError):
            validate(uri, csp_example_erronous, 2)

        # Check that an error is raised if 'subarray_id' is omitted
        csp_example_erronous = example_by_uri(uri)
        del csp_example_erronous["common"]["subarray_id"]
        with pytest.raises(ValueError):
            validate(uri, csp_example_erronous, 2)

        # Check validation when interface version is given as part of JSON
        validate(None, example_by_uri(uri), 2)

        # Check invalid version raises error
        with pytest.raises(ValueError, match=r"Could not generate example"):
            example_by_uri(LOWCSP_RELEASERESOURCES_PREFIX + "0.0")


@pytest.mark.parametrize("csp_ver", ["3.0", "3.1"])
def test_csp_pst_scan_pulsar_timing_schema(csp_ver):
    """
    Test FLOW THROUGH configuration for (CSP 3.0, PST 2.4)
    and (CSP 3.1, PST 2.5)
    """
    uri_low_csp = LOWCSP_CONFIGURE_PREFIX + csp_ver
    csp_config_pst_pt = get_low_csp_configure_example(
        uri_low_csp, "pst_scan_pt"
    )
    validate(uri_low_csp, csp_config_pst_pt, 2)


@pytest.mark.parametrize("csp_ver", ["3.0", "3.1"])
def test_csp_pst_scan_flow_through_schema(csp_ver):
    """
    Test FLOW THROUGH configuration for (CSP 3.0, PST 2.4)
    and (CSP 3.1, PST 2.5)
    """
    uri_low_csp = LOWCSP_CONFIGURE_PREFIX + csp_ver
    csp_config_pst_ft = get_low_csp_configure_example(
        uri_low_csp, "pst_scan_ft"
    )
    validate(uri_low_csp, csp_config_pst_ft, 2)

    # Check that an error is raised if 'subarray_id' is omitted
    if "timing_beams" in csp_config_pst_ft["lowcbf"].keys():
        del csp_config_pst_ft["lowcbf"]["timing_beams"]["beams"][0][
            "delay_poly"
        ]
        with pytest.raises(ValueError):
            validate(uri_low_csp, csp_config_pst_ft, 2)


@pytest.mark.parametrize("csp_ver", ["2.0", "3.1", "3.2"])
def test_csp_pst_scan_voltage_recorder_schema(csp_ver):
    """
    Test VOLTAGE RECORDER configuration for (CSP 2.0, PST 2.4)
    and (CSP 3.1, PST 2.5), (CSP 3.2, PST 2.5)
    """
    uri_low_csp = LOWCSP_CONFIGURE_PREFIX + csp_ver
    csp_config_pst_vr = get_low_csp_configure_example(
        uri_low_csp, "pst_scan_vr"
    )
    validate(uri_low_csp, csp_config_pst_vr, 2)

    # Check that an error is raised if 'subarray_id' is omitted
    if (
        float(csp_ver) > 2.0
        and "timing_beams" in csp_config_pst_vr["lowcbf"].keys()
    ):
        del csp_config_pst_vr["lowcbf"]["timing_beams"]["beams"][0][
            "delay_poly"
        ]
        with pytest.raises(ValueError):
            validate(uri_low_csp, csp_config_pst_vr, 2)


@pytest.mark.parametrize("csp_ver", ["3.0", "3.1"])
def test_csp_pst_scan_dynamic_spectrum_schema(csp_ver):
    """
    Test DYNAMIC_SPECTRUM configuration for CSP 3.0 and 3.1
    """
    uri_low_csp = LOWCSP_CONFIGURE_PREFIX + csp_ver
    csp_config_pst_ds = get_low_csp_configure_example(
        uri_low_csp, "pst_scan_ds"
    )
    validate(uri_low_csp, csp_config_pst_ds, 2)

    # Check that an error is raised if 'subarray_id' is omitted
    if "timing_beams" in csp_config_pst_ds["lowcbf"].keys():
        del csp_config_pst_ds["lowcbf"]["timing_beams"]["beams"][0][
            "delay_poly"
        ]
        with pytest.raises(ValueError):
            validate(uri_low_csp, csp_config_pst_ds, 2)


def test_low_csp_scan():
    """
    Test LOW CSP scan schema correctly validates the expected JSON.
    """
    # sourcery skip: no-loop-in-tests
    for uri_version in ["2.0", "3.0", "3.1", "3.2", "4.0"]:
        uri = LOWCSP_SCAN_PREFIX + uri_version

        # Check normal case validates
        validate(uri, example_by_uri(uri), 2)

        # Check that an error is raised if 'subarray_id' is omitted
        csp_example_erronous = example_by_uri(uri)
        del csp_example_erronous["common"]
        with pytest.raises(ValueError):
            validate(uri, csp_example_erronous, 2)

        # Check that an error is raised if 'subarray_id' is omitted
        csp_example_erronous = example_by_uri(uri)
        del csp_example_erronous["common"]["subarray_id"]
        with pytest.raises(ValueError):
            validate(uri, csp_example_erronous, 2)

        # Check validation when interface version is given as part of JSON
        validate(None, example_by_uri(uri), 2)

        # Check invalid version raises error
        with pytest.raises(ValueError, match=r"Could not generate example"):
            example_by_uri(LOWCSP_SCAN_PREFIX + "0.0")


def test_low_csp_configure():
    """
    Test LOW CSP configure schema correctly validates the expected JSON.
    """
    for uri_version in ["2.0", "3.0", "3.1", "3.2", "4.0"]:
        uri = LOWCSP_CONFIGURE_PREFIX + uri_version
        # Check normal case validates
        validate(uri, example_by_uri(uri), 2)

        # Check that no error is raised if optional 'PST' is omitted
        csp_example_erronous = example_by_uri(uri)
        if "pst" in csp_example_erronous:
            del csp_example_erronous["pst"]
        validate(uri, csp_example_erronous, 2)

        # Check that no error is raised if optional 'CBF' is omitted
        csp_example_erronous = example_by_uri(uri)
        if "lowcbf" in csp_example_erronous:
            del csp_example_erronous["lowcbf"]
        validate(uri, csp_example_erronous, 2)

        # Check that no error is raised if optional 'PSS' is omitted
        csp_example_erronous = example_by_uri(uri)
        if "pss" in csp_example_erronous:
            del csp_example_erronous["pss"]
        validate(uri, csp_example_erronous, 2)

        # Check that an error is raised if 'subarray_id' is omitted
        csp_example_erronous = example_by_uri(uri)
        if "common" in csp_example_erronous:
            del csp_example_erronous["common"]
        with pytest.raises(ValueError):
            validate(uri, csp_example_erronous, 2)

        # Check validation when interface version is given as part ofif JSON
        validate(None, example_by_uri(uri), 2)

        # Check invalid version raises error
        with pytest.raises(ValueError, match=r"Could not generate example"):
            example_by_uri(LOWCSP_CONFIGURE_PREFIX + "0.0")


@pytest.mark.parametrize(
    "version",
    [
        uri
        for example in model_uris_and_example_names["RELEASERESOURCES"]
        for uri in example
    ],
)
@pytest.mark.parametrize(
    "csp_field_name", list(CSP_RELEASERESOURCES_FIELD_BOUNDS.keys())
)
@pytest.mark.parametrize("example", [None, "2"])
def test_csp_release_resources_field(version, csp_field_name, example):
    # Get version
    (major, minor) = split_interface_version(version)

    # Get examples
    csp_release_resources_example = get_csp_releaseresources_example(
        version, example
    )

    field_name = (
        csp_field_name[0] if use_camel_case(version) else csp_field_name[1]
    )

    # Note: No field validation in csp_release_resources < v3.0
    if (major, minor) >= (3, 0):
        # Check bounds on values in field
        validate_int_field_bounds(
            version,
            field_name,
            csp_release_resources_example,
            CSP_RELEASERESOURCES_FIELD_BOUNDS[csp_field_name],
        )


@pytest.mark.parametrize(
    "version",
    [
        uri
        for example in model_uris_and_example_names["ASSIGNRESOURCES"]
        for uri in example
    ],
)
@pytest.mark.parametrize(
    "csp_field_name", list(CSP_ASSIGNRESOURCES_FIELD_BOUNDS.keys())
)
def test_csp_assign_resources_field(version, csp_field_name):
    # Get version
    major, minor = split_interface_version(version)

    # Get examples
    csp_assign_resources_example = get_csp_assignresources_example(version)

    field_name = (
        csp_field_name[0] if use_camel_case(version) else csp_field_name[1]
    )

    # Note: No field validation in csp_assign_resources < v3.0

    if (major, minor) >= (3, 0):
        # Check bounds on values in field
        validate_int_field_bounds(
            version,
            field_name,
            csp_assign_resources_example,
            CSP_ASSIGNRESOURCES_FIELD_BOUNDS[csp_field_name],
        )


@pytest.mark.parametrize("csp_ver", csp_config_versions())
def test_only_one_output_link_host(csp_ver):
    csp_config = get_csp_config_example(csp_ver)

    major, minor = split_interface_version(csp_ver)

    if major >= 4:
        parent_path = csp_config["midcbf"]["correlation"]["processing_regions"]
    elif major >= 3:
        parent_path = csp_config["cbf"]["fsp"]
    else:
        # Validation only added in 3.0+
        return

    # limit on output link map entries added in v3.0
    if (major, minor) >= (3, 0):
        field_name = (
            "output_link_map" if use_camel_case(csp_ver) else "outputLinkMap"
        )

        for parent in parent_path:
            parent[field_name] = [[0, 1], [200, 2]]
            with pytest.raises(ValueError):
                validate(csp_ver, csp_config, 2)

            # End loop with a valid example
            parent[field_name] = [[0, 1]]
            validate(csp_ver, csp_config, 2)


@pytest.mark.parametrize("csp_ver", csp_config_versions())
def test_output_host_ip(csp_ver):
    csp_config = get_csp_config_example(csp_ver)

    major, minor = split_interface_version(csp_ver)

    if major >= 4:
        parent_path = csp_config["midcbf"]["correlation"]["processing_regions"]
    elif major >= 1:
        parent_path = csp_config["cbf"]["fsp"]
    else:
        parent_path = csp_config["fsp"]

    field_name = "output_host" if use_camel_case(csp_ver) else "outputHost"

    for parent in parent_path:
        parent[field_name] = [[0, "0.0.0.0"]]
        validate(csp_ver, csp_config, 2)

        if (major, minor) >= (3, 0):
            parent[field_name] = [[0, "0.0.0"]]
            with pytest.raises(ValueError):
                validate(csp_ver, csp_config, 2)

            parent[field_name] = [[0, "255.255.255.256"]]
            with pytest.raises(ValueError):
                validate(csp_ver, csp_config, 2)

            # End loop with a valid example
            parent[field_name] = [[0, "255.255.255.255"]]
            validate(csp_ver, csp_config, 2)


@pytest.mark.parametrize("csp_ver", csp_config_versions())
@pytest.mark.parametrize(
    "frequency_band_offset_field",
    [
        ("frequency_band_offset_stream1", "frequencyBandOffsetStream1"),
        ("frequency_band_offset_stream2", "frequencyBandOffsetStream2"),
    ],
)
def test_frequency_band_offset_stream(csp_ver, frequency_band_offset_field):
    csp_config = get_csp_config_example(csp_ver)

    major, minor = split_interface_version(csp_ver)

    if major >= 4:
        cbf_path = csp_config["midcbf"]
    elif major >= 1:
        cbf_path = csp_config["cbf"]
    else:
        # No cbf field in version 0
        return

    fbo_field = (
        frequency_band_offset_field[0]
        if use_camel_case(csp_ver)
        else frequency_band_offset_field[1]
    )

    # Validate valid cases
    cbf_path[fbo_field] = -100000000
    validate(csp_ver, csp_config, 2)

    cbf_path[fbo_field] = 100000000
    validate(csp_ver, csp_config, 2)

    # range validation only added to version 3.0+
    if (major, minor) >= (3, 0):
        # Validate invalid cases
        cbf_path[fbo_field] = -100000001
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)

        cbf_path[fbo_field] = 100000001
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)


@pytest.mark.parametrize("csp_ver", csp_config_versions())
def test_output_port(csp_ver):
    """
    Test the validation and format restrictions on the output_port field.
    """

    csp_config = get_csp_config_example(csp_ver)
    major, minor = split_interface_version(csp_ver)

    if major >= 4:
        parent_path = csp_config["midcbf"]["correlation"]["processing_regions"]
    elif major >= 1:
        parent_path = csp_config["cbf"]["fsp"]
    else:
        parent_path = csp_config["fsp"]

    if (major, minor) >= (4, 0):
        start_channel_upper_bound = MAX_CORR_CHANNELS
    else:
        start_channel_upper_bound = MAX_CHANNELS_PER_FSP - 1

    field_name = "output_port" if use_camel_case(csp_ver) else "outputPort"

    example_output_port = expand_output_port([[0, 9000, 1]])

    if (major, minor) >= (4, 0):
        # Test valid expanded mapping
        parent_path[0][field_name] = copy.deepcopy(example_output_port)
        validate(csp_ver, csp_config, 2)

        parent_path[0][field_name].pop()
        validate(csp_ver, csp_config, 2)

        # Test invalid number of mappings - lower bound
        parent_path[0][field_name] = []
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)

        if (major, minor) >= (4, 1):
            # Test invalid number of mappings - upper bound
            parent_path[0][field_name] = copy.deepcopy(example_output_port)
            parent_path[0][field_name].append([14880, 9744])
            validate(csp_ver, csp_config, 2)
        else:  # 4.0
            # Test invalid number of mappings - upper bound
            parent_path[0][field_name] = copy.deepcopy(example_output_port)
            parent_path[0][field_name].append([14880, 9744])
            with pytest.raises(ValueError):
                validate(csp_ver, csp_config, 2)

        # Test invalid expanded mapping entry length
        parent_path[0][field_name] = copy.deepcopy(example_output_port)
        parent_path[0][field_name][0] = [0]
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)

        parent_path[0][field_name] = copy.deepcopy(example_output_port)
        parent_path[0][field_name][0] = [0, 0, 1]
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)

        # Test invalid start channel id
        parent_path[0][field_name] = copy.deepcopy(example_output_port)
        parent_path[0][field_name][0][0] = -1
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)

        parent_path[0][field_name][0][0] = start_channel_upper_bound + 1
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)

        # Test invalid if non-ascending
        parent_path[0][field_name] = copy.deepcopy(example_output_port)
        # Swap positions of first two entries
        (
            parent_path[0][field_name][0],
            parent_path[0][field_name][1],
        ) = (
            parent_path[0][field_name][1],
            parent_path[0][field_name][0],
        )
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)

        # Test invalid if start channel id is a non-increment of 20
        parent_path[0][field_name] = copy.deepcopy(example_output_port)
        parent_path[0][field_name][1] = [15, 9001]
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)

        # Test invalid ports
        parent_path[0][field_name] = copy.deepcopy(example_output_port)
        parent_path[0][field_name][1] = [0, -1]
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)

        parent_path[0][field_name] = copy.deepcopy(example_output_port)
        parent_path[0][field_name][1] = [0, 65536]
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)
    elif (major, minor) >= (3, 0):
        # Test valid mapping length
        parent_path[0][field_name] = [[0, 9000]]
        validate(csp_ver, csp_config, 2)

        parent_path[0][field_name] = copy.deepcopy(example_output_port)
        validate(csp_ver, csp_config, 2)

        # Test invalid mapping length
        parent_path[0][field_name] = copy.deepcopy(example_output_port)
        parent_path[0][field_name].append([14880, 9744])
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)

        # Test invalid mapping entry length
        parent_path[0][field_name] = [[0, 9000, 1, 1]]
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)

        parent_path[0][field_name] = [[0]]
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)

        parent_path[0][field_name] = []
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)
    else:
        # Test valid 2 length mapping
        parent_path[0][field_name] = [[0, 9000]]
        validate(csp_ver, csp_config, 2)

        # Test valid 3 length mapping
        parent_path[0][field_name] = [[0, 9000, 1]]
        validate(csp_ver, csp_config, 2)

        # Test invalid mapping entry length
        parent_path[0][field_name] = [[0]]
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)

        parent_path[0][field_name] = [[0, 9000, 1, 1]]
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)


@pytest.mark.parametrize("csp_ver", csp_config_versions())
def test_output_host_channel(csp_ver):
    csp_config = get_csp_config_example(csp_ver)
    major, minor = split_interface_version(csp_ver)

    if major >= 4:
        parent_path = csp_config["midcbf"]["correlation"]["processing_regions"]
    elif major >= 1:
        parent_path = csp_config["cbf"]["fsp"]
    else:
        parent_path = csp_config["fsp"]

    field_name = "output_host" if use_camel_case(csp_ver) else "outputHost"

    if (major, minor) >= (4, 0):
        start_channel_upper_bound = MAX_CORR_CHANNELS
    else:
        start_channel_upper_bound = MAX_CHANNELS_PER_FSP - 1

    if (major, minor) >= (3, 0):
        # Test valid values
        parent_path[0][field_name] = [[0, "0.0.0.0"]]
        validate(csp_ver, csp_config, 2)

        parent_path[0][field_name] = [[start_channel_upper_bound, "0.0.0.0"]]
        validate(csp_ver, csp_config, 2)

        # Test invalid values
        parent_path[0][field_name] = [[-1, "0.0.0.0"]]
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)

        parent_path[0][field_name] = [
            [start_channel_upper_bound + 1, "0.0.0.0"]
        ]
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)

        parent_path[0][field_name] = [["0.0.0.0"]]
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)

        if (major, minor) >= (4, 0):
            parent_path[0][field_name] = []
            with pytest.raises(ValueError):
                validate(csp_ver, csp_config, 2)
    else:
        # Test valid values
        parent_path[0][field_name] = [[0, "0.0.0.0"]]
        validate(csp_ver, csp_config, 2)

        # Test invalid values
        parent_path[0][field_name] = [["0.0.0.0"]]
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)


@pytest.mark.parametrize("csp_ver", csp_config_versions())
def test_output_link_map(csp_ver):
    csp_config = get_csp_config_example(csp_ver)
    major, minor = split_interface_version(csp_ver)

    if major >= 4:
        parent_path = csp_config["midcbf"]["correlation"]["processing_regions"]
    elif major >= 1:
        parent_path = csp_config["cbf"]["fsp"]
    else:
        parent_path = csp_config["fsp"]

    field_name = (
        "output_link_map" if use_camel_case(csp_ver) else "outputLinkMap"
    )

    if (major, minor) >= (4, 0):
        start_channel_upper_bound = MAX_CORR_CHANNELS
    else:
        start_channel_upper_bound = MAX_CHANNELS_PER_FSP - 1

    if (major, minor) >= (3, 0):
        # Test valid values
        parent_path[0][field_name] = [[0, 1]]
        validate(csp_ver, csp_config, 2)

        parent_path[0][field_name] = [[start_channel_upper_bound, 1]]
        validate(csp_ver, csp_config, 2)

        # Test invalid values
        parent_path[0][field_name] = [[-1, 1]]
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)

        parent_path[0][field_name] = [[start_channel_upper_bound + 1, 1]]
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)

        parent_path[0][field_name] = [[0, 0]]
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)

        parent_path[0][field_name] = [[0]]
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)

        parent_path[0][field_name] = []
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)
    else:
        # Test valid values
        parent_path[0][field_name] = [[0, 1]]
        validate(csp_ver, csp_config, 2)

        # Test invalid values
        parent_path[0][field_name] = [[1]]
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)


@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(4, 0)))
def test_fsp_ids(csp_ver):
    csp_config = get_csp_config_example(csp_ver)

    parent_path = csp_config["midcbf"]["correlation"]["processing_regions"]

    field_name = "fsp_ids" if use_camel_case(csp_ver) else "fspIDs"

    # Test valid values
    parent_path[0][field_name] = []
    for value in range(1, 27):
        parent_path[0][field_name].append(value)
        validate(csp_ver, csp_config, 2)

    parent_path[0][field_name] = [27]
    validate(csp_ver, csp_config, 2)

    # test invalid values
    parent_path[0][field_name] = [0]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    parent_path[0][field_name] = [28]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    # test invalid too many elements
    parent_path[0][field_name] = []
    for value in range(0, 27):
        parent_path[0][field_name].append(1)
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    # test invalid too few elements
    parent_path[0][field_name] = []
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    # test invalid missing element
    del parent_path[0][field_name]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)


@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(4, 0)))
def test_start_freq(csp_ver):
    csp_config = get_csp_config_example(csp_ver)

    parent_path = csp_config["midcbf"]["correlation"]["processing_regions"]

    field_name = "start_freq" if use_camel_case(csp_ver) else "startFreq"

    major, minor = split_interface_version(csp_ver)

    if (major, minor) >= (4, 1):
        start_freq_lower_bound = 0
        start_freq_upper_bound = 15400000000
    else:
        start_freq_lower_bound = 350000000
        start_freq_upper_bound = 15400000000

    # Test valid values
    parent_path[0][field_name] = start_freq_lower_bound
    validate(csp_ver, csp_config, 2)

    parent_path[0][field_name] = start_freq_upper_bound
    validate(csp_ver, csp_config, 2)

    # Test invalid min value
    parent_path[0][field_name] = start_freq_lower_bound - 1
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    # Test invalid max value
    parent_path[0][field_name] = start_freq_upper_bound + 1
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    # Test missing required value
    del parent_path[0][field_name]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)


@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(4, 0)))
def test_channel_width(csp_ver):
    csp_config = get_csp_config_example(csp_ver)

    parent_path = csp_config["midcbf"]["correlation"]["processing_regions"]

    field_name = "channel_width" if use_camel_case(csp_ver) else "channelWidth"

    # Test valid values
    for channel_width in CHANNEL_WIDTH_VALUES:
        parent_path[0][field_name] = channel_width
        validate(csp_ver, csp_config, 2)

    # Test invalid values
    parent_path[0][field_name] = 0
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    # Test missing required value
    del parent_path[0][field_name]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)


@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(4, 0)))
def test_channel_count(csp_ver):
    csp_config = get_csp_config_example(csp_ver)

    parent_path = csp_config["midcbf"]["correlation"]["processing_regions"]

    field_name = "channel_count" if use_camel_case(csp_ver) else "channelCount"

    # Test valid values
    parent_path[0][field_name] = 1
    validate(csp_ver, csp_config, 2)

    parent_path[0][field_name] = 2**31 - 1
    validate(csp_ver, csp_config, 2)

    # Test invalid min value
    parent_path[0][field_name] = 0
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    # Test invalid max value
    parent_path[0][field_name] = 2**31
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    # Test missing required value
    del parent_path[0][field_name]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)


@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(4, 0)))
def test_sdp_start_channel_id(csp_ver):
    csp_config = get_csp_config_example(csp_ver)

    parent_path = csp_config["midcbf"]["correlation"]["processing_regions"]

    field_name = (
        "sdp_start_channel_id"
        if use_camel_case(csp_ver)
        else "sdpStartChannelID"
    )

    # Test valid value
    parent_path[0][field_name] = 0
    validate(csp_ver, csp_config, 2)

    parent_path[0][field_name] = MAX_CORR_CHANNELS
    validate(csp_ver, csp_config, 2)

    # Test invalid value
    parent_path[0][field_name] = -1
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    parent_path[0][field_name] = MAX_CORR_CHANNELS + 1
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    # Test missing invalid
    del parent_path[0][field_name]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)


# TMC doesn't collect their version numbers as nicely
@pytest.mark.parametrize(
    "tmc_ver", [(1, 0), (2, 0), (3, 0), (3, 1), (3, 2), (3, 3), (4, 0), (4, 1)]
)
@pytest.mark.parametrize("csp_ver", csp_config_versions())
def test_get_csp_config_schema_with_tmc(csp_ver, tmc_ver):
    get_csp_config_schema(
        csp_ver,
        1,
        tmc_schema_uri=TMC_CONFIGURE_PREFIX + f"{tmc_ver[0]}.{tmc_ver[1]}",
    )


@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(5, 0)))
def test_pst_bf(csp_ver):
    csp_config = get_csp_config_example(csp_ver, scan="pst_bf")
    validate(csp_ver, csp_config, 2)


@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(5, 0)))
def test_pst_bf_fsp_ids(csp_ver):
    csp_config = get_csp_config_example(csp_ver, scan="pst_bf")

    processing_regions = csp_config["midcbf"]["pst_bf"]["processing_regions"]
    pr_config = processing_regions[0]

    fsp_id_length_range = (1, 26)
    fsp_id_value_range = (1, 27)
    valid_values = [
        value
        for value in range(fsp_id_value_range[0], fsp_id_value_range[1] + 1)
    ]

    # validate array length
    pr_config["fsp_ids"] = None
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    pr_config["fsp_ids"] = valid_values[: fsp_id_length_range[0] - 1]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    pr_config["fsp_ids"] = valid_values[: fsp_id_length_range[0]]
    validate(csp_ver, csp_config, 2)

    pr_config["fsp_ids"] = valid_values[: fsp_id_length_range[1]]
    validate(csp_ver, csp_config, 2)

    pr_config["fsp_ids"] = valid_values[: fsp_id_length_range[1] + 1]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    # validate array values
    pr_config["fsp_ids"] = [fsp_id_value_range[0] - 1]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    pr_config["fsp_ids"] = [fsp_id_value_range[0]]
    validate(csp_ver, csp_config, 2)

    pr_config["fsp_ids"] = [fsp_id_value_range[1]]
    validate(csp_ver, csp_config, 2)

    pr_config["fsp_ids"] = [fsp_id_value_range[1] + 1]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    # validate unique ids
    pr_config["fsp_ids"] = [fsp_id_value_range[0], fsp_id_value_range[0]]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    # validate unique ids in the processing regions
    pr_config["fsp_ids"] = [5]
    pr_copy = copy.deepcopy(pr_config)
    processing_regions.append(pr_copy)

    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    pr_copy["fsp_ids"] = [6]
    validate(csp_ver, csp_config, 2)


@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(5, 0)))
def test_pst_bf_start_freq(csp_ver):
    csp_config = get_csp_config_example(csp_ver, scan="pst_bf")

    processing_regions = csp_config["midcbf"]["pst_bf"]["processing_regions"]
    pr_config = processing_regions[0]

    start_freq_multiple = 53760
    # can't start at 1 because it is not a multiple of 53760
    start_freq_range = (start_freq_multiple, 15399982080)

    # validate star_freq range
    _validate_value_range(
        pr_config, csp_ver, csp_config, "start_freq", start_freq_range
    )

    # validate multiple of 53760

    # SKB-749 - testing the entire range is too time consuming, using
    #           representative examples.
    for multiple in range(1, 5):
        pr_config["start_freq"] = start_freq_multiple * multiple
        validate(csp_ver, csp_config, 2)

        pr_config["start_freq"] = start_freq_multiple * multiple - 1
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)

        pr_config["start_freq"] = start_freq_multiple * multiple + 1
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)


@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(5, 0)))
def test_pst_bf_channel_count(csp_ver):
    csp_config = get_csp_config_example(csp_ver, scan="pst_bf")

    processing_regions = csp_config["midcbf"]["pst_bf"]["processing_regions"]
    pr_config = processing_regions[0]

    channel_count_range = (1, 47923)

    # validate values range
    _validate_value_range(
        pr_config, csp_ver, csp_config, "channel_count", channel_count_range
    )


@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(5, 0)))
def test_pst_bf_pst_start_channel_id(csp_ver):
    csp_config = get_csp_config_example(csp_ver, scan="pst_bf")

    processing_regions = csp_config["midcbf"]["pst_bf"]["processing_regions"]
    pr_config = processing_regions[0]
    timing_beam_config = pr_config["timing_beams"][0]

    pst_start_channel_count_id_range = (0, 2147483647)

    # validate channel_id range

    sync_first_channel_value(
        pr_config,
        timing_beam_config,
        "pst_start_channel_id",
        pst_start_channel_count_id_range[0] - 1,
    )
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    sync_first_channel_value(
        pr_config,
        timing_beam_config,
        "pst_start_channel_id",
        pst_start_channel_count_id_range[0],
    )
    validate(csp_ver, csp_config, 2)

    sync_first_channel_value(
        pr_config,
        timing_beam_config,
        "pst_start_channel_id",
        pst_start_channel_count_id_range[1],
    )
    validate(csp_ver, csp_config, 2)

    sync_first_channel_value(
        pr_config,
        timing_beam_config,
        "pst_start_channel_id",
        pst_start_channel_count_id_range[1] + 1,
    )
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)


@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(5, 0)))
def test_pst_bf_timing_beams_array(csp_ver):
    csp_config = get_csp_config_example(csp_ver, scan="pst_bf")

    processing_regions = csp_config["midcbf"]["pst_bf"]["processing_regions"]
    pr_config = processing_regions[0]

    timing_beam_array_length_range = (1, 16)
    example_timing_beam = copy.deepcopy(pr_config["timing_beams"][0])

    pr_config["timing_beams"] = [
        example_timing_beam
        for _ in range(0, timing_beam_array_length_range[0] - 1)
    ]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    pr_config["timing_beams"] = [
        example_timing_beam
        for _ in range(0, timing_beam_array_length_range[0])
    ]
    validate(csp_ver, csp_config, 2)

    pr_config["timing_beams"] = [
        example_timing_beam
        for _ in range(0, timing_beam_array_length_range[1])
    ]
    validate(csp_ver, csp_config, 2)

    pr_config["timing_beams"] = [
        example_timing_beam
        for _ in range(0, timing_beam_array_length_range[1] + 1)
    ]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)


@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(5, 0)))
def test_pst_bf_timing_beams_id(csp_ver):
    csp_config = get_csp_config_example(csp_ver, scan="pst_bf")

    processing_regions = csp_config["midcbf"]["pst_bf"]["processing_regions"]
    pr_config = processing_regions[0]
    timing_beam = pr_config["timing_beams"][0]

    timing_beam_id_range = (1, 16)

    # validate channel_id range
    _validate_value_range(
        timing_beam,
        csp_ver,
        csp_config,
        "timing_beam_id",
        timing_beam_id_range,
    )


@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(5, 0)))
def test_pst_bf_receptors(csp_ver):
    csp_config = get_csp_config_example(csp_ver, scan="pst_bf")

    processing_regions = csp_config["midcbf"]["pst_bf"]["processing_regions"]
    pr_config = processing_regions[0]
    timing_beam = pr_config["timing_beams"][0]

    # SKB-749: rather than check each 000 - 999 exhaustively, check smaller
    # subset of examples for time-saving.
    # ska range: 001 - 133
    # mkt range: 000 - 063
    valid_ska_dish_examples = ["001", "060", "133"]
    invalid_ska_dish_examples = ["000", "134", "999"]

    valid_mkt_dish_examples = ["000", "030", "063"]
    invalid_mkt_dish_examples = ["064", "133", "999"]

    for example in valid_ska_dish_examples:
        timing_beam["receptors"] = ["SKA" + example]
        validate(csp_ver, csp_config, 2)

    for example in invalid_ska_dish_examples:
        timing_beam["receptors"] = ["SKA" + example]
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)

    for example in valid_mkt_dish_examples:
        timing_beam["receptors"] = ["MKT" + example]
        validate(csp_ver, csp_config, 2)

    for example in invalid_mkt_dish_examples:
        timing_beam["receptors"] = ["MKT" + example]
        with pytest.raises(ValueError):
            validate(csp_ver, csp_config, 2)


@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(5, 0)))
def test_pst_bf_output_link_map(csp_ver):
    csp_config = get_csp_config_example(csp_ver, scan="pst_bf")

    processing_regions = csp_config["midcbf"]["pst_bf"]["processing_regions"]
    pr_config = processing_regions[0]
    timing_beam = pr_config["timing_beams"][0]

    channel_id_range = (0, MAX_SIGNED_32BIT_INT)

    # test at least one mapping
    timing_beam["output_link_map"] = []
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    # Test channel id range
    _validate_channel_map_channel_id(
        timing_beam,
        csp_ver,
        csp_config,
        pr_config,
        "output_link_map",
        channel_id_range,
        1,
        1,
        "pst_start_channel_id",
    )

    # test mapping length
    sync_first_channel_value(pr_config, timing_beam, "pst_start_channel_id", 1)
    timing_beam["output_link_map"] = [[1]]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    timing_beam["output_link_map"] = [[1, 1]]
    validate(csp_ver, csp_config, 2)

    timing_beam["output_link_map"] = [[1, 1, 1]]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    # test ascending value
    sync_first_channel_value(
        pr_config, timing_beam, "pst_start_channel_id", channel_id_range[0]
    )
    timing_beam["output_link_map"] = [
        [channel_id_range[0], 1],
        [channel_id_range[0] + 1, 1],
    ]
    validate(csp_ver, csp_config, 2)

    timing_beam["output_link_map"] = [
        [channel_id_range[0], 1],
        [channel_id_range[0], 1],
    ]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    sync_first_channel_value(
        pr_config, timing_beam, "pst_start_channel_id", channel_id_range[0] + 1
    )
    timing_beam["output_link_map"] = [
        [channel_id_range[0] + 1, 1],
        [channel_id_range[0], 1],
    ]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    # test link value
    sync_first_channel_value(
        pr_config, timing_beam, "pst_start_channel_id", channel_id_range[0]
    )
    timing_beam["output_link_map"] = [[channel_id_range[0], 0]]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    timing_beam["output_link_map"] = [[channel_id_range[0], 1]]
    validate(csp_ver, csp_config, 2)

    timing_beam["output_link_map"] = [[channel_id_range[0], 2]]
    validate(csp_ver, csp_config, 2)

    # Test not synced with pst_start_channel_id
    sync_first_channel_value(
        pr_config, timing_beam, "pst_start_channel_id", channel_id_range[0]
    )
    timing_beam["output_link_map"] = [[channel_id_range[0] + 1, 1]]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)


@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(5, 0)))
def test_pst_bf_output_host(csp_ver):
    csp_config = get_csp_config_example(csp_ver, scan="pst_bf")

    processing_regions = csp_config["midcbf"]["pst_bf"]["processing_regions"]
    pr_config = processing_regions[0]
    timing_beam = pr_config["timing_beams"][0]

    channel_id_increment = 1

    channel_id_range = (0, MAX_SIGNED_32BIT_INT)
    valid_host = "1.1.1.1"

    # Test at least one mapping
    timing_beam["output_host"] = []
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    # Test channel id range
    _validate_channel_map_channel_id(
        timing_beam,
        csp_ver,
        csp_config,
        pr_config,
        "output_host",
        channel_id_range,
        channel_id_increment,
        valid_host,
        "pst_start_channel_id",
    )

    # test ascending value
    timing_beam["output_host"] = [
        [channel_id_range[0], valid_host],
        [channel_id_range[0] + channel_id_increment, valid_host],
    ]
    validate(csp_ver, csp_config, 2)

    timing_beam["output_host"] = [
        [channel_id_range[0] + channel_id_increment, valid_host],
        [channel_id_range[0], valid_host],
    ]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    # test host value
    timing_beam["output_host"] = [[channel_id_range[0], "invalid ip"]]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    timing_beam["output_host"] = [[channel_id_range[0], valid_host]]
    validate(csp_ver, csp_config, 2)

    # Test not synced with pst_start_channel_id
    sync_first_channel_value(
        pr_config, timing_beam, "pst_start_channel_id", channel_id_range[0]
    )
    timing_beam["output_host"] = [[channel_id_range[0] + 1, valid_host]]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)


@pytest.mark.parametrize("csp_ver", csp_config_versions(min_ver=(5, 0)))
def test_pst_bf_output_port(csp_ver):
    csp_config = get_csp_config_example(csp_ver, scan="pst_bf")

    processing_regions = csp_config["midcbf"]["pst_bf"]["processing_regions"]
    pr_config = processing_regions[0]
    timing_beam = pr_config["timing_beams"][0]

    channel_id_increment = 1
    channel_id_range = (0, MAX_SIGNED_32BIT_INT)
    port_range = (0, MAX_PORT_VALUE)

    # Test at least one mapping
    timing_beam["output_port"] = []
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    # Test mapping size
    timing_beam["output_port"] = [[channel_id_range[0], port_range[0], 1]]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    # Test channel id range
    _validate_channel_map_channel_id(
        timing_beam,
        csp_ver,
        csp_config,
        pr_config,
        "output_port",
        channel_id_range,
        channel_id_increment,
        port_range[0],
        "pst_start_channel_id",
    )

    # Test channel_ids in ascending order
    timing_beam["output_port"] = [
        [channel_id_range[0], port_range[0]],
        [channel_id_range[0] + channel_id_increment, port_range[0] + 1],
    ]
    validate(csp_ver, csp_config, 2)

    timing_beam["output_port"] = [
        [channel_id_range[0] + channel_id_increment, port_range[0] + 1],
        [channel_id_range[0], port_range[0]],
    ]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    # Test port range
    timing_beam["output_port"] = [[channel_id_range[0], port_range[0] - 1]]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    timing_beam["output_port"] = [[channel_id_range[0], port_range[0]]]
    validate(csp_ver, csp_config, 2)

    timing_beam["output_port"] = [[channel_id_range[0], port_range[1]]]
    validate(csp_ver, csp_config, 2)

    timing_beam["output_port"] = [[channel_id_range[0], port_range[1] + 1]]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    # Test not synced with pst_start_channel_id
    sync_first_channel_value(
        pr_config, timing_beam, "pst_start_channel_id", channel_id_range[0]
    )
    timing_beam["output_port"] = [[channel_id_range[0] + 1, port_range[0]]]
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)


def _validate_value_range(to_validate, csp_ver, csp_config, parameter, range):
    """validate a simple integer value at the edges of the min and max range

    :param to_validate: the config to validate
    :param csp_ver: the csp config version
    :param csp_config: the overall csp_config
    :param parameter: name of the parameter to validate
    :param range: tuple (min, max) to validate against (inclusive)
    """
    to_validate[parameter] = range[0] - 1
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    to_validate[parameter] = range[0]
    validate(csp_ver, csp_config, 2)

    to_validate[parameter] = range[1]
    validate(csp_ver, csp_config, 2)

    to_validate[parameter] = range[1] + 1
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)


def _validate_channel_map_channel_id(
    to_validate,
    csp_ver,
    csp_config,
    pr_config,
    parameter,
    range,
    increment,
    valid_map_value,
    start_channel_param_name,
):
    """validate a the channel_map channel_id value at the edges of the min
    and max range

    :param to_validate: the config to validate
    :param csp_ver: the csp config version
    :param csp_config: the overall csp_config
    :param pr_config: the processing region config
    :param parameter: name of the parameter to validate
    :param range: tuple (min, max) to validate against (inclusive)
    :param increment: increment between valid values
    :param valid_map_value: a valid channel map value for the entry
    :param start_channel_param_name the property name that the first value
           should be (e.g. pst_start_channel_id, sdp_start_channel_id)
    """

    to_validate[parameter] = [[range[0] - increment, valid_map_value]]
    sync_first_channel_value(
        pr_config, to_validate, start_channel_param_name, range[0] - increment
    )
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    pr_config[start_channel_param_name] = range[0]
    sync_first_channel_value(
        pr_config, to_validate, start_channel_param_name, range[0]
    )
    validate(csp_ver, csp_config, 2)

    to_validate[parameter] = [[range[1], valid_map_value]]
    sync_first_channel_value(
        pr_config, to_validate, start_channel_param_name, range[1]
    )
    validate(csp_ver, csp_config, 2)

    to_validate[parameter] = [[range[1] + increment, valid_map_value]]
    sync_first_channel_value(
        pr_config, to_validate, start_channel_param_name, range[1] + increment
    )
    with pytest.raises(ValueError):
        validate(csp_ver, csp_config, 2)

    # reset values
    sync_first_channel_value(
        pr_config, to_validate, start_channel_param_name, 0
    )


def sync_first_channel_value(
    pr_config, config, start_channel_param_name, value_to_sync
):
    """sync the first channel id to that of the given value. used to make
    values like pst_start_channel_id are set in the right places.

    :param pr_config: the processing region config
    :param config: the config where output_* parameters live in because it is
        not neccesarily in the pr_config.
    :param start_channel_param_name: the name of the start_channel_id param in
        in the pr_config
    :param value_to_sync: value that all these parameters should be set to
    """
    pr_config[start_channel_param_name] = value_to_sync

    # sometimes these values are optional
    if "output_link_map" in config:
        config["output_link_map"][0][0] = value_to_sync

    if "output_port" in config:
        config["output_port"][0][0] = value_to_sync

    if "output_host" in config:
        config["output_host"][0][0] = value_to_sync
