import os
import tempfile
from collections import namedtuple
from pathlib import Path
from unittest import mock
from unittest.mock import call, patch

from ska_telmodel.data.large_files import (
    _basic_auth,
    _create_download_url,
    _get_user_details,
    is_large_file_cached,
    large_file_download,
    large_file_hash,
    large_file_link_content,
    large_file_search,
    large_file_upload_to_car,
    upload_large_file,
)


class MockResponse:
    def __init__(self, status_code, data, size=5):
        self.status_code = status_code
        self.data = data
        self.size = size

    def json(self):
        return self.data

    def iter_content(self, chunk_size):
        return [b"1" * chunk_size for _ in range(self.size)]


@patch("ska_telmodel.data.large_files._get_utc_time")
def test_large_file_link_content_basic(mock_datetime):
    mock_datetime.return_value = "2024-05-04T13:32:12"
    output = large_file_link_content(
        "123ABC", "https://fake.url/with/path.item"
    )

    help_line = "library as normal (with or without the `.link` suffix)"

    assert (
        output
        == f"""# This is a link file.
# The original file is considered too large to be stored in GitLab.

# To download the contents do one of the following:
# - use the CLI: `ska-telmodel cat <filename>`
# - use the CLI: `ska-telmodel cp <filename> <location>`
# - Use the `downloadUrl` value to download the file directly
# - Use the `ska-telmodel` {help_line}
# File added on 2024-05-04T13:32:12 UTC

file_hash = "123ABC"
downloadUrl = "https://fake.url/with/path.item"
fileSize = -1
"""
    )


@patch("ska_telmodel.data.large_files._get_utc_time")
def test_large_file_link_content_normal(mock_datetime):
    mock_datetime.return_value = "2024-05-04T13:32:12"
    output = large_file_link_content(
        "123ABC",
        "https://fake.url/with/path.item",
        key_path="with/path/file.name",
        file_size=123,
    )

    help_line = "library as normal (with or without the `.link` suffix)"

    assert (
        output
        == f"""# This is a link file.
# The original file is considered too large to be stored in GitLab.

# To download the contents do one of the following:
# - use the CLI: `ska-telmodel cat with/path/file.name`
# - use the CLI: `ska-telmodel cp with/path/file.name <location>`
# - Use the `downloadUrl` value to download the file directly
# - Use the `ska-telmodel` {help_line}
# File added on 2024-05-04T13:32:12 UTC

file_hash = "123ABC"
downloadUrl = "https://fake.url/with/path.item"
fileSize = 123
"""
    )


def test_get_user_details_env(monkeypatch):
    with mock.patch.dict(os.environ, clear=True):
        monkeypatch.setenv("CAR_TMDATA_USERNAME", "local_user")
        monkeypatch.setenv("CAR_TMDATA_PASSWORD", "local_password")

        details = _get_user_details()

    assert details == "local_user:local_password"


@patch("ska_telmodel.data.large_files.getpass")
def test_get_user_details_stdin(mock_getpass, monkeypatch):
    mock_getpass.getpass.return_value = "stdin_password"
    monkeypatch.setattr("builtins.input", lambda _: "stdin_user")
    with mock.patch.dict(os.environ, clear=True):
        details = _get_user_details()

    assert details == "stdin_user:stdin_password"


def test_basic_auth():
    assert _basic_auth("user:pass") == "Basic dXNlcjpwYXNz"


def test_download_url_ext():
    repo = "https://artefact.skao.int/repository"
    assert (
        _create_download_url("123ABC", "file", "repo")
        == f"{repo}/sts-608/gitlab/repo/largefiles/123ABC.file"
    )


def test_download_url():
    repo = "https://artefact.skao.int/repository"
    assert (
        _create_download_url("123ABC", ".file", "repo")
        == f"{repo}/sts-608/gitlab/repo/largefiles/123ABC.file"
    )


def test_large_file_hash():
    assert (
        large_file_hash(Path("tests/upload_backend_test_data/linky.file.link"))
        == "021da6320476658171482131d74182b56625d932aecee93b7ffc53d536442f5a"
    )


@patch("ska_telmodel.data.large_files.requests")
def test_upload_basic(mock_requests, monkeypatch):
    Response = namedtuple("Response", ["status_code"])
    mock_requests.request.return_value = Response(204)

    with mock.patch.dict(os.environ, clear=True):
        monkeypatch.setenv("CAR_TMDATA_USERNAME", "local_user")
        monkeypatch.setenv("CAR_TMDATA_PASSWORD", "local_password")

        status, link = large_file_upload_to_car(
            Path("tests/upload_backend_test_data/linky.file.link"),
            "123ABC",
            "repo",
        )

    repo = "https://artefact.skao.int/repository"
    assert status is True
    assert link == f"{repo}/sts-608/gitlab/repo/largefiles/123ABC.link"

    assert mock_requests.mock_calls == [
        call.request(
            "POST",
            "https://artefact.skao.int/service/rest/v1/components",
            params={"repository": "sts-608"},
            headers={
                "Authorization": "Basic bG9jYWxfdXNlcjpsb2NhbF9wYXNzd29yZA=="
            },
            data={
                "raw.directory": "gitlab/repo/largefiles",
                "raw.asset1.filename": "123ABC.link",
            },
            files=[
                (
                    "raw.asset1",
                    ("linky.file.link", mock.ANY, "application/binary"),
                )
            ],
        )
    ]


@patch("ska_telmodel.data.large_files.requests")
def test_upload_failed(mock_requests, monkeypatch):
    Response = namedtuple("Response", ["status_code"])
    mock_requests.request.return_value = Response(400)

    with mock.patch.dict(os.environ, clear=True):
        monkeypatch.setenv("CAR_TMDATA_USERNAME", "local_user")
        monkeypatch.setenv("CAR_TMDATA_PASSWORD", "local_password")

        status, link = large_file_upload_to_car(
            Path("tests/upload_backend_test_data/linky.file.link"),
            "123ABC",
            "repo",
        )

    assert status is False
    assert link is None

    assert mock_requests.mock_calls == [
        call.request(
            "POST",
            "https://artefact.skao.int/service/rest/v1/components",
            params={"repository": "sts-608"},
            headers={
                "Authorization": "Basic bG9jYWxfdXNlcjpsb2NhbF9wYXNzd29yZA=="
            },
            data={
                "raw.directory": "gitlab/repo/largefiles",
                "raw.asset1.filename": "123ABC.link",
            },
            files=[
                (
                    "raw.asset1",
                    ("linky.file.link", mock.ANY, "application/binary"),
                )
            ],
        )
    ]


@patch("ska_telmodel.data.large_files.cache_path")
def test_is_large_file_cached_exists(mock_cache):
    with tempfile.TemporaryDirectory() as tmp_dir:
        mock_cache.return_value = Path(tmp_dir)
        Path(tmp_dir, "test_file").touch()
        cached, path = is_large_file_cached("test_file")

    assert cached is True
    assert str(path) == str(Path(tmp_dir, "test_file"))


@patch("ska_telmodel.data.large_files.cache_path")
def test_is_large_file_cached_not_exists(mock_cache):
    with tempfile.TemporaryDirectory() as tmp_dir:
        mock_cache.return_value = Path(tmp_dir)
        cached, path = is_large_file_cached("test_file")

    assert cached is False
    assert str(path) == str(Path(tmp_dir, "test_file"))


@patch("ska_telmodel.data.large_files.requests")
def test_large_file_search_success(mock_requests):
    mock_requests.get.return_value = MockResponse(
        200,
        {"items": [{"downloadUrl": "http://link/to/file", "fileSize": 1000}]},
    )

    success, url, size = large_file_search("hash", "ext", "repo")

    assert mock_requests.get.mock_calls == [
        call(
            "https://artefact.skao.int/service/rest/v1/search/assets",
            params={"repository": "sts-608", "sha256": "hash"},
        )
    ]
    assert success is True
    assert url == "http://link/to/file"
    assert size == 1000


@patch("ska_telmodel.data.large_files.requests")
def test_large_file_search_failed(mock_requests):
    mock_requests.get.return_value = MockResponse(
        404,
        {"items": []},
    )

    success, url, size = large_file_search("hash", "ext", "repo")

    assert success is False
    repo = "https://artefact.skao.int/repository"
    assert url == f"{repo}/sts-608/gitlab/repo/largefiles/hash.ext"
    assert size == 0


@patch("ska_telmodel.data.large_files.requests")
def test_large_file_search__no_items(mock_requests):
    mock_requests.get.return_value = MockResponse(
        200,
        {"items": []},
    )

    success, url, size = large_file_search("hash", "ext", "repo")

    assert success is False
    repo = "https://artefact.skao.int/repository"
    assert url == f"{repo}/sts-608/gitlab/repo/largefiles/hash.ext"
    assert size == 0


@patch("ska_telmodel.data.large_files.requests")
def test_large_file_download_not_found(mock_requests):
    mock_requests.get.return_value = MockResponse(
        404,
        {},
    )

    path = large_file_download("hash", print_progress=False)

    assert path is None


@patch("ska_telmodel.data.large_files.requests")
def test_large_file_download_download_failed(mock_requests):
    def fake_request(
        url, params=None, allow_redirects=False, stream=False, timeout=30
    ):
        if url.endswith("/v1/search/assets"):
            return MockResponse(
                200,
                {
                    "items": [
                        {
                            "downloadUrl": "http://link/to/file",
                            "fileSize": 1000,
                        }
                    ]
                },
            )
        resp = MockResponse(
            404,
            None,
        )
        return resp

    mock_requests.get = fake_request

    path = large_file_download("hash", print_progress=False)

    assert path is None


@patch("ska_telmodel.data.large_files.requests")
@patch("ska_telmodel.data.large_files.cache_path")
def test_large_file_download_download(mock_cache, mock_requests):
    def fake_request(
        url, params=None, allow_redirects=False, stream=False, timeout=30
    ):
        if url.endswith("/v1/search/assets"):
            return MockResponse(
                200,
                {
                    "items": [
                        {
                            "downloadUrl": "http://link/to/file",
                            "fileSize": 1000,
                        }
                    ]
                },
            )
        resp = MockResponse(200, data="123")
        return resp

    mock_requests.get = fake_request

    with tempfile.TemporaryDirectory() as tmp_dir:
        mock_cache.return_value = Path(tmp_dir, "large_files")
        path = large_file_download("hash", print_progress=False)

    assert str(path) == f"{tmp_dir}/large_files/hash"


@patch("ska_telmodel.data.large_files.requests")
@patch("ska_telmodel.data.large_files.cache_path")
def test_large_file_download_download_no_download(mock_cache, mock_requests):
    def fake_request(
        url, params=None, allow_redirects=False, stream=False, timeout=30
    ):
        if url.endswith("/v1/search/assets"):
            return MockResponse(
                200,
                {
                    "items": [
                        {
                            "downloadUrl": "http://link/to/file",
                            "fileSize": 1000,
                        }
                    ]
                },
            )
        resp = MockResponse(200, data="123")
        return resp

    mock_requests.get = fake_request

    with tempfile.TemporaryDirectory() as tmp_dir:
        mock_cache.return_value = Path(tmp_dir)
        Path(tmp_dir, "hash").touch()
        path = large_file_download("hash", print_progress=False)

    assert str(path) == f"{tmp_dir}/hash"
    assert mock_requests.mock_calls == []


@patch("ska_telmodel.data.large_files.large_file_hash")
@patch("ska_telmodel.data.large_files.large_file_search")
@patch("ska_telmodel.data.large_files.large_file_upload_to_car")
@patch("ska_telmodel.data.large_files.large_file_link_content")
def test_upload_large_files_exists(
    mock_link_file, mock_upload, mock_search, mock_hash
):
    mock_hash.return_value = "hash123"
    mock_search.return_value = (True, "https://download/url", 123)
    mock_upload.return_value = (True, "https://download/url2")
    mock_link_file.return_value = "link file"

    with tempfile.TemporaryDirectory() as tmp_dir:
        test_file = Path(tmp_dir, "test.file")
        with test_file.open("w") as wf:
            wf.write("contents: empty")

        content = upload_large_file("repo", test_file, "new/key/locaion.file")

    assert mock_hash.mock_calls == [call(test_file)]
    assert mock_search.mock_calls == [call("hash123", ".file", "repo")]
    assert mock_upload.mock_calls == []
    assert mock_link_file.mock_calls == [
        call(
            "hash123",
            "https://download/url",
            key_path="new/key/locaion.file",
            file_size=15,
        )
    ]
    assert content == "link file"


@patch("ska_telmodel.data.large_files.large_file_hash")
@patch("ska_telmodel.data.large_files.large_file_search")
@patch("ska_telmodel.data.large_files.large_file_upload_to_car")
@patch("ska_telmodel.data.large_files.large_file_link_content")
def test_upload_large_files_not_exists(
    mock_link_file, mock_upload, mock_search, mock_hash
):
    mock_hash.return_value = "hash123"
    mock_search.return_value = (False, "https://download/url", 0)
    mock_upload.return_value = (True, "https://download/url2")
    mock_link_file.return_value = "link file"

    with tempfile.TemporaryDirectory() as tmp_dir:
        test_file = Path(tmp_dir, "test.file")
        with test_file.open("w") as wf:
            wf.write("contents: empty")

        content = upload_large_file("repo", test_file, "new/key/locaion.file")

    assert mock_hash.mock_calls == [call(test_file)]
    assert mock_search.mock_calls == [call("hash123", ".file", "repo")]
    assert mock_upload.mock_calls == [call(test_file, "hash123", "repo")]
    assert mock_link_file.mock_calls == [
        call(
            "hash123",
            "https://download/url2",
            key_path="new/key/locaion.file",
            file_size=15,
        )
    ]
    assert content == "link file"
