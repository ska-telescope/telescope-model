import pathlib
import tempfile
from unittest.mock import patch

from ska_telmodel.data import TMData
from ska_telmodel.data.large_files import large_file_link_content


@patch("ska_telmodel.data.frontend.large_file_download")
def test_get_link_with_link(mock_download):
    with tempfile.TemporaryDirectory("ska-telmodel") as dirname:
        parent = pathlib.Path(dirname)
        mock_download.return_value = parent / "download_content"
        with (parent / "download_content").open("w") as file:
            file.write("Lorem Ipsum")
        with open(parent / "frontend.py.link", "w") as f:
            content = large_file_link_content(
                "123abc",
                "http://test.link/file.link",
                "tmdata/frontend.py",
                123,
            )
            f.write(content)

        tmdata = TMData([f"file://{dirname}"], update=True)
        assert tmdata["frontend.py.link"].get().decode() == "Lorem Ipsum"


@patch("ska_telmodel.data.frontend.large_file_download")
def test_get_link_without_link(mock_download):
    with tempfile.TemporaryDirectory("ska-telmodel") as dirname:
        parent = pathlib.Path(dirname)
        mock_download.return_value = parent / "download_content"
        with (parent / "download_content").open("w") as file:
            file.write("Lorem Ipsum")
        with open(parent / "frontend.py.link", "w") as f:
            content = large_file_link_content(
                "123abc",
                "http://test.link/file.link",
                "tmdata/frontend.py",
                123,
            )
            f.write(content)

        tmdata = TMData([f"file://{dirname}"], update=True)
        assert tmdata["frontend.py"].get().decode() == "Lorem Ipsum"


@patch("ska_telmodel.data.frontend.large_file_download")
def test_contains_link_with_link(mock_download):
    with tempfile.TemporaryDirectory("ska-telmodel") as dirname:
        parent = pathlib.Path(dirname)
        mock_download.return_value = parent / "download_content"
        with (parent / "download_content").open("w") as file:
            file.write("Lorem Ipsum")
        with open(parent / "frontend.py.link", "w") as f:
            content = large_file_link_content(
                "123abc",
                "http://test.link/file.link",
                "tmdata/frontend.py",
                123,
            )
            f.write(content)

        tmdata = TMData([f"file://{dirname}"], update=True)
        assert "frontend.py.link" in tmdata


@patch("ska_telmodel.data.frontend.large_file_download")
def test_contains_link_without_link(mock_download):
    with tempfile.TemporaryDirectory("ska-telmodel") as dirname:
        parent = pathlib.Path(dirname)
        mock_download.return_value = parent / "download_content"
        with (parent / "download_content").open("w") as file:
            file.write("Lorem Ipsum")
        with open(parent / "frontend.py.link", "w") as f:
            content = large_file_link_content(
                "123abc",
                "http://test.link/file.link",
                "tmdata/frontend.py",
                123,
            )
            f.write(content)

        tmdata = TMData([f"file://{dirname}"], update=True)
        assert "frontend.py" in tmdata
