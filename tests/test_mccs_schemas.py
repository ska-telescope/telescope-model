import pytest

from ska_telmodel.mccs.version import (
    MCCS_ASSIGNEDRES_PREFIX,
    MCCS_ASSIGNRES_PREFIX,
    MCCS_CONFIGURE_PREFIX,
    MCCS_RELEASERES_PREFIX,
    MCCS_SCAN_PREFIX,
    mccs_assignedres_uri,
    mccs_assignres_uri,
    mccs_configure_uri,
    mccs_releaseres_uri,
    mccs_scan_uri,
)
from ska_telmodel.schema import example_by_uri, validate


def test_mccs_assigned_ressources():
    """Test MCCS assigned resources schema correctly validates
    the expected JSON.
    """
    assigned_ver = mccs_assignedres_uri(1, 0)

    # Test with string containing allocated resources
    validate(assigned_ver, example_by_uri(assigned_ver), 2)

    # Test when no resources are allocated to sub-array
    validate(assigned_ver, example_by_uri(assigned_ver, True), 2)

    # Try with interface version given as part of JSON object
    mccs_assigned_versioned = example_by_uri(assigned_ver)
    mccs_assigned_versioned["interface"] = assigned_ver
    validate(None, mccs_assigned_versioned, 1)

    # Check invalid version raises error
    with pytest.raises(ValueError, match=r"Could not generate example"):
        example_by_uri(MCCS_ASSIGNEDRES_PREFIX + "0.0")


@pytest.mark.parametrize(
    "field_name, invalid_input",
    [
        ("subarray_beam_ids", [1, 2]),
        ("subarray_beam_ids", [50]),
        ("station_ids", [[513]]),
        ("channel_blocks", [1] * 50),
    ],
)
def test_mccs_assigned_ressources_invalid_input(field_name, invalid_input):
    """Test MCCS assigned resources schema throws an error on invalid input."""
    assigned_ver = mccs_assignedres_uri(1, 0)
    assigned_json = example_by_uri(assigned_ver)
    assigned_json[field_name] = invalid_input

    with pytest.raises(ValueError, match=r"should evaluate to True"):
        validate(assigned_ver, assigned_json, 2)


def test_mccs_assign_ressources():
    """Test MCCS resource allocation schema correctly validates
    the expected JSON.
    """
    assign_ver = mccs_assignres_uri(1, 0)
    validate(assign_ver, example_by_uri(assign_ver), 2)

    # Check that an error is raised if 'subarray_id' is omitted
    mccs_assign_erronous = example_by_uri(assign_ver)
    del mccs_assign_erronous["subarray_id"]
    with pytest.raises(ValueError):
        validate(assign_ver, mccs_assign_erronous, 1)

    # Try with interface version given as part of JSON object
    mccs_assign_versioned = example_by_uri(assign_ver)
    mccs_assign_versioned["interface"] = assign_ver
    validate(None, mccs_assign_versioned, 1)

    # Check invalid version raises error
    with pytest.raises(ValueError, match=r"Could not generate example"):
        example_by_uri(MCCS_ASSIGNRES_PREFIX + "0.0")


@pytest.mark.parametrize(
    "field_name, invalid_input",
    [
        ("subarray_id", 50),
        ("subarray_id", -1),
        ("subarray_beam_ids", [1, 2]),
        ("subarray_beam_ids", [50]),
        ("station_ids", [[513]]),
        ("channel_blocks", [1] * 50),
    ],
)
def test_mccs_assign_ressources_invalid_input(field_name, invalid_input):
    """Test MCCS assign resources schema throws an error on invalid input."""
    assign_ver = mccs_assignres_uri(1, 0)
    assign_json = example_by_uri(assign_ver)
    assign_json[field_name] = invalid_input

    with pytest.raises(ValueError, match=r"should evaluate to True"):
        validate(assign_ver, assign_json, 2)


def test_mccs_release_ressources():
    """Test MCCS resource release schema correctly validates
    the expected JSON.
    """
    release_ver = mccs_releaseres_uri(1, 0)
    validate(release_ver, example_by_uri(release_ver), 2)

    # Check that an error is raised if 'subarray_id' is omitted
    mccs_release_erronous = example_by_uri(release_ver)
    del mccs_release_erronous["subarray_id"]
    validate(release_ver, mccs_release_erronous, 0)
    with pytest.raises(ValueError):
        validate(release_ver, mccs_release_erronous, 1)

    # Try with interface version given as part of JSON object
    mccs_release_versioned = example_by_uri(release_ver)
    mccs_release_versioned["interface"] = release_ver
    validate(None, mccs_release_versioned, 1)

    # Check invalid version raises error
    with pytest.raises(ValueError, match=r"Could not generate example"):
        example_by_uri(MCCS_RELEASERES_PREFIX + "0.0")


@pytest.mark.parametrize(
    "field_name, invalid_input", [("subarray_id", 50), ("subarray_id", -1)]
)
def test_mccs_release_ressources_invalid_input(field_name, invalid_input):
    """Test MCCS release resources schema throws an error on invalid input."""
    release_ver = mccs_releaseres_uri(1, 0)
    release_json = example_by_uri(release_ver)
    release_json[field_name] = invalid_input

    with pytest.raises(ValueError, match=r"should evaluate to True"):
        validate(release_ver, release_json, 2)


def test_mccs_configure():
    """Test MCCS configure schema correctly validates the
    expected JSON.
    """
    configure_ver = mccs_configure_uri(1, 0)
    validate(configure_ver, example_by_uri(configure_ver), 2)

    # Try with interface version given as part of JSON object
    mccs_configure_versioned = example_by_uri(configure_ver)
    mccs_configure_versioned["interface"] = configure_ver
    validate(None, mccs_configure_versioned, 1)

    # Check invalid version raises error
    with pytest.raises(ValueError, match=r"Could not generate example"):
        example_by_uri(MCCS_CONFIGURE_PREFIX + "0.0")


@pytest.mark.parametrize(
    "field_name, invalid_input",
    [
        ("stations", [{"station_id": 513}]),
        ("stations", [{"station_id": 1}] * 513),
    ],
)
def test_mccs_configure_invalid_input(field_name, invalid_input):
    """Test MCCS configure schema throws an error on invalid input."""
    configure_ver = mccs_configure_uri(1, 0)
    configure_json = example_by_uri(configure_ver)
    configure_json[field_name] = invalid_input

    with pytest.raises(ValueError, match=r"should evaluate to True"):
        validate(configure_ver, configure_json, 2)


@pytest.mark.parametrize(
    "field_name, invalid_input",
    [
        ("subarray_beam_id", 50),
        ("station_ids", [0, 1]),
        ("station_ids", [1, 513]),
        ("update_rate", -1.0),
        ("channels", [[0, 8, 1]]),
        ("channels", [[1, 8, 1, 1]]),
        ("channels", [[1, 1, 1, 1]]),
        ("channels", [[1, 1, 0, 1]]),
        ("channels", [[1, 1, 1, 10]]),
        ("antenna_weights", [0.0] * 513),
        ("antenna_weights", [-0.1]),
        ("antenna_weights", [256.1]),
        ("phase_centre", [0.0, 25.0]),
        ("phase_centre", [0.0, 0.0, 0.0]),
    ],
)
def test_mccs_configure_subarray_beam_invalid_input(field_name, invalid_input):
    """Test MCCS configure schema throws an error on invalid
    subarray beam field inputs."""
    configure_ver = mccs_configure_uri(1, 0)
    configure_json = example_by_uri(configure_ver)
    configure_json["subarray_beams"][0][field_name] = invalid_input

    with pytest.raises(ValueError, match=r"should evaluate to True"):
        validate(configure_ver, configure_json, 2)


def test_mccs_scan():
    """Test MCCS scan schema correctly validates the
    expected JSON.
    """
    scan_ver = mccs_scan_uri(1, 0)
    validate(scan_ver, example_by_uri(scan_ver), 2)

    # Check that an error is raised if 'scan_id' is omitted
    mccs_scan_erronous = example_by_uri(scan_ver)
    del mccs_scan_erronous["scan_id"]
    validate(scan_ver, mccs_scan_erronous, 0)
    with pytest.raises(ValueError):
        validate(scan_ver, mccs_scan_erronous, 1)

    # Try with interface version given as part of JSON object
    mccs_scan_versioned = example_by_uri(scan_ver)
    mccs_scan_versioned["interface"] = scan_ver
    validate(None, mccs_scan_versioned, 1)

    # Check invalid version raises error
    with pytest.raises(ValueError, match=r"Could not generate example"):
        example_by_uri(MCCS_SCAN_PREFIX + "0.0")


@pytest.mark.parametrize(
    "field_name, invalid_input",
    [
        ("start_time", -1.0),
    ],
)
def test_mccs_scan_invalid_input(field_name, invalid_input):
    """Test MCCS scan schema throws an error on invalid
    inputs."""
    scan_ver = mccs_scan_uri(1, 0)
    scan_json = example_by_uri(scan_ver)
    scan_json[field_name] = invalid_input

    with pytest.raises(ValueError, match=r"should evaluate to True"):
        validate(scan_ver, scan_json, 2)
