import pytest

from ska_telmodel.midcbf.version import MIDCBF_INITSYSPARAM_PREFIX
from ska_telmodel.schema import example_by_uri, validate

STRICT_ERRS = 2
PERMISSIVE_ERRS = 1
PERMISSIVE_WARN = 0

# list of all defined versions of the MidCBF schemas
IFACE_VERSIONS = ["1.0", "1.1"]


def test_midcbf_initsysparam():
    # all versions of example 'initsysparam' JSON
    # should pass schema checks
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        validate(
            initsysparam_uri, example_by_uri(initsysparam_uri), STRICT_ERRS
        )

    # example 'initsysparam' JSON with additional dish parameter
    # should pass schema checks
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        good_example = example_by_uri(initsysparam_uri)
        good_example["dish_parameters"]["MKT000"] = {"vcc": 197, "k": 2222}
        validate(initsysparam_uri, good_example, STRICT_ERRS)

    # example 'initsysparam' JSON with single dish parameter
    # should pass schema checks
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        good_example = example_by_uri(initsysparam_uri)
        good_example["dish_parameters"] = {"SKA133": {"vcc": 133, "k": 133}}
        validate(initsysparam_uri, good_example, STRICT_ERRS)

    # example 'initsysparam' JSON with missing interface field
    # should raise error
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        bad_example = example_by_uri(initsysparam_uri)
        del bad_example["interface"]
        with pytest.raises(ValueError):
            validate(initsysparam_uri, bad_example, STRICT_ERRS)

    # example 'initsysparam' JSON with missing dish_parameters field
    # should raise error
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        bad_example = example_by_uri(initsysparam_uri)
        del bad_example["dish_parameters"]
        with pytest.raises(ValueError):
            validate(initsysparam_uri, bad_example, STRICT_ERRS)

    # example 'initsysparam' JSON with additional top-level field
    # should raise error
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        bad_example = example_by_uri(initsysparam_uri)
        bad_example["bad field"] = "This is an invalid field"
        with pytest.raises(ValueError):
            validate(initsysparam_uri, bad_example, STRICT_ERRS)

    # example 'initsysparam' JSON with empty dish_parameters
    # should raise error
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        bad_example = example_by_uri(initsysparam_uri)
        bad_example["dish_parameters"] = {}
        with pytest.raises(ValueError):
            validate(initsysparam_uri, bad_example, STRICT_ERRS)

    # example 'initsysparam' JSON with invalid dish ID 'SKA000'
    # should raise error
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        bad_example = example_by_uri(initsysparam_uri)
        bad_example["dish_parameters"]["SKA000"] = {"vcc": 5, "k": 500}
        with pytest.raises(ValueError):
            validate(initsysparam_uri, bad_example, STRICT_ERRS)

    # example 'initsysparam' JSON with invalid dish ID 'SKA134'
    # should raise error
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        bad_example = example_by_uri(initsysparam_uri)
        bad_example["dish_parameters"]["SKA134"] = {"vcc": 134, "k": 1340}
        with pytest.raises(ValueError):
            validate(initsysparam_uri, bad_example, STRICT_ERRS)

    # example 'initsysparam' JSON with invalid dish ID 'MKT064'
    # should raise error
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        bad_example = example_by_uri(initsysparam_uri)
        bad_example["dish_parameters"]["MKT064"] = {"vcc": 64, "k": 640}
        with pytest.raises(ValueError):
            validate(initsysparam_uri, bad_example, STRICT_ERRS)

    # example 'initsysparam' JSON with missing vcc and k
    # should raise error
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        bad_example = example_by_uri(initsysparam_uri)
        bad_example["dish_parameters"]["SKA001"] = {}
        with pytest.raises(ValueError):
            validate(initsysparam_uri, bad_example, STRICT_ERRS)

    # example 'initsysparam' JSON with missing vcc ID
    # should raise error
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        bad_example = example_by_uri(initsysparam_uri)
        del bad_example["dish_parameters"]["SKA001"]["vcc"]
        with pytest.raises(ValueError):
            validate(initsysparam_uri, bad_example, STRICT_ERRS)

    # example 'initsysparam' JSON with missing k value
    # should raise error
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        bad_example = example_by_uri(initsysparam_uri)
        del bad_example["dish_parameters"]["SKA001"]["k"]
        with pytest.raises(ValueError):
            validate(initsysparam_uri, bad_example, STRICT_ERRS)

    # example 'initsysparam' JSON with extra field
    # should raise error
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        bad_example = example_by_uri(initsysparam_uri)
        bad_example["dish_parameters"]["SKA001"][
            "bad field"
        ] = "This is an invalid field"
        with pytest.raises(ValueError):
            validate(initsysparam_uri, bad_example, STRICT_ERRS)

    # example 'initsysparam' JSON with invalid vcc ID 0
    # should raise error
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        bad_example = example_by_uri(initsysparam_uri)
        bad_example["dish_parameters"]["SKA001"]["vcc"] = 0
        with pytest.raises(ValueError):
            validate(initsysparam_uri, bad_example, STRICT_ERRS)
    # example 'initsysparam' JSON with invalid vcc ID 198
    # should raise error
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        bad_example = example_by_uri(initsysparam_uri)
        bad_example["dish_parameters"]["SKA001"]["vcc"] = 198
        with pytest.raises(ValueError):
            validate(initsysparam_uri, bad_example, STRICT_ERRS)

    # example 'initsysparam' JSON with invalid k 0
    # should raise error
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        bad_example = example_by_uri(initsysparam_uri)
        bad_example["dish_parameters"]["SKA001"]["k"] = 0
        with pytest.raises(ValueError):
            validate(initsysparam_uri, bad_example, STRICT_ERRS)

    # example 'initsysparam' JSON with invalid k 2223
    # should raise error
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        bad_example = example_by_uri(initsysparam_uri)
        bad_example["dish_parameters"]["SKA001"]["k"] = 2223
        with pytest.raises(ValueError):
            validate(initsysparam_uri, bad_example, STRICT_ERRS)

    # example 'initsysparam' JSON with duplicate vcc IDs
    # should raise error
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        bad_example = example_by_uri(initsysparam_uri)
        bad_example["dish_parameters"]["SKA100"]["vcc"] = 1
        with pytest.raises(ValueError):
            validate(initsysparam_uri, bad_example, STRICT_ERRS)

    # Check invalid schema version raises error
    # (v0.0 will not exist, first recommended semantic version is v1.0)
    with pytest.raises(ValueError, match=r"Could not generate example"):
        example_by_uri(MIDCBF_INITSYSPARAM_PREFIX + "0.0")


def test_midcbf_initsysparam_with_uri():
    # all versions of example 'initsysparam' JSON
    # should pass schema checks
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        validate(
            initsysparam_uri,
            example_by_uri(initsysparam_uri, "uri"),
            STRICT_ERRS,
        )

    # example 'initsysparam' JSON with missing interface field
    # should raise error
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        bad_example = example_by_uri(initsysparam_uri, "uri")
        del bad_example["interface"]
        with pytest.raises(ValueError):
            validate(initsysparam_uri, bad_example, STRICT_ERRS)

    # example 'initsysparam' JSON with missing tm_data_sources field
    # should raise error
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        bad_example = example_by_uri(initsysparam_uri, "uri")
        del bad_example["tm_data_sources"]
        with pytest.raises(ValueError):
            validate(initsysparam_uri, bad_example, STRICT_ERRS)

    # example 'initsysparam' JSON with empty tm_data_sources field
    # should raise error
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        bad_example = example_by_uri(initsysparam_uri, "uri")
        bad_example["tm_data_sources"] = []
        with pytest.raises(ValueError):
            validate(initsysparam_uri, bad_example, STRICT_ERRS)

    # example 'initsysparam' JSON with invalid tm_data_sources length
    # should raise error
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        bad_example = example_by_uri(initsysparam_uri, "uri")
        bad_example["tm_data_sources"].append("extra data source")
        with pytest.raises(ValueError):
            validate(initsysparam_uri, bad_example, STRICT_ERRS)

    # example 'initsysparam' JSON with missing tm_data_filepath field
    # should raise error
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        bad_example = example_by_uri(initsysparam_uri, "uri")
        del bad_example["tm_data_filepath"]
        with pytest.raises(ValueError):
            validate(initsysparam_uri, bad_example, STRICT_ERRS)

    # example 'initsysparam' JSON with non-JSON tm_data_filepath
    # should raise error
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        bad_example = example_by_uri(initsysparam_uri, "uri")
        bad_example["tm_data_filepath"] = "invalid-path.yaml"
        with pytest.raises(ValueError):
            validate(initsysparam_uri, bad_example, STRICT_ERRS)

    # example 'initsysparam' JSON with tm_data_filepath containing whitespaces
    # should raise error
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        bad_example = example_by_uri(initsysparam_uri, "uri")
        bad_example["tm_data_filepath"] = "invalid path.json"
        with pytest.raises(ValueError):
            validate(initsysparam_uri, bad_example, STRICT_ERRS)

    # Check invalid schema version raises error
    # (v0.0 will not exist, first recommended semantic version is v1.0)
    with pytest.raises(ValueError, match=r"Could not generate example"):
        example_by_uri(MIDCBF_INITSYSPARAM_PREFIX + "0.0")

    # Check invalid format
    for version in IFACE_VERSIONS:
        initsysparam_uri = MIDCBF_INITSYSPARAM_PREFIX + version
        pattern = r"Could not generate initsysparam example for format"
        with pytest.raises(ValueError, match=pattern):
            example_by_uri(initsysparam_uri, "invalid_format")
