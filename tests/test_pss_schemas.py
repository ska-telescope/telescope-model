import pytest

from ska_telmodel.pss.examples import get_pss_config_example
from ska_telmodel.pss.version import PSS_CONFIG_VER0_0, PSS_CONFIG_VERSIONS
from ska_telmodel.schema import validate


@pytest.mark.parametrize("pss_version", PSS_CONFIG_VERSIONS)
def test_pss_configure_schema_is_valid(
    pss_version: str,
) -> None:
    scan_config = get_pss_config_example(pss_version)
    # assert (
    #     scan_config["interface"] == pss_version
    # ), f"expected {scan_config['interface']} to be {pss_version}"

    validate(pss_version, scan_config, 2)


def test_pss_configure_schema_with_invalid_version():
    """
    Test to verify invalid PSS version
    """

    with pytest.raises(
        ValueError, match=r"Could not generate example for schema*"
    ):
        get_pss_config_example(PSS_CONFIG_VER0_0)
