"""Top-level tests."""

import importlib
from unittest.mock import MagicMock, patch

import pytest
from jsonschema import validators
from schema import Literal, Optional, Or, Regex, SchemaError

from ska_telmodel._common import (
    TMSchema,
    lookup_example,
    lookup_module,
    lookup_schema,
)
from ska_telmodel.schema import (
    SchemaUri,
    _validate_schema_using_document_url,
    validate,
)


def test_schema_uri():
    prefix = "https://schema.skao.int/ska-name/"
    version = "0.1"
    uri = prefix + version
    schema_uri = SchemaUri(uri)
    assert str(schema_uri) == uri
    assert schema_uri.prefix == prefix
    assert schema_uri.version == version
    major, minor = schema_uri.major_minor
    assert major == 0
    assert minor == 1


def test_tmschema_getitem():
    tmschema = TMSchema()
    assert "test" not in tmschema
    with pytest.raises(IndexError):
        tmschema["test"]
    with pytest.raises(IndexError):
        del tmschema["test"]
    tmschema.add_field("test", int)
    assert "test" in tmschema
    assert tmschema["test"] == int
    tmschema.add_field("test2", str, description="foo")
    assert "test2" in tmschema
    assert tmschema["test2"] == str
    tmschema.add_opt_field("test3", str)
    assert "test3" in tmschema
    assert tmschema["test3"] == str
    tmschema.add_opt_field("test4", int, description="bar")
    assert "test4" in tmschema
    assert tmschema["test4"] == int

    del tmschema["test"]
    del tmschema["test2"]
    del tmschema["test3"]
    del tmschema["test4"]
    assert "test4" not in tmschema


def test_tmschema_find_field_recursive():
    tmschema = TMSchema()
    tmschema.add_field("test", int)
    tmschema.add_opt_field("test2", int)
    subschema1 = TMSchema()
    subschema1.add_field("test3", str)
    subschema1.add_opt_field("test4", str)
    tmschema.add_field("test5", subschema1)
    subschema2 = TMSchema()
    subschema2.add_field("test6", int)
    subschema2.add_opt_field("test7", int)
    tmschema.add_field("test8", [subschema2])
    subschema3 = TMSchema()
    subschema3.add_field("test9", int)
    subschema3.add_opt_field("test10", int)
    tmschema.add_field("test11", {"test12": subschema3})

    assert tmschema.find_field_recursive("test") is tmschema
    assert tmschema.find_field_recursive("test2") is tmschema
    assert tmschema.find_field_recursive("test3") is subschema1
    assert tmschema.find_field_recursive("test4") is subschema1
    assert tmschema.find_field_recursive("test6") is subschema2
    assert tmschema.find_field_recursive("test7") is subschema2
    assert tmschema.find_field_recursive("test9") is subschema3
    assert tmschema.find_field_recursive("test10") is subschema3


def test_tmschema_is_field_optional():
    tmschema = TMSchema()
    assert tmschema.is_field_optional("test") is None

    tmschema.add_field("test", int)
    assert tmschema.is_field_optional("test") is False
    tmschema.add_opt_field("test2", int)
    assert tmschema.is_field_optional("test2") is True

    # Check single subschema and if their fields can be optional
    subschema1 = TMSchema()
    subschema1.add_field("test3", str)
    subschema1.add_opt_field("test4", str)

    tmschema.add_field("test5", subschema1)
    assert tmschema.is_field_optional("test5") is False
    assert subschema1.is_field_optional("test3") is False
    assert subschema1.is_field_optional("test4") is True

    # Check list of subschemas and if their fields can be optional
    subschema2 = TMSchema()
    subschema2.add_field("test6", int)
    subschema2.add_opt_field("test7", int)

    tmschema.add_field("test8", [subschema2])
    assert tmschema.is_field_optional("test8") is False
    assert subschema2.is_field_optional("test6") is False
    assert subschema2.is_field_optional("test7") is True

    # Check subschemas (list or single) can be optional
    subschema3 = TMSchema()
    tmschema.add_opt_field("test9", subschema3)
    assert tmschema.is_field_optional("test9") is True

    subschema4 = TMSchema()
    tmschema.add_opt_field("test10", [subschema4])
    assert tmschema.is_field_optional("test10") is True

    # With all the subschemas, ensure invalid fields are still not found
    assert tmschema.is_field_optional("test11") is None


def test_validate_schema_using_document_url_valid():
    """Test validation with a valid schema and object."""
    schema_dict = {
        "type": "object",
        "properties": {
            "interface": {"type": "string"},
            "transaction_id": {"type": "string"},
        },
        "required": ["interface", "transaction_id"],
    }

    class SchemaDict(dict):
        """A dict that also has validator_cls and schema property
        for jsonschema validation."""

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.validator_cls = validators.Draft7Validator

        @property
        def schema(self):
            """Return self as the schema dict."""
            return self

    schema = SchemaDict(schema_dict)
    mock_cache = MagicMock()
    mock_cache.get_schema = MagicMock(return_value=schema)

    with patch("ska_telmodel.schema.SchemaCache", return_value=mock_cache):
        valid_obj = {
            "interface": "https://host/test_url",
            "transaction_id": "txn-001",
        }
        # Should not raise any exception
        _validate_schema_using_document_url(
            valid_obj,
            version="https://host/test_url",
        )


@patch("ska_telmodel.schema._validate_schema")
def test_validate_with_external_url(mock_validate_schema):
    """Test the validate function for external url"""
    schema_dict = {
        "type": "object",
        "properties": {
            "interface": {"type": "string"},
            "transaction_id": {"type": "string"},
        },
        "required": ["interface", "transaction_id"],
    }

    mock_validate_schema.side_effect = SchemaError("Strict validation")
    with patch(
        "ska_telmodel.schema._fetch_schema_from_cache",
        return_value=schema_dict,
    ):
        valid_obj = {
            "interface": "https://host/ska-telmodel/json_schema",
            "transaction_id": "txn-001",
        }
        validate(
            version="https://host/ska-telmodel/json_schema",
            config=valid_obj,
            jsonschema_fallback=True,
        )


@patch("ska_telmodel.schema._validate_schema")
def test_validate_with_external_url_raise_value_error(mock_validate_schema):
    """Test the validate function for external url"""
    schema_dict = {
        "type": "object",
        "properties": {
            "interface": {"type": "string"},
            "transaction_id": {"type": "string"},
            "transaction_id2": {"type": "string"},
        },
        "required": ["interface", "transaction_id"],
    }

    mock_validate_schema.side_effect = SchemaError("Strict validation")
    with patch(
        "ska_telmodel.schema._fetch_schema_from_cache",
        return_value=schema_dict,
    ):
        with pytest.raises(ValueError):
            valid_obj = {
                "interface": "https://host/ska-telmodel/json_schema/2.3"
            }
            validate(
                version="https://host/ska-telmodel/json_schema/2.3",
                config=valid_obj,
                jsonschema_fallback=True,
            )
    mock_validate_schema.side_effect = SchemaError("Strict validation")
    with pytest.raises(Exception):
        valid_obj = {"interface": "https://host/ska-telmodel/json_schema/2.3"}
        validate(
            version="https://host/ska-telmodel/json_schema/2.3",
            config=valid_obj,
            jsonschema_fallback=True,
        )


@patch("ska_telmodel.schema._validate_schema")
def test_validate_with_external_url_raise_schema_err(mock_validate_schema):
    """Test the validate function for external url"""
    schema_dict = {
        "type": "object",
        "properties": {
            "interface": {"type": "string"},
            "transaction_id": {"type": "string"},
        },
        "required": ["interface", "transaction_id", "transaction_id"],
    }

    mock_validate_schema.side_effect = SchemaError("Strict validation")
    with patch(
        "ska_telmodel.schema._fetch_schema_from_cache",
        return_value=schema_dict,
    ):
        with pytest.raises(ValueError):
            valid_obj = {
                "interface": "https://host/ska-telmodel/json_schema/2.3",
                "transaction_id": 1,
            }
            validate(
                version="https://host/ska-telmodel/json_schema/2.3",
                config=valid_obj,
                jsonschema_fallback=True,
            )


def test_lookup_module():
    # Make sure we can load some SDP schema module
    sdp_schemas = importlib.import_module("ska_telmodel.sdp.schemas")
    mod = lookup_module(
        sdp_schemas, "https://schema.skao.int/ska-sdp-configure/0.1"
    )
    assert mod == importlib.import_module("ska_telmodel.sdp.schemas.v0_1")

    # And that attempting to load a version that doesn't exist fails
    with pytest.raises(ModuleNotFoundError):
        mod = lookup_module(
            sdp_schemas, "https://schema.skao.int/ska-sdp-configure/0.0"
        )


def test_lookup_schema():
    # Make sure we can load some SDP schema module
    sdp_schemas = importlib.import_module("ska_telmodel.sdp.schemas")
    lookup_schema(
        sdp_schemas,
        "https://schema.skao.int/ska-sdp-scan/0.1",
        0,
        "get_sdp_scan",
    )

    # And that attempting to load a non-existent version module fails
    with pytest.raises(SchemaError):
        lookup_schema(
            sdp_schemas,
            "https://schema.skao.int/ska-sdp-scan/0.0",
            0,
            "get_sdp_scan",
        )

    # And that attempting to load a non-existent function fails
    with pytest.raises(SchemaError):
        lookup_schema(
            sdp_schemas,
            "https://schema.skao.int/ska-sdp-scan/0.1",
            0,
            "get_sdp_fantasy",
        )


def test_lookup_example():
    # Make sure we can load an SDP example module
    sdp_examples = importlib.import_module("ska_telmodel.sdp.examples")
    lookup_example(
        sdp_examples,
        "https://schema.skao.int/ska-sdp-scan/0.2",
        "get_sdp_scan",
    )

    # And that attempting to load a non-existent version module fails
    with pytest.raises(ValueError):
        lookup_example(
            sdp_examples,
            "https://schema.skao.int/ska-sdp-scan/0.0",
            "get_sdp_scan",
        )

    # And that attempting to load a non-existent function fails
    with pytest.raises(ValueError):
        lookup_example(
            sdp_examples,
            "https://schema.skao.int/ska-sdp-scan/0.1",
            "get_sdp_fantasy",
        )


def test_tmschema_stringify_keys_recursive():
    # Construct a fairly complicated nested schema, at the bottom of
    # which we use type / regular expression keys
    tmschema1 = TMSchema({str: str, Regex("pattern"): int})
    tmschema2 = TMSchema(
        {Optional(Literal("literal")): Or({"foo": tmschema1}, str)}
    )

    stringified = tmschema2.stringify_keys_recursive()

    assert "(any str)" in stringified["literal"].args[0]["foo"]
    assert (
        "(string matching Regex('pattern'))"
        in stringified["literal"].args[0]["foo"]
    )

    with pytest.raises(ValueError):
        TMSchema({1: str}).stringify_keys_recursive()


def test_invalid_schema_name():
    with pytest.raises(
        ValueError, match="Schema name is not a valid identifier"
    ):
        TMSchema(
            {},
            name="&",
            as_reference=True,
        )
