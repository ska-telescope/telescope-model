"""Tests for SchemaCache class."""
import pathlib
import tempfile
from unittest.mock import Mock, patch

import pytest
import requests

from ska_telmodel.data import cache
from ska_telmodel.data.schema_cache import SchemaCache


@pytest.fixture
def schema_cache():
    """Create a SchemaCache instance for testing."""
    return SchemaCache("http://example.com")


def test_get_cache_key(schema_cache):
    """Test the _get_cache_key method."""
    url_path = "/schema.json"
    expected_key = "schema_cache__schema.json"
    assert schema_cache._get_cache_key(url_path) == expected_key


def test_is_cache_valid_nonexistent_file(schema_cache):
    """Test _is_cache_valid with non-existent cache file."""
    assert not schema_cache._is_cache_valid("nonexistent.json")


def test_get_schema_not_in_cache(schema_cache, monkeypatch):
    """Test get_schema when schema is not in cache."""

    def mock_get(*args, **kwargs):
        response = requests.Response()
        response.status_code = 200
        response._content = b'{"type": "object"}'
        response._content_consumed = True
        response.url = args[0]

        def json_fn():
            return {"type": "object"}

        response.json = json_fn

        def ok_status():
            pass

        response.raise_for_status = ok_status

        return response

    monkeypatch.setattr("requests.get", mock_get)

    with tempfile.TemporaryDirectory() as tmpdir:
        monkeypatch.setattr(pathlib.Path, "home", lambda: pathlib.Path(tmpdir))
        cache_dir = cache.cache_path("schema.json", []).parent
        cache_dir.mkdir(parents=True, exist_ok=True)
        schema = schema_cache.get_schema("/schema.json")
        assert schema == {"type": "object"}


def test_is_cache_valid_expired_cache(schema_cache):
    """Test _is_cache_valid with an expired cache."""
    with patch(
        "ska_telmodel.data.cache.cache_exists", return_value=True
    ), patch(
        "ska_telmodel.data.cache.get_cache_time"
    ) as mock_get_cache_time, patch(
        "time.time", return_value=1000
    ):
        mock_get_cache_time.return_value.timestamp.return_value = (
            500  # Expired cache
        )
        assert not schema_cache._is_cache_valid("expired_key")


def test_get_schema(schema_cache, monkeypatch):
    """Test get_schema when schema is not in cache."""

    with tempfile.TemporaryDirectory() as tmpdir3:
        monkeypatch.setattr(
            pathlib.Path, "home", lambda: pathlib.Path(tmpdir3)
        )
        cache_dir = cache.cache_path("schema.json", []).parent
        cache_dir.mkdir(parents=True, exist_ok=True)
        schema_cache._is_cache_valid = Mock(return_value=False)

        # Mock requests.get and raise_for_status
        mock_response = Mock()
        mock_response.json.return_value = {"type": "object"}
        mock_response.raise_for_status = Mock()

        with patch("requests.get", return_value=mock_response) as mock_get:
            schema = schema_cache.get_schema("/schema.json")
            mock_get.assert_called_once_with("http://example.com/schema.json")
            mock_response.raise_for_status.assert_called_once()
            assert schema == {"type": "object"}


def test_get_schema_in_cache(schema_cache, monkeypatch):
    """Test get_schema when schema is not in cache."""

    with tempfile.TemporaryDirectory() as tmpdir2:
        monkeypatch.setattr(
            pathlib.Path, "home", lambda: pathlib.Path(tmpdir2)
        )
        cache_dir = cache.cache_path("schema.json", []).parent
        cache_dir.mkdir(parents=True, exist_ok=True)
        schema_cache._is_cache_valid = Mock(return_value=True)
        schema = schema_cache.get_schema("/schema.json")
        assert schema == {"type": "object"}
