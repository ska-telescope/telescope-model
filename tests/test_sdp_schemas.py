import types

import jsonschema
import pytest
from schema import Schema

from ska_telmodel.schema import example_by_uri, schema_by_uri, validate
from ska_telmodel.sdp.common import get_receptor_schema
from ska_telmodel.sdp.version import (
    SDP_ASSIGNRES_PREFIX,
    SDP_CONFIGURE_PREFIX,
    SDP_RECVADDRS_PREFIX,
    SDP_RELEASERES_PREFIX,
    SDP_SCAN_PREFIX,
)


def test_receptors_not_strict():
    schema = get_receptor_schema(False)
    assert schema.is_valid("xxx")
    assert not schema.is_valid(0)


def test_receptors_strict():
    # pylint: disable=not-callable
    schema = get_receptor_schema(True)
    # Add is_valid method as it's missing from Or.
    schema.is_valid = types.MethodType(Schema.is_valid, schema)

    assert schema.is_valid("xxx")

    for i in range(1, 225):
        assert schema.is_valid(f"C{i}")
        assert schema.is_valid(f"c{i}")
    assert schema.is_valid("W1-1")
    assert schema.is_valid("N1-1")
    assert schema.is_valid("E1-1")
    for i in range(1, 134):
        assert schema.is_valid(f"SKA{i:03d}")
    for i in range(0, 64):
        assert schema.is_valid(f"MKT{i:03d}")


def test_sdp_assign_resources():
    for version in ["0.2", "0.3", "0.4", "1.0"]:
        assignres_ver = SDP_ASSIGNRES_PREFIX + version
        assignres = example_by_uri(assignres_ver)
        validate(assignres_ver, assignres, 2)

        # Should also validate via JSON schema
        jsonschema.validate(
            example_by_uri(assignres_ver),
            schema_by_uri(assignres_ver).json_schema(assignres_ver),
        )

        # Test that the old schema URI domain still works
        if version <= "0.3":
            assignres_ver_old = (
                "https://schema.skatelescope.org/ska-sdp-assignres/" + version
            )
            validate(assignres_ver_old, example_by_uri(assignres_ver_old), 2)

            # Check that an error is raised if 'id' / 'eb_id' is omitted
            assignres_erroneous = example_by_uri(assignres_ver)
            for key in ["id", "eb_id"]:
                if key in assignres_erroneous:
                    del assignres_erroneous[key]
            validate(assignres_ver, assignres_erroneous, 0)
            with pytest.raises(ValueError):
                validate(assignres_ver, assignres_erroneous, 1)

        # Try with interface version given as part of JSON object
        assignres_example = example_by_uri(assignres_ver)
        assignres_example["interface"] = assignres_ver
        validate(None, assignres_example, 1)


def test_sdp_release_resources():
    for version in ["0.4"]:
        releaseres_ver = SDP_RELEASERES_PREFIX + version
        validate(releaseres_ver, example_by_uri(releaseres_ver), 2)

        # Should also validate via JSON schema
        jsonschema.validate(
            example_by_uri(releaseres_ver),
            schema_by_uri(releaseres_ver).json_schema(releaseres_ver),
        )


def test_sdp_configure():
    for version in ["0.2", "0.3"]:
        configure_ver = SDP_CONFIGURE_PREFIX + version
        example1 = example_by_uri(configure_ver)
        example2 = example_by_uri(configure_ver, "new_calibration")
        validate(configure_ver, example1, 2)
        validate(configure_ver, example2, 2)

        # Should also validate via JSON schema
        jschema = schema_by_uri(configure_ver).json_schema(configure_ver)
        jsonschema.validate(example1, jschema)
        jsonschema.validate(example2, jschema)

        # Try with interface version given as part of JSON object
        configure_example = example_by_uri(configure_ver)
        configure_example["interface"] = configure_ver
        validate(None, configure_example, 2)


def test_sdp_scan():
    for version in ["0.2", "0.3"]:
        scan_ver = SDP_SCAN_PREFIX + version
        validate(scan_ver, example_by_uri(scan_ver), 2)

        # Try with interface version given as part of JSON object
        scan_example = example_by_uri(scan_ver)
        scan_example["interface"] = scan_ver
        validate(None, scan_example, 2)


def test_sdp_receive_addresses():
    addrs_ver1 = SDP_RECVADDRS_PREFIX + "0.1"
    addrs_ver2 = SDP_RECVADDRS_PREFIX + "0.3"
    addrs_ver3 = SDP_RECVADDRS_PREFIX + "0.4"
    addrs_ver4 = SDP_RECVADDRS_PREFIX + "0.5"

    validate(addrs_ver1, example_by_uri(addrs_ver1), 2)
    with pytest.raises(
        ValueError,
        match=r"'SDP receive addresses map 0.3' Key 'scanId' error:",
    ):
        validate(addrs_ver2, example_by_uri(addrs_ver1), 1)

    validate(addrs_ver2, example_by_uri(addrs_ver2), 2)
    with pytest.raises(
        ValueError,
        match=r"Validation 'SDP receive addresses 0.1' Missing keys: "
        "'receiveAddresses', 'scanId', 'totalChannels'",
    ):
        validate(addrs_ver1, example_by_uri(addrs_ver2), 2)

    validate(addrs_ver3, example_by_uri(addrs_ver3), 2)
    validate(addrs_ver4, example_by_uri(addrs_ver4), 2)

    # Try with interface version given as part of JSON object
    recv_addrs = example_by_uri(addrs_ver1)
    recv_addrs["interface"] = addrs_ver1
    validate(None, recv_addrs, 2)
    recv_addrs = example_by_uri(addrs_ver2)
    recv_addrs["interface"] = addrs_ver2
    validate(None, recv_addrs, 2)


def test_channel_map_error():
    addrs_ver2 = SDP_RECVADDRS_PREFIX + "0.3"

    obj = example_by_uri(addrs_ver2)
    obj["science"]["host"][0][1] = 2  # Invalid, should be str
    validate(addrs_ver2, obj, 1)
    with pytest.raises(
        ValueError,
        match=r"Strict validation 'SDP receive addresses map 0.3' "
        "Key 'science' error",
    ):
        validate(addrs_ver2, obj, 2)


def test_sdp_invalid_examples():
    error = r"No matching example version"
    with pytest.raises(ValueError, match=error):
        example_by_uri(SDP_ASSIGNRES_PREFIX + "0.0")
    with pytest.raises(ValueError, match=error):
        example_by_uri(SDP_RELEASERES_PREFIX + "0.0")
    with pytest.raises(ValueError, match=error):
        example_by_uri(SDP_CONFIGURE_PREFIX + "0.0")
    with pytest.raises(ValueError, match=error):
        example_by_uri(SDP_SCAN_PREFIX + "0.0")
    with pytest.raises(ValueError, match=error):
        example_by_uri(SDP_RECVADDRS_PREFIX + "0.0")


def test_sdp_invalid_old_schema_uri():
    with pytest.raises(ValueError, match=r"Unknown schema URI kind:"):
        validate("https://schema.skatelescope.org/xxx/0.0", {})
    with pytest.raises(
        ValueError, match=r"Unknown schema URI kind for example:"
    ):
        example_by_uri("https://schema.skatelescope.org/xxx/0.0")
