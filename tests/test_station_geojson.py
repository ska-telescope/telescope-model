import pytest

from ska_telmodel.mccs.station_geojson import (
    STATION_FEATURES_PREFIX,
    STATION_FEATURES_VERSIONS,
    STATION_GEOMETRY_PREFIX,
    STATION_GEOMETRY_VERSIONS,
    STATION_PREFIX,
    STATION_PROPERTIES_PREFIX,
    STATION_PROPERTIES_VERSIONS,
    STATION_VERSIONS,
    check_station_interface_version,
)
from ska_telmodel.schema import example_by_uri, validate


def test_station_versions(caplog):
    assert (
        check_station_interface_version(STATION_PREFIX + "0.7", STATION_PREFIX)
        == "0.7"
    )

    invalid_prefix = "http://schexma.example.org/invalid"
    with pytest.raises(
        ValueError,
        match=f"STATION interface URI '{invalid_prefix}' not allowed",
    ):
        check_station_interface_version(invalid_prefix)


def test_properties():
    for properties_uri in STATION_PROPERTIES_VERSIONS:
        # Check normal case validates
        validate(properties_uri, example_by_uri(properties_uri), 2)

        # Check that an error is raised if 'receptors' is omitted
        location_erronious = example_by_uri(properties_uri)
        del location_erronious["station_num"]
        with pytest.raises(ValueError):
            validate(properties_uri, location_erronious, 2)

        # Check validation when interface version is given as part of JSON
        validate(None, example_by_uri(properties_uri), 1)

        # Check invalid version raises error
        with pytest.raises(ValueError, match=r"Could not generate example"):
            example_by_uri(STATION_PROPERTIES_PREFIX + "9.9")


def test_geometry():
    for geometry_uri in STATION_GEOMETRY_VERSIONS:
        # Check normal case validates
        validate(geometry_uri, example_by_uri(geometry_uri), 2)

        # Check that an error is raised if 'receptors' is omitted
        geometry_erronious = example_by_uri(geometry_uri)
        del geometry_erronious["type"]
        with pytest.raises(ValueError):
            validate(geometry_uri, geometry_erronious, 2)

        # Check validation when interface version is given as part of JSON
        validate(None, example_by_uri(geometry_uri), 1)

        # Check invalid version raises error
        with pytest.raises(ValueError, match=r"Could not generate example"):
            example_by_uri(STATION_GEOMETRY_PREFIX + "9.9")


def test_features():
    for features_uri in STATION_FEATURES_VERSIONS:
        # Check normal case validates
        validate(features_uri, example_by_uri(features_uri), 2)

        # Check that an error is raised if 'receptors' is omitted
        features_erronious = example_by_uri(features_uri)
        del features_erronious["type"]
        with pytest.raises(ValueError):
            validate(features_uri, features_erronious, 2)

        # Check validation when interface version is given as part of JSON
        validate(None, example_by_uri(features_uri), 1)

        # Check invalid version raises error
        with pytest.raises(ValueError, match=r"Could not generate example"):
            example_by_uri(STATION_FEATURES_PREFIX + "9.9")


def test_station():
    for station_uri in STATION_VERSIONS:
        # Check normal case validates
        validate(station_uri, example_by_uri(station_uri), 2)

        # Check that an error is raised if 'receptors' is omitted
        station_erronious = example_by_uri(station_uri)
        del station_erronious["type"]
        with pytest.raises(ValueError):
            validate(station_uri, station_erronious, 2)

        # Check validation when interface version is given as part of JSON
        validate(None, example_by_uri(station_uri), 1)

        # Check invalid version raises error
        with pytest.raises(ValueError, match=r"Could not generate example"):
            example_by_uri(STATION_PREFIX + "9.9")
