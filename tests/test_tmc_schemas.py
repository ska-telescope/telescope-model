import json
from contextlib import nullcontext as does_not_raise

import jsonschema
import pytest

from ska_telmodel.schema import example_by_uri, schema_by_uri, validate
from ska_telmodel.tmc.version import (
    LOW_TMC_ASSIGNRESOURCES_PREFIX,
    LOW_TMC_CONFIGURE_PREFIX,
    LOW_TMC_RELEASERESOURCES_PREFIX,
    LOW_TMC_SCAN_PREFIX,
    TMC_ASSIGNEDRES_PREFIX,
    TMC_ASSIGNRESOURCES_PREFIX,
    TMC_CONFIGURE_PREFIX,
    TMC_RELEASERESOURCES_PREFIX,
    TMC_SCAN_PREFIX,
    check_tmc_interface_version,
    low_tmc_assignresources_uri,
    low_tmc_configure_uri,
    low_tmc_releaseresources_uri,
    low_tmc_scan_uri,
    tmc_assignedres_uri,
    tmc_assignresources_uri,
    tmc_configure_uri,
    tmc_releaseresources_uri,
    tmc_scan_uri,
)

version_list = [1, 2]
version_list_low_tmc_scan = [1, 2, 3, 4]
version_list_low_tmc_release = [1, 2, 3]


def test_tmc_versions(caplog):
    assert (
        check_tmc_interface_version(
            LOW_TMC_CONFIGURE_PREFIX + "2.0", LOW_TMC_CONFIGURE_PREFIX
        )
        == "2.0"
    )

    assert (
        check_tmc_interface_version(
            LOW_TMC_CONFIGURE_PREFIX + "1.0", LOW_TMC_CONFIGURE_PREFIX
        )
        == "1.0"
    )
    assert (
        check_tmc_interface_version(
            LOW_TMC_CONFIGURE_PREFIX + "3.1", LOW_TMC_CONFIGURE_PREFIX
        )
        == "3.1"
    )
    assert (
        check_tmc_interface_version(
            LOW_TMC_CONFIGURE_PREFIX + "3.2", LOW_TMC_CONFIGURE_PREFIX
        )
        == "3.2"
    )
    assert (
        check_tmc_interface_version(
            LOW_TMC_CONFIGURE_PREFIX + "4.0", LOW_TMC_CONFIGURE_PREFIX
        )
        == "4.0"
    )
    assert (
        check_tmc_interface_version(
            LOW_TMC_CONFIGURE_PREFIX + "4.1", LOW_TMC_CONFIGURE_PREFIX
        )
        == "4.1"
    )
    assert (
        check_tmc_interface_version(
            LOW_TMC_ASSIGNRESOURCES_PREFIX + "3.2",
            LOW_TMC_ASSIGNRESOURCES_PREFIX,
        )
        == "3.2"
    )
    assert (
        check_tmc_interface_version(
            LOW_TMC_ASSIGNRESOURCES_PREFIX + "4.0",
            LOW_TMC_ASSIGNRESOURCES_PREFIX,
        )
        == "4.0"
    )
    assert (
        check_tmc_interface_version(
            LOW_TMC_SCAN_PREFIX + "4.0", LOW_TMC_SCAN_PREFIX
        )
        == "4.0"
    )
    assert (
        check_tmc_interface_version(
            LOW_TMC_RELEASERESOURCES_PREFIX + "3.0",
            LOW_TMC_RELEASERESOURCES_PREFIX,
        )
        == "3.0"
    )

    invalid_prefix = "http://schexma.example.org/invalid"
    with pytest.raises(
        ValueError, match=f"TMC interface URI '{invalid_prefix}' not allowed"
    ):
        check_tmc_interface_version(invalid_prefix)


@pytest.mark.parametrize(
    "ver, minor",
    [(1, 0), (2, 0), (3, 0), (3, 1), (3, 2), (4, 0)],
)
def test_low_tmc_assign_resources(ver, minor):
    """Test TMC low assign resource allocation schema correctly validates
    the expected JSON.
    """

    assign_ver = low_tmc_assignresources_uri(ver, minor)
    validate(assign_ver, example_by_uri(assign_ver), 2)

    # Check that an error is raised if 'subarray_id' is omitted
    tmc_assign_erronous = example_by_uri(assign_ver)
    del tmc_assign_erronous["subarray_id"]
    with pytest.raises(ValueError):
        validate(assign_ver, tmc_assign_erronous, 1)

    # Try with interface version given as part of JSON object
    tmc_assign_versioned = example_by_uri(assign_ver)
    tmc_assign_versioned["interface"] = assign_ver
    validate(None, tmc_assign_versioned, 1)

    # Check invalid version raises error
    with pytest.raises(ValueError, match=r"Could not generate example"):
        example_by_uri(LOW_TMC_ASSIGNRESOURCES_PREFIX + "0.0")


def test_tmc_assign_resources():
    """Test Mid TMC resource allocation schema correctly validates
    the expected JSON.
    """
    assign_ver = tmc_assignresources_uri(2, 1)
    validate(assign_ver, example_by_uri(assign_ver), 2)

    # Check that an error is raised if 'subarray_id' is omitted
    tmc_assign_erronous = example_by_uri(assign_ver)
    del tmc_assign_erronous["subarray_id"]
    with pytest.raises(ValueError):
        validate(assign_ver, tmc_assign_erronous, 1)

    # Try with interface version given as part of JSON object
    tmc_assign_versioned = example_by_uri(assign_ver)
    tmc_assign_versioned["interface"] = assign_ver
    validate(None, tmc_assign_versioned, 1)

    # Check invalid version raises error
    with pytest.raises(ValueError, match=r"Could not generate example"):
        example_by_uri(TMC_ASSIGNRESOURCES_PREFIX + "0.0")


@pytest.mark.parametrize(
    "field_name, invalid_input",
    [("subarray_id", 50), ("subarray_id", -1)],
)
@pytest.mark.parametrize(
    "sdp_field_name, sdp_field_invalid_input",
    [("interface", 0.4), ("execution_block", "-1")],
)
def test_low_tmc_assign_resources_invalid_input(
    field_name, invalid_input, sdp_field_name, sdp_field_invalid_input
):
    """Test assign resources schema throws an error on invalid input."""
    for ver in version_list:
        assign_ver = low_tmc_assignresources_uri(ver, 0)
        validate(assign_ver, example_by_uri(assign_ver), 2)
        assign_json = example_by_uri(assign_ver)
        assign_json[field_name] = invalid_input
        with pytest.raises(ValueError, match=r"should evaluate to True"):
            validate(assign_ver, assign_json, 2)

    assign_ver = low_tmc_assignresources_uri(3, 2)
    validate(assign_ver, example_by_uri(assign_ver), 2)
    assign_json = example_by_uri(assign_ver)
    assign_json[field_name] = invalid_input
    with pytest.raises(ValueError):
        validate(assign_ver, assign_json, 2)
    # checking for fields within sdp
    assign_json = example_by_uri(assign_ver)
    assign_json["sdp"][sdp_field_name] = sdp_field_invalid_input
    with pytest.raises(ValueError):
        validate(assign_ver, assign_json, 2)


@pytest.mark.parametrize(
    "field_name, invalid_input",
    [("sdp", None)],
)
def test_low_tmc_assign_resources_empty_sdp_input(field_name, invalid_input):
    """Test assign resources schema throws an error if input is empty ."""

    assign_ver = low_tmc_assignresources_uri(4, 0)
    assign_json = example_by_uri(assign_ver)
    assign_json[field_name] = invalid_input
    with pytest.raises(ValueError):
        validate(assign_ver, assign_json, 2)


@pytest.mark.parametrize(
    "field_name, invalid_input",
    [("sdp", 50), ("subarray_id", "-1")],
)
@pytest.mark.parametrize(
    "sdp_field_name, sdp_field_invalid_input",
    [
        ("interface", 0.4),
        ("execution_block", "-1"),
    ],
)
def test_tmc_assign_resources_invalid_input(
    field_name, invalid_input, sdp_field_name, sdp_field_invalid_input
):
    """Test Mid TMC assign resources schema
    throws an error on invalid input."""

    assign_ver = tmc_assignresources_uri(2, 1)
    assign_json = example_by_uri(assign_ver)
    assign_json[field_name] = invalid_input
    with pytest.raises(ValueError):
        validate(assign_ver, assign_json, 2)
    # checking for fields within sdp
    assign_json = example_by_uri(assign_ver)
    assign_json["sdp"][sdp_field_name] = sdp_field_invalid_input
    with pytest.raises(ValueError):
        validate(assign_ver, assign_json, 2)


@pytest.mark.parametrize(
    "field_name, invalid_input",
    [
        ("subarray_beam_ids", [1, 2]),
        ("subarray_beam_ids", [50]),
        ("station_ids", [[513]]),
        ("channel_blocks", [1] * 50),
    ],
)
def test_low_tmc_assign_resources_invalid_mccs_input(
    field_name, invalid_input
):
    """Test MCCS schema throws an error on invalid input."""
    for ver in version_list:
        assign_ver = low_tmc_assignresources_uri(ver, 0)
        validate(assign_ver, example_by_uri(assign_ver), 2)
        assign_json = example_by_uri(assign_ver)
        assign_json["mccs"][field_name] = invalid_input

        with pytest.raises(ValueError, match=r"should evaluate to True"):
            validate(assign_ver, assign_json, 2)


@pytest.mark.parametrize(
    "ver, minor",
    [(1, 0), (2, 0), (3, 0), (3, 1), (3, 2), (3, 3), (4, 0), (4, 1)],
)
def test_low_tmc_configure(ver, minor):
    """Test TMC configure schema correctly validates the
    expected JSON.
    """

    configure_ver = low_tmc_configure_uri(ver, minor)
    validate(configure_ver, example_by_uri(configure_ver), 2)

    # Try with interface version given as part of JSON object
    tmc_configure_versioned = example_by_uri(configure_ver)
    tmc_configure_versioned["interface"] = configure_ver
    validate(None, tmc_configure_versioned, 1)

    # Check invalid version raises error
    with pytest.raises(ValueError, match=r"Could not generate example"):
        example_by_uri(LOW_TMC_CONFIGURE_PREFIX + "0.0")

    configure_ver = low_tmc_configure_uri(3, 1)
    validate(configure_ver, example_by_uri(configure_ver), 2)


@pytest.mark.parametrize(
    "field_name, invalid_input",
    [
        ("stations", [{"station_id": 513}]),
        ("stations", [{"station_id": 1}] * 513),
    ],
)
def test_tmc_configure_invalid_mccs_input(field_name, invalid_input):
    """Test TMC configure schema throws an error on invalid input."""
    for ver, minor in [(1, 0), (2, 0), (3, 0), (3, 1)]:
        configure_ver = low_tmc_configure_uri(ver, minor)
        configure_json = example_by_uri(configure_ver)
        configure_json["mccs"][field_name] = invalid_input

        with pytest.raises(ValueError, match=r"should evaluate to True"):
            validate(configure_ver, configure_json, 2)


@pytest.mark.parametrize(
    "field_name, invalid_input",
    [("scan_duration", -1.0)],
)
def test_low_tmc_configure_invalid_tmc_input(field_name, invalid_input):
    """Test TMC configure schema throws an error on invalid input."""
    for ver, minor in [
        (1, 0),
        (2, 0),
        (3, 0),
        (3, 1),
        (3, 2),
        (3, 3),
        (4, 0),
        (4, 1),
    ]:
        configure_ver = low_tmc_configure_uri(ver, minor)
        validate(configure_ver, example_by_uri(configure_ver), 2)

        configure_json = example_by_uri(configure_ver)
        configure_json["tmc"][field_name] = invalid_input

        with pytest.raises(ValueError, match=r"should evaluate to True"):
            validate(configure_ver, configure_json, 2)


@pytest.mark.parametrize(
    "field_name",
    ["tmc", "transaction_id"],
)
def test_low_tmc_configure_optional_root_elements(field_name):
    """Verify that optional elements can be removed from the Configure payload
    without triggering a validation error
    """
    for ver, minor in [(2, 0), (3, 0), (3, 1), (3, 2), (3, 3), (4, 0), (4, 1)]:
        configure_ver = low_tmc_configure_uri(ver, minor)
        validate(configure_ver, example_by_uri(configure_ver), 2)

        configure_json = example_by_uri(configure_ver)
        del configure_json[field_name]
        validate(configure_ver, configure_json, 2)


@pytest.mark.parametrize(
    "field_name, invalid_input",
    [
        ("subarray_beam_id", 50),
        ("station_ids", [0, 1]),
        ("station_ids", [1, 513]),
        ("update_rate", -1.0),
        ("channels", [[0, 8, 1]]),
        ("channels", [[1, 8, 1, 1]]),
        ("channels", [[1, 1, 1, 1]]),
        ("channels", [[1, 1, 0, 1]]),
        ("channels", [[1, 1, 1, 10]]),
        ("antenna_weights", [0.0] * 513),
        ("antenna_weights", [-0.1]),
        ("antenna_weights", [256.1]),
        ("phase_centre", [0.0, 25.0]),
        ("phase_centre", [0.0, 0.0, 0.0]),
    ],
)
def test_tmc_configure_subarray_beam_invalid_subarray_beam_input(
    field_name, invalid_input
):
    """Test TMC configure schema throws an error on invalid
    subarray beam inputs."""
    for ver, minor in [(2, 0), (3, 0), (3, 1)]:
        configure_ver = low_tmc_configure_uri(ver, minor)
        configure_json = example_by_uri(configure_ver)
        configure_json["mccs"]["subarray_beams"][0][field_name] = invalid_input

        with pytest.raises(ValueError, match=r"should evaluate to True"):
            validate(configure_ver, configure_json, 2)


@pytest.mark.parametrize(
    "field_name, invalid_input",
    [
        ("subarray_beam_id", 49),
        ("update_rate", -0.1),
    ],
)
def test_tmc_configure_3_2_subarray_beam_invalid_subarray_beam_input(
    field_name, invalid_input
):
    """Test TMC configure 3.2 schema throws an error on invalid
    subarray beam inputs."""
    configure_ver = low_tmc_configure_uri(3, 2)
    configure_json = example_by_uri(configure_ver)
    configure_json["mccs"]["subarray_beams"][0][field_name] = invalid_input
    with pytest.raises(ValueError, match=r"should evaluate to True"):
        validate(configure_ver, configure_json, 2)


@pytest.mark.parametrize(
    "logical_bands_item, item_invalid_value",
    [("start_channel", 1), ("number_of_channels", 385)],
)
def test_tmc_configure_3_2_subarray_beam_invalid_logical_bands_input(
    logical_bands_item, item_invalid_value
):
    """Test TMC configure 3.2 schema throws an error on invalid
    logical bands inputs."""
    configure_ver = low_tmc_configure_uri(3, 2)
    configure_json = example_by_uri(configure_ver)
    logical_bands = configure_json["mccs"]["subarray_beams"][0][
        "logical_bands"
    ][0]
    logical_bands[logical_bands_item] = item_invalid_value
    with pytest.raises(ValueError, match=r"should evaluate to True"):
        validate(configure_ver, configure_json, 2)


@pytest.mark.parametrize(
    "sky_coordinates_item, invalid_value",
    [("reference_frame", "ABCD"), ("c1", -360.0), ("c2", 180.0)],
)
def test_tmc_configure_3_2_subarray_beam_invalid_sky_coordinates_input(
    sky_coordinates_item, invalid_value
):
    """Test TMC configure 3.2 schema throws an error on invalid
    sky coordinate inputs."""
    configure_ver = low_tmc_configure_uri(3, 2)
    configure_json = example_by_uri(configure_ver)
    sky_cordinates = configure_json["mccs"]["subarray_beams"][0][
        "sky_coordinates"
    ]
    sky_cordinates[sky_coordinates_item] = invalid_value
    with pytest.raises(ValueError, match=r"should evaluate to True"):
        validate(configure_ver, configure_json, 2)


@pytest.mark.parametrize(
    "csp_field_name, csp_field_invalid_input",
    [
        ("common", {"config_id": 1}),
        ("common", {"subarray_id": "17"}),
        ("common", {"frequency_band": 1}),
        ("lowcbf", 1),
    ],
)
def test_low_tmc_configure_3_2_csp_resources_invalid_input(
    csp_field_name,
    csp_field_invalid_input,
):
    """Test Low TMC configure resources schema with SDP and CSP
    throws an error on invalid input."""

    config_ver = low_tmc_configure_uri(3, 2)
    # checking for fields within csp
    config_json = example_by_uri(config_ver)
    config_json["csp"][csp_field_name] = csp_field_invalid_input
    with pytest.raises(ValueError):
        validate(config_ver, config_json, 2)


def test_low_tmc_release_resources():
    """Test TMC resource release schema correctly validates
    the expected JSON.
    """
    for ver in version_list_low_tmc_release:
        release_ver = low_tmc_releaseresources_uri(ver, 0)
        validate(release_ver, example_by_uri(release_ver), 2)

        # Check that an error is raised if 'subarray_id' is omitted
        tmc_release_erronous = example_by_uri(release_ver)
        del tmc_release_erronous["subarray_id"]
        validate(release_ver, tmc_release_erronous, 0)
        with pytest.raises(ValueError):
            validate(release_ver, tmc_release_erronous, 1)

        # Try with interface version given as part of JSON object
        tmc_release_versioned = example_by_uri(release_ver)
        tmc_release_versioned["interface"] = release_ver
        validate(None, tmc_release_versioned, 1)

        # Check invalid version raises error
        with pytest.raises(ValueError, match=r"Could not generate example"):
            example_by_uri(LOW_TMC_RELEASERESOURCES_PREFIX + "0.0")


@pytest.mark.parametrize(
    "field_name, invalid_input", [("subarray_id", 50), ("subarray_id", -1)]
)
def test_low_tmc_release_resources_invalid_input(field_name, invalid_input):
    """Test TMC release resources schema throws an error on invalid input."""
    for ver in version_list_low_tmc_release:
        release_ver = low_tmc_releaseresources_uri(ver, 0)
        validate(release_ver, example_by_uri(release_ver), 2)

        release_json = example_by_uri(release_ver)
        release_json[field_name] = invalid_input

        with pytest.raises(ValueError, match=r"should evaluate to True"):
            validate(release_ver, release_json, 2)


def test_low_tmc_scan():
    """Test TMC scan schema correctly validates the
    expected JSON.
    """
    for ver in version_list_low_tmc_scan:
        scan_ver = low_tmc_scan_uri(ver, 0)
        validate(scan_ver, example_by_uri(scan_ver), 2)

        # Check that an error is raised if 'scan_id' is omitted
        tmc_scan_erronous = example_by_uri(scan_ver)
        del tmc_scan_erronous["scan_id"]
        validate(scan_ver, tmc_scan_erronous, 0)
        with pytest.raises(ValueError):
            validate(scan_ver, tmc_scan_erronous, 1)

        # Try with interface version given as part of JSON object
        tmc_scan_versioned = example_by_uri(scan_ver)
        tmc_scan_versioned["interface"] = scan_ver
        validate(None, tmc_scan_versioned, 1)

        # Check invalid version raises error
        with pytest.raises(ValueError, match=r"Could not generate example"):
            example_by_uri(LOW_TMC_SCAN_PREFIX + "0.0")


def test_tmc_scan():
    """Test TMC Mid scan schema 2.1 correctly validates the
    expected JSON.
    """
    # for version 2.1
    scan_ver = tmc_scan_uri(2, 1)
    validate(scan_ver, example_by_uri(scan_ver), 2)
    # Check that an error is raised if 'scan_id' is omitted
    tmc_scan_erronous = example_by_uri(scan_ver)
    del tmc_scan_erronous["scan_id"]
    validate(scan_ver, tmc_scan_erronous, 0)
    with pytest.raises(ValueError):
        validate(scan_ver, tmc_scan_erronous, 1)
    # Try with interface version given as part of JSON object
    tmc_scan_versioned = example_by_uri(scan_ver)
    tmc_scan_versioned["interface"] = scan_ver
    validate(None, tmc_scan_versioned, 1)
    # Check invalid version raises error
    with pytest.raises(ValueError, match=r"Could not generate example"):
        example_by_uri(TMC_SCAN_PREFIX + "0.0")


def test_tmc_assigned_resources():
    """Test TMC assigned resources schema correctly validates
    the expected JSON.
    """
    assigned_ver = tmc_assignedres_uri(1, 0)

    # Test with string containing allocated resources
    validate(assigned_ver, example_by_uri(assigned_ver), 2)

    # Test when no resources are allocated to sub-array
    validate(assigned_ver, example_by_uri(assigned_ver, True), 2)

    # Try with interface version given as part of JSON object
    tmc_assigned_versioned = example_by_uri(assigned_ver)
    tmc_assigned_versioned["interface"] = assigned_ver
    validate(None, tmc_assigned_versioned, 1)

    # Check invalid version raises error
    with pytest.raises(ValueError, match=r"Could not generate example"):
        example_by_uri(TMC_ASSIGNEDRES_PREFIX + "0.0")


@pytest.mark.parametrize(
    "field_name, invalid_input",
    [
        ("subarray_beam_ids", [1, 2]),
        ("subarray_beam_ids", [50]),
        ("station_ids", [[513]]),
        ("channel_blocks", [1] * 50),
    ],
)
def test_tmc_assigned_resources_invalid_mccs_input(field_name, invalid_input):
    """Test TMC assigned resources schema throws an error on invalid input."""
    assigned_ver = tmc_assignedres_uri(1, 0)
    assigned_json = example_by_uri(assigned_ver)
    assigned_json["mccs"][field_name] = invalid_input

    with pytest.raises(ValueError, match=r"should evaluate to True"):
        validate(assigned_ver, assigned_json, 2)


def test_tmc_release_resources():
    """Test TMC resource release schema correctly validates
    the expected JSON.
    """

    release_resources_ver = tmc_releaseresources_uri(2, 1)
    validate(release_resources_ver, example_by_uri(release_resources_ver), 2)

    # Check that an error is raised if 'subarray_id' is omitted
    tmc_release_erronous = example_by_uri(release_resources_ver)
    del tmc_release_erronous["subarray_id"]
    validate(release_resources_ver, tmc_release_erronous, 0)
    with pytest.raises(ValueError):
        validate(release_resources_ver, tmc_release_erronous, 1)

    # Try with interface version given as part of JSON object
    tmc_release_versioned = example_by_uri(release_resources_ver)
    tmc_release_versioned["interface"] = release_resources_ver
    validate(None, tmc_release_versioned, 1)

    assert isinstance(tmc_release_versioned["receptor_ids"], list)

    tmc_release_erronous = example_by_uri(release_resources_ver)
    validate(None, tmc_release_versioned, 1)

    # Check invalid version raises error
    with pytest.raises(ValueError, match=r"Could not generate example"):
        example_by_uri(TMC_RELEASERESOURCES_PREFIX + "0.0")


@pytest.mark.parametrize(
    "field_name, invalid_input",
    [("subarray_id", -1)],
)
def test_tmc_release_resources_invalid_input(field_name, invalid_input):
    """Test TMC release resources schema throws an error on invalid input."""
    release_resource_ver = tmc_releaseresources_uri(2, 1)
    release_resource_json = example_by_uri(release_resource_ver)
    release_resource_json[field_name] = invalid_input

    with pytest.raises(ValueError, match=r"should evaluate to True"):
        validate(release_resource_ver, release_resource_json, 2)


@pytest.mark.parametrize(
    "field_name, invalid_input",
    [("receptor_ids", [1, 2])],
)
def test_tmc_release_resources_receptor_ids_invalid_input(
    field_name, invalid_input
):
    """Test TMC release resources schema throws error when
    invalid input provided to receptor_ids --> list of integer."""
    release_resource_ver = tmc_releaseresources_uri(2, 1)
    release_resource_json = example_by_uri(release_resource_ver)
    release_resource_json[field_name] = invalid_input

    with pytest.raises(ValueError):
        validate(release_resource_ver, release_resource_json, 2)


@pytest.mark.parametrize(
    "field_name, valid_input",
    [("receptor_ids", [])],
)
def test_tmc_release_resources_receptor_ids_valid_input(
    field_name, valid_input
):
    """
    Test TMC release resources schema does not throw an error on valid input.
    valid input --> empty list
    """
    release_resource_ver = tmc_releaseresources_uri(2, 1)
    release_resource_json = example_by_uri(release_resource_ver)
    release_resource_json[field_name] = valid_input

    validate(release_resource_ver, release_resource_json, 2)


@pytest.mark.parametrize(
    "field_name, valid_input",
    [("receptor_ids", ["001", "002"])],
)
def test_tmc_release_resources_receptor_ids_valid_list_of_string_input(
    field_name, valid_input
):
    """
    Test TMC release resources schema does not throw an error on valid input.
    valid input --> list of string
    """
    release_resource_ver = tmc_releaseresources_uri(2, 1)
    release_resource_json = example_by_uri(release_resource_ver)
    release_resource_json[field_name] = valid_input

    validate(release_resource_ver, release_resource_json, 2)


@pytest.mark.parametrize(
    "mid_tmc_config_version",
    ((2, 1), (2, 2), (2, 3), (4, 0), (4, 1)),
)
def test_tmc_configure(mid_tmc_config_version):
    """Test TMC mid configure schema correctly validates the
    expected JSON.
    """
    major, minor = mid_tmc_config_version
    # Try to check if version is correct
    configure_ver = tmc_configure_uri(major, minor)
    validate(configure_ver, example_by_uri(configure_ver), 2)

    # Try with interface version given as part of JSON object
    tmc_conf_versioned = example_by_uri(configure_ver)
    tmc_conf_versioned["interface"] = configure_ver
    validate(None, tmc_conf_versioned, 1)

    # Check that an error is raised if 'interface' is omitted
    tmc_config_erronous = example_by_uri(configure_ver)
    del tmc_config_erronous["interface"]
    validate(configure_ver, tmc_config_erronous, 0)
    with pytest.raises(ValueError):
        validate(configure_ver, tmc_config_erronous, 1)


@pytest.mark.parametrize(
    "schema_version,example_version",
    [
        pytest.param((4, 1), (4, 0), id="schema 4.1 reads example 4.0"),
        pytest.param((2, 3), (2, 2), id="schema 2.3 reads example 2.2"),
        pytest.param((2, 3), (2, 1), id="schema 2.3 reads example 2.1"),
        pytest.param((2, 2), (2, 1), id="schema 2.2 reads example 2.1"),
    ],
)
def test_tmc_mid_configure_backward_compatibility(
    schema_version, example_version
):
    """Test TMC-Mid Configure minor versions are backwards compatible by
    having newer schema validate older JSON examples of the same major
    version.
    """
    configure_ver = tmc_configure_uri(*schema_version)
    configure_json = example_by_uri(tmc_configure_uri(*example_version))
    validate(configure_ver, configure_json, 2)


def test_tmc_configure_invalid_version():
    """Test TMC mid configure example raises an error if example for version
    does not exist
    """
    # Check invalid version raises error
    with pytest.raises(ValueError, match=r"Could not generate example"):
        example_by_uri(TMC_CONFIGURE_PREFIX + "0.0")


@pytest.mark.parametrize(
    "configure_ver",
    [
        tmc_configure_uri(2, 2),
        tmc_configure_uri(2, 3),
        tmc_configure_uri(3, 0),
        tmc_configure_uri(4, 0),
        tmc_configure_uri(4, 1),
    ],
)
def test_tmc_configure_partial_configuration(configure_ver):
    """Test TMC mid configure schema 2.2 and above correctly validates JSON
    containing a partial configuration for a delta scan.
    """
    configure_json = {
        "interface": configure_ver,
        "transaction_id": "txn-....-00002",
        "pointing": {
            "target": {
                "ca_offset_arcsec": 0.0,
                "ie_offset_arcsec": 5.0,
            }
        },
        "tmc": {"partial_configuration": True},
    }
    validate(configure_ver, configure_json, 2)


@pytest.mark.parametrize(
    "field_name, invalid_input",
    [("scan_duration", -1.0)],
)
def test_tmc_configure_invalid_tmc_input(field_name, invalid_input):
    """Test TMC configure schema throws an error on invalid input."""
    configure_ver = tmc_configure_uri(2, 1)
    configure_json = example_by_uri(configure_ver)
    configure_json["tmc"][field_name] = invalid_input

    with pytest.raises(ValueError, match=r"should evaluate to True"):
        validate(configure_ver, configure_json, 2)


@pytest.mark.parametrize(
    "field_name, invalid_input",
    [("sdp", 50), ("subarray_id", "-1"), ("csp", "50")],
)
@pytest.mark.parametrize(
    "sdp_field_name, sdp_field_invalid_input",
    [("interface", 0.4), ("scan_type", 1)],
)
@pytest.mark.parametrize(
    "csp_field_name, csp_field_invalid_input",
    [("interface", 0.0), ("lowcbf", 1), ("common", {"config_id": 1})],
)
def test_low_tmc_configure_resources_invalid_input(
    field_name,
    invalid_input,
    sdp_field_name,
    sdp_field_invalid_input,
    csp_field_name,
    csp_field_invalid_input,
):
    """Test Low TMC configure resources schema with SDP and CSP
    throws an error on invalid input."""

    for ver, minor in [(3, 1), (4, 1)]:
        config_ver = low_tmc_configure_uri(ver, minor)
        config_json = example_by_uri(config_ver)
        config_json[field_name] = invalid_input
        with pytest.raises(ValueError):
            validate(config_ver, config_json, 2)

        # checking for fields within sdp
        config_json = example_by_uri(config_ver)
        config_json["sdp"][sdp_field_name] = sdp_field_invalid_input
        with pytest.raises(ValueError):
            validate(config_ver, config_json, 2)

        # checking for fields within csp
        config_json = example_by_uri(config_ver)
        config_json["csp"][csp_field_name] = csp_field_invalid_input
        with pytest.raises(ValueError):
            validate(config_ver, config_json, 2)


@pytest.mark.parametrize(
    "field_name, invalid_input",
    [
        ("stn_beam_id", "50"),
        ("stn_beam_id", "10"),
        ("freq_ids", "520"),
    ],
)
def test_low_tmc_configure_stn_beams_invalid_stn_beams_input(
    field_name, invalid_input
):
    """Test TMC configure schema throws an error on invalid
    stn beams inputs."""
    configure_ver = low_tmc_configure_uri(3, 1)
    configure_json = example_by_uri(configure_ver)
    configure_json["csp"]["lowcbf"]["stations"]["stn_beams"][0][
        field_name
    ] = invalid_input

    with pytest.raises(ValueError):
        validate(configure_ver, configure_json, 2)


@pytest.mark.parametrize(
    "field_name, invalid_input",
    [
        ("stns", "50"),
        ("stn_beam", "10"),
    ],
)
def test_low_tmc_configure_stations_invalid_stations_input(
    field_name, invalid_input
):
    """Test TMC configure schema throws an error on invalid
    stations inputs."""
    configure_ver = low_tmc_configure_uri(3, 1)
    configure_json = example_by_uri(configure_ver)
    configure_json["csp"]["lowcbf"]["stations"][field_name] = invalid_input

    with pytest.raises(ValueError):
        validate(configure_ver, configure_json, 2)


@pytest.mark.parametrize(
    "field_name, invalid_input",
    [("stations", 50), ("vis", 50)],
)
@pytest.mark.parametrize(
    "lowcbf_field_name, lowcbf_field_invalid_input",
    [
        ("stns", [431]),
        ("stn_beams", [{"stn_beam_id": 432}]),
    ],
)
def test_low_tmc_configure_lowcbf_invalid_input(
    field_name, invalid_input, lowcbf_field_name, lowcbf_field_invalid_input
):
    """Test TMC LOW configure schema throws an error on invalid
    lowcbf inputs."""

    # checking for fields within lowcbf
    config_ver = low_tmc_configure_uri(3, 1)
    config_json = example_by_uri(config_ver)
    config_json["csp"]["lowcbf"][field_name] = invalid_input
    with pytest.raises(ValueError):
        validate(config_ver, config_json, 2)

    # checking for fields within lowcbf stations
    config_ver = low_tmc_configure_uri(3, 1)
    config_json = example_by_uri(config_ver)
    config_json["csp"]["lowcbf"]["stations"][
        lowcbf_field_name
    ] = lowcbf_field_invalid_input
    with pytest.raises(ValueError):
        validate(config_ver, config_json, 2)


@pytest.mark.parametrize(
    "field_name, invalid_input",
    [("fsp", {"function_mode": 2, "fsp_ids": [1]}), ("stn_beams", "10")],
)
def test_low_tmc_configure_vis_invalid_vis_input(field_name, invalid_input):
    """Test TMC configure schema throws an error on invalid
    vis inputs."""
    configure_ver = low_tmc_configure_uri(3, 1)
    configure_json = example_by_uri(configure_ver)
    configure_json["csp"]["lowcbf"]["vis"][field_name] = invalid_input

    with pytest.raises(ValueError):
        validate(configure_ver, configure_json, 2)


@pytest.mark.parametrize(
    "field_name, invalid_input",
    [("function_mode", 10), ("fsp_ids", "1")],
)
def test_low_tmc_configure_fsp_invalid__input(field_name, invalid_input):
    """Test TMC configure schema throws an error on invalid
    vis inputs."""
    configure_ver = low_tmc_configure_uri(3, 1)
    configure_json = example_by_uri(configure_ver)
    configure_json["csp"]["lowcbf"]["vis"]["fsp"][field_name] = invalid_input

    with pytest.raises(ValueError):
        validate(configure_ver, configure_json, 2)


def test_low_tmc_configure_version_3_2():
    """Test TMC LOW configure schema version 3.2"""
    configure_ver = LOW_TMC_CONFIGURE_PREFIX + "3.2"
    configureres = example_by_uri(configure_ver)
    validate(configure_ver, configureres, 1)
    jsonschema.validate(
        example_by_uri(configure_ver),
        schema_by_uri(configure_ver).json_schema(configure_ver),
    )


@pytest.mark.parametrize(
    "omitted_key",
    (
        "interface",
        "mccs",
        "csp",
        "sdp",
    ),
)
def test_low_tmc_configure_version_3_2_with_omitted_key(omitted_key):
    """
    Test TMC low configure version 3.2 schema
    correctly validates the expected JSON.
    """
    configure_ver = LOW_TMC_CONFIGURE_PREFIX + "3.2"
    configure_scan = example_by_uri(configure_ver)
    validate(configure_ver, configure_scan, 1)

    # Check that an error is raised if key is omitted
    configure_ver_erronous = example_by_uri(configure_ver)
    del configure_ver_erronous[omitted_key]
    with pytest.raises(ValueError):
        validate(configure_ver, configure_ver_erronous, 1)


@pytest.mark.parametrize(
    "configure_ver, failure_expected",
    [
        (tmc_configure_uri(2, 1), True),
        (tmc_configure_uri(2, 2), True),
        (tmc_configure_uri(2, 3), False),
    ],
)
def test_pointing_target_is_optional(configure_ver, failure_expected):
    """
    Test that TMC MID pointing.target object is mandatory for schema
    versions < v2.3, and optional from v2.3 onwards.
    """
    configure_json = example_by_uri(configure_ver)
    del configure_json["pointing"]["target"]
    if not failure_expected:
        validate(configure_ver, configure_json, 2)
    else:
        with pytest.raises(ValueError):
            validate(configure_ver, configure_json, 2)


@pytest.mark.parametrize(
    "configure_ver, failure_expected",
    [
        (tmc_configure_uri(2, 1), True),
        (tmc_configure_uri(2, 2), True),
        (tmc_configure_uri(2, 3), False),
    ],
)
def test_pointing_is_optional(configure_ver, failure_expected):
    """
    Test that TMC MID top-level pointing object is mandatory for schema
    versions < v2.3, and optional from v2.3 onwards.
    """
    configure_json = example_by_uri(configure_ver)
    del configure_json["pointing"]
    if not failure_expected:
        validate(configure_ver, configure_json, 2)
    else:
        with pytest.raises(ValueError):
            validate(configure_ver, configure_json, 2)


@pytest.mark.parametrize(
    "value, failure_expected",
    [
        ("MAINTAIN", False),
        ("UPDATE", False),
        ("RESET", False),
        ("maintain", True),
        ("update", True),
        ("reset", True),
        ("", True),
        ("foo", True),
    ],
)
def test_pointing_correction_only_allows_enumerated_values(
    value, failure_expected
):
    """
    Test that the TMC MID pointing.correction field only allows the values
    MAINTAIN, UPDATE, and RESET.
    """
    configure_ver = tmc_configure_uri(2, 3)
    configure_json = example_by_uri(configure_ver)

    configure_json["pointing"]["correction"] = value

    if not failure_expected:
        validate(configure_ver, configure_json, 2)
    else:
        with pytest.raises(ValueError):
            validate(configure_ver, configure_json, 2)


def update_key_value(input, field_name, invalid_value):
    """
    Recursively process JSON, setting fields with the name field_name
    to the provided value.

    Warning: this key recursively updates fields! Calling this function with
    the JSON below and field_name='subarray_id' would set subarray_id in
    multiple places. Be sure that your input JSON does not have identically
    named fields before calling this function.

      {
        "subarray_id": 1,
        "lowcbf": {all JSON
          "subarray_id": 1,
        }
      }

    """
    if isinstance(input, dict):
        if field_name in input:
            input[field_name] = invalid_value
        for _, value in input.items():
            update_key_value(value, field_name, invalid_value)
    elif isinstance(input, list):
        for key in input:
            update_key_value(key, field_name, invalid_value)

    return json


@pytest.mark.parametrize(
    "version, field_name, invalid_input",
    [
        ((4, 0), "subarray_beam_id", 0),
        ((4, 0), "subarray_beam_id", 769),
        ((4, 0), "number_of_channels", 385),
        ((4, 0), "number_of_channels", 7),
        ((4, 0), "station_id", 0),
        ((4, 0), "station_id", 513),
        ((4, 0), "aperture_id", "XY001.01"),
        ((4, 0), "aperture_id", "AP1111.01"),
        ((4, 0), "aperture_id", "AP123.111"),
    ],
)
def test_tmc_assignresources_4_0_invalid_values(
    version, field_name, invalid_input
):
    """Test TMC Assignresources 4.0 schema throws an error on invalid
    subarray beam id."""
    major_version, minor_version = version
    assign_ver = low_tmc_assignresources_uri(major_version, minor_version)
    assign_json = example_by_uri(assign_ver)
    update_key_value(assign_json, field_name, invalid_input)
    with pytest.raises(ValueError, match=r"should evaluate to True"):
        validate(assign_ver, assign_json, 2)


def delete_key(input, key_to_delete):
    """
    Recursively process JSON, deletes given key from json.

    Warning: this key recursively delete keys! Calling this function with
    the JSON below and field_name='subarray_id' would delete subarray_id in
    multiple places. Be sure that your input JSON does not have identically
    named fields before calling this function.

      {
        "subarray_id": 1,
        "lowcbf": {all JSON
          "subarray_id": 1,
        }
      }
    """
    if isinstance(input, dict):
        if key_to_delete in input:
            del input[key_to_delete]
        for _, value in input.items():
            delete_key(value, key_to_delete)
    elif isinstance(input, list):
        for key in input:
            delete_key(key, key_to_delete)

    return json


@pytest.mark.parametrize(
    "field_name",
    [
        "interface",
        "subarray_beams",
        "subarray_beam_id",
        "apertures",
        "number_of_channels",
        "station_id",
        "aperture_id",
    ],
)
def test_tmc_assignresources_4_0_aperture_missing_mccs_keys(field_name):
    """Test TMC Assignresources 4.0 schema throws an error on key is
    not present in json."""
    assign_ver = low_tmc_assignresources_uri(4, 0)
    assign_json = example_by_uri(assign_ver)
    delete_key(assign_json, field_name)
    with pytest.raises(ValueError):
        validate(assign_ver, assign_json, 2)


def test_tmc_assignresources_4_0_aperture_array_size():
    """Test TMC Assignresources 4.0 schema throws an error on aperture array
    size is not valid."""
    assign_ver = low_tmc_assignresources_uri(4, 0)
    assign_json = example_by_uri(assign_ver)
    extended_aperture_array = [None] * 513
    assign_json["mccs"]["subarray_beams"][0][
        "apertures"
    ] = extended_aperture_array
    with pytest.raises(ValueError):
        validate(assign_ver, assign_json, 2)


@pytest.mark.parametrize(
    "schema_uri,reference_frame,expectation",
    [
        pytest.param(
            tmc_configure_uri(3, 0),
            "special",
            does_not_raise(),
            id='TMC-Mid Configure v3.0: reference_frame="special" accepted',
        ),
        pytest.param(
            tmc_configure_uri(3, 0),
            "ICRS",
            does_not_raise(),
            id='TMC-Mid Configure v3.0: reference_frame="ICRS" accepted',
        ),
        pytest.param(
            tmc_configure_uri(3, 0),
            "SPECIAL",
            pytest.raises(ValueError),
            id="TMC-Mid Configure v3.0: reference_frame validation is case sensitive",  # noqa: E501
        ),
        pytest.param(
            tmc_configure_uri(3, 0),
            "foo",
            pytest.raises(ValueError),
            id="TMC-Mid Configure v3.0: invalid reference_frame rejected",
        ),
        pytest.param(
            tmc_configure_uri(2, 3),
            "icrs",
            does_not_raise(),
            id="TMC-Mid Configure v2.3: reference_frame validation is not case sensitive",  # noqa: E501
        ),
        pytest.param(
            tmc_configure_uri(2, 3),
            "foo",
            does_not_raise(),
            id="TMC-Mid Configure v2.3: invalid reference_frame not rejected",
        ),
    ],
)
def test_pointing_reference_frame_values(
    schema_uri, reference_frame, expectation
):
    """
    Verify pointing.reference_frame validation behaviour:

        - any value allowed prior to 3.0
        - case-sensitive validation of an enumerated value set from 3.0+
    """
    example_json = example_by_uri(schema_uri)
    example_json["pointing"]["target"]["reference_frame"] = reference_frame
    with expectation:
        validate(schema_uri, example_json, 2)


def test_tmc_configure_version_4_0_with_omitted_csp_correlation_key():
    """
    Test TMC low configure version 3.2 schema
    correctly validates the expected JSON.
    """
    configure_ver = TMC_CONFIGURE_PREFIX + "4.0"
    configure_scan = example_by_uri(configure_ver)
    validate(configure_ver, configure_scan, 1)

    # Check that an error is raised if key is omitted
    configure_ver_erronous = example_by_uri(configure_ver)
    del configure_ver_erronous["csp"]["midcbf"]["correlation"]
    with pytest.raises(ValueError):
        validate(configure_ver, configure_ver_erronous, 2)


@pytest.mark.parametrize(
    "ommited_processing_region_key",
    [
        "fsp_ids",
        "start_freq",
        "channel_width",
        "channel_count",
        "integration_factor",
        "sdp_start_channel_id",
    ],
)
def test_tmc_configure_version_4_0_with_omitted_csp_processing_region_keys(
    ommited_processing_region_key,
):
    """
    Test TMC low configure version 3.2 schema
    correctly validates the expected JSON.
    """
    configure_ver = TMC_CONFIGURE_PREFIX + "4.0"
    configure_scan = example_by_uri(configure_ver)
    validate(configure_ver, configure_scan, 1)

    # Check that an error is raised if key is omitted
    configure_ver_erronous = example_by_uri(configure_ver)
    csp_configuration = configure_ver_erronous["csp"]["midcbf"]["correlation"][
        "processing_regions"
    ][0]
    del csp_configuration[ommited_processing_region_key]
    with pytest.raises(ValueError):
        validate(configure_ver, configure_ver_erronous, 2)


def test_disable_strictness2(monkeypatch):
    """
    Tests that strictness level 2 cannot be used outside of tests
    """

    configure_ver = TMC_CONFIGURE_PREFIX + "4.0"
    configure_ver_erronous = example_by_uri(configure_ver)
    configure_ver_erronous["asd"] = 1

    with pytest.raises(ValueError):
        validate(configure_ver, configure_ver_erronous, 2)

    # Should only raise a warning, because "ska_tmc" appears in the
    # call stack
    monkeypatch.delenv("PYTEST_VERSION")
    validate(configure_ver, configure_ver_erronous, 2)
